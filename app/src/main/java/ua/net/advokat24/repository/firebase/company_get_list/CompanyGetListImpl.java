package ua.net.advokat24.repository.firebase.company_get_list;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import ua.net.advokat24.util.Fields;
import ua.net.advokat24.util.models.BaseModel;
import ua.net.advokat24.util.models.CategoryListModel;
import ua.net.advokat24.util.models.MTask;

import java.util.ArrayList;
import java.util.List;

public class CompanyGetListImpl implements CompanyGetListInteractor {

    @Override
    public void getCompaniList(final CompanyGetListListener listener) {

        final List<BaseModel> list = new ArrayList<>();
        FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database1.getReference(Fields.COMPANY_TABLE);
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    BaseModel model = postSnapshot.getValue(BaseModel.class);
                    list.add(model);
                }
                listener.saccess(list);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(Fields.TAG, getClass().getSimpleName() + " getList Categorys error "+ databaseError.toException());
                listener.error();
            }
        };
        myRef.addListenerForSingleValueEvent(postListener);
    }
}

package ua.net.advokat24.repository.firebase.tasksimpl;

import ua.net.advokat24.util.models.MTask;

import java.util.List;

public interface TasksListener {
    void resultList(List<MTask>list);
    void error();
}

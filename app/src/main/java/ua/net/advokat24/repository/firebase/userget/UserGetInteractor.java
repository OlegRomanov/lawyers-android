package ua.net.advokat24.repository.firebase.userget;

public interface UserGetInteractor {
    void getUser(String id, UserGetListener listener);
    void getUserRunTime(String id, UserGetListener listener);
}

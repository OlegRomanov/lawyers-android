package ua.net.advokat24.repository.firebase.tasksimpl;

public interface TasksInteractor {

    void getList(String id, boolean isClient, TasksListener listener);
    void getTasksRunTime(String id, boolean isClient, TasksListener listener);

}

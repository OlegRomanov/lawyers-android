package ua.net.advokat24.repository.firebase.uploadfile;

import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import ua.net.advokat24.util.Function;

import ua.net.advokat24.util.Function;

public class UploadFileImpl implements UploadFileInteractor {

    @Override
    public void uploadFile(String firebasePath, Uri uri, final UploadFileListener listener) {

//        if(!isDocument){
//            res = "images/userphoto/" + FirebaseAuth.getInstance().getCurrentUser().getUid() + ".jpg";
//        } else {
//            res = "images/document/" + FirebaseAuth.getInstance().getCurrentUser().getUid() + ".jpg";
//        }
        final String res = "images/" + firebasePath + "/" + FirebaseAuth.getInstance().getCurrentUser().getUid() + ".jpg";
        Function.log("before uploadFile. res = " + res, getClass().getName());
        Function.log("before uploadFile. uri = " + String.valueOf(uri), getClass().getName());
        StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
        StorageReference riversRef = mStorageRef.child(res);
        riversRef.putFile(uri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Function.log("load Image saccess. uri = " + res, getClass().getName());
                        listener.saccess();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Function.eLog(exception.getMessage(), getClass().getName());
                        listener.error(exception.getMessage());
                    }
                });
    }
}

package ua.net.advokat24.repository.firebase.userchack;

import ua.net.advokat24.util.events.EventError;
import ua.net.advokat24.util.events.EventValid;

public interface UserCheckListener extends EventValid, EventError {
}

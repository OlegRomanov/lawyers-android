package ua.net.advokat24.repository.firebase.userupdate;

import android.net.Uri;

import ua.net.advokat24.util.models.usermodels.MUser;

public interface UserUpdateInteractor {

    void updateUser(MUser mUser, Uri userPhoto, Uri documentsPhoto, UserUpdateListener listener);
    void updateUser(MUser mUser, Uri userPhoto, UserUpdateListener listener);
    void updateUser(MUser mUser, UserUpdateListener listener);

}

package ua.net.advokat24.repository.firebase.company_get_list;

public interface CompanyGetListInteractor {

    void getCompaniList(CompanyGetListListener listener);

}

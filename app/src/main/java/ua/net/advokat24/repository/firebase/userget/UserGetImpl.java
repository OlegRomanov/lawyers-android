package ua.net.advokat24.repository.firebase.userget;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import ua.net.advokat24.repository.firebase.clearnotifykey.ClearNotifyKeyImpl;
import ua.net.advokat24.repository.firebase.clearnotifykey.ClearNotifyKeyInteractor;
import ua.net.advokat24.repository.firebase.clearnotifykey.OnClearKeyListener;
import ua.net.advokat24.util.Fields;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.usermodels.MUser;
import ua.net.advokat24.util.utils.FirebaseUtils;

import ua.net.advokat24.util.Function;

public class UserGetImpl implements UserGetInteractor {

    @Override
    public void getUser(String id, final UserGetListener listener) {
        FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database1.getReference(Fields.USERS_TABTE);
        myRef.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                try {
                    if (snapshot.getValue() != null) {
                        try {
                            final MUser mUser = snapshot.getValue(MUser.class);
                            listener.user(mUser);
                        } catch (Exception e) {
                            Function.eLog(e.getMessage(), getClass().getName());
                            listener.user(null);
                        }
                    } else {
                        listener.error("User not found");
                    }
                } catch (Exception e) {
                    Function.eLog(e.getMessage(), getClass().getName());
                    listener.error("Firebase onDataChange Error");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Function.eLog(databaseError.getMessage(), getClass().getName());
                listener.error("Firebase DatabaseError Error");
            }
        });
    }

    @Override
    public void getUserRunTime(String id, final UserGetListener listener) {
        FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database1.getReference(Fields.USERS_TABTE);
        myRef.child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                try {
                    if (snapshot.getValue() != null) {
                        try {
                            MUser mUser = snapshot.getValue(MUser.class);
                            listener.user(mUser);
                        } catch (Exception e) {
                            Function.eLog(e.getMessage(), getClass().getName());
                            listener.user(null);
                        }
                    } else {
                        listener.error("User not found");
                    }
                } catch (Exception e) {
                    Function.eLog(e.getMessage(), getClass().getName());
                    listener.error("Firebase onDataChange Error");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Function.eLog(databaseError.getMessage(), getClass().getName());
                listener.error("Firebase DatabaseError Error");
            }
        });

    }
}

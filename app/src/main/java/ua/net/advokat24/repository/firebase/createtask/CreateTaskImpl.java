package ua.net.advokat24.repository.firebase.createtask;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import ua.net.advokat24.util.Fields;
import ua.net.advokat24.util.models.MTask;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CreateTaskImpl implements CreateTaskInteractor {

    public static boolean isRun = false;
    private ValueEventListener postListener;
    private DatabaseReference myRef;
    private String key;
    private DatabaseReference mDatabase;
    private MTask task;

    @Override
    public void createTask(MTask mTask, final CreateTaskListener listener) {
        isRun = true;
        task = mTask;
        task.setActive(true);
        task.setAnswerReqwest(false);
        task.setPremiumRun(false);

        SimpleDateFormat sdf = new SimpleDateFormat(Fields.FULL_TIME_FORMAT);
        String currentDateandTime = sdf.format(new Date());
        task.setData(currentDateandTime);

        mDatabase = FirebaseDatabase.getInstance().getReference(Fields.TASKS_TABLE);
        key = mDatabase.push().getKey();
        if(task.getId() == null){
            task.setId(key);
        }else {
            key = task.getId();
        }
        mDatabase.child(key).setValue(task);
        FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        myRef = database1.getReference(Fields.TASKS_TABLE);
        postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    if (dataSnapshot.getValue() != null) {
                        try {
                            task = dataSnapshot.getValue(MTask.class);
                            if(!task.isActive() && task.isAnswerReqwest() && task.getLawyerId() != null){
                                myRef.child(key).removeEventListener(postListener);
                                isRun = false;
                                listener.saccess(task.getLawyerId());
                            } else if (task.isActive() && task.isAnswerReqwest()){
                                task.setAnswerReqwest(false);
                                mDatabase.child(key).setValue(task);
                                listener.lawyerDeny();
                            }
                        } catch (Exception e) {
                            Log.e(Fields.TAG, " CreateTaskImpl catch");
                        }
                    } else {
                        Log.e(Fields.TAG, " CreateTaskImpl = null.");
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        myRef.child(key).addValueEventListener(postListener);
    }

    @Override
    public void close() {
        myRef.child(key).removeEventListener(postListener);
        if(task.isActive()){
            task.setActive(false);
            mDatabase.child(key).setValue(task);
        }
        isRun = false;
    }
}

package ua.net.advokat24.repository.firebase.getusers;

import android.support.annotation.Nullable;

import ua.net.advokat24.util.models.MTask;

public interface GetUsersInteractor {
    void getUsersForTask(@Nullable MTask mTask, GetUsersListener listener);

    void getUsersForRunTimeList(@Nullable String category, String city, GetUsersListener listener);
}

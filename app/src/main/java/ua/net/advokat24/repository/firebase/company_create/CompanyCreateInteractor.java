package ua.net.advokat24.repository.firebase.company_create;

import ua.net.advokat24.util.models.BaseModel;

public interface CompanyCreateInteractor {

    void createCompany(BaseModel model, CompanyCreateListener listener);

}

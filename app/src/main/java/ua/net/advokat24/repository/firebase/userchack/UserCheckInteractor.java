package ua.net.advokat24.repository.firebase.userchack;

public interface UserCheckInteractor {
    void checkUser(String id, UserCheckListener listener);
}

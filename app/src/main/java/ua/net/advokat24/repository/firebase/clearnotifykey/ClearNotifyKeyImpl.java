package ua.net.advokat24.repository.firebase.clearnotifykey;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import ua.net.advokat24.repository.firebase.categoryimpl.CategorysInteractor;
import ua.net.advokat24.repository.firebase.categoryimpl.OnCategorysListener;
import ua.net.advokat24.util.Fields;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.CategoryListModel;
import ua.net.advokat24.util.models.usermodels.MUser;

import ua.net.advokat24.util.Function;

public class ClearNotifyKeyImpl implements ClearNotifyKeyInteractor {

    @Override
    public void clearKeys(String key, final OnClearKeyListener listener) {
        ValueEventListener listen = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Function.log("dataSnapshot.getChildrenCount() = " + String.valueOf(dataSnapshot.getChildrenCount()), getClass().getName() + ".clearKeys");
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    MUser user = postSnapshot.getValue(MUser.class);
                    if(user != null){
                        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference(Fields.USERS_TABTE);
                        mDatabase.child(user.getId()).child("notificationKay").setValue(null);
                    }
                }
                if(listener != null){
                    listener.saccess();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                if(listener != null){
                    listener.saccess();
                }
            }
        };

        DatabaseReference mDatabase =FirebaseDatabase.getInstance().getReference(Fields.USERS_TABTE);
        mDatabase.orderByChild("notificationKay").equalTo(key).addListenerForSingleValueEvent(listen);
    }
}

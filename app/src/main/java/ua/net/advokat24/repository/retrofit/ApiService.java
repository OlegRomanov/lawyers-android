package ua.net.advokat24.repository.retrofit;



import ua.net.advokat24.util.models.MPushSender;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiService {

    @Headers({"Content-Type: application/json"})
    @POST("fcm/send")
    Call<String> authValidatePin(@Header("Authorization") String contentRange, @Body String data);

//    @Headers({"Content-Type: application/json"})
//    @POST("fcm/send")
//    Call<String> authValidatePin(@Header("Authorization") String contentRange, @Body MPushSender data);

}

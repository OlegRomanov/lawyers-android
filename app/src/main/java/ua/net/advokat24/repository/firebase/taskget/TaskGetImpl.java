package ua.net.advokat24.repository.firebase.taskget;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import ua.net.advokat24.util.Fields;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.MTask;

public class TaskGetImpl implements TaskGetInteractor {

    @Override
    public void getTask(String id, final TaskGetListener listener) {

        FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database1.getReference(Fields.TASKS_TABLE);
        myRef.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                try {
                    if (snapshot.getValue() != null) {
                        try {
                            MTask mTask1 = snapshot.getValue(MTask.class);
                            if(listener != null){
                                listener.task(mTask1);
                            }
                        } catch (Exception e) {
                            Log.e(Fields.TAG, " CreateTaskImpl catch", e);
                        }
                    } else {
                        Log.e(Fields.TAG, " CreateTaskImpl = null.");
                    }
                } catch (Exception e) {
                    Log.e(Fields.TAG, " CreateTaskImpl catch", e);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(Fields.TAG, " mTask service.onCancelled");
            }
        });

    }

    @Override
    public void getTaskRunTime(String id, final TaskGetListener listener) {
        FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database1.getReference(Fields.TASKS_TABLE);
        myRef.child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                try {
                    if (snapshot.getValue() != null) {
                        try {
                            MTask mTask1 = snapshot.getValue(MTask.class);
                            if(listener != null){
                                listener.task(mTask1);
                            }
                        } catch (Exception e) {
                            Log.e(Fields.TAG, " CreateTaskImpl catch", e);
                        }
                    } else {
                        Log.e(Fields.TAG, " CreateTaskImpl = null.");
                    }
                } catch (Exception e) {
                    Log.e(Fields.TAG, " CreateTaskImpl catch", e);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(Fields.TAG, " mTask service.onCancelled");
            }
        });

    }
}

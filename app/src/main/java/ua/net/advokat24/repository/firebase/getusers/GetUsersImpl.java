package ua.net.advokat24.repository.firebase.getusers;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import ua.net.advokat24.util.Fields;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.MTask;
import ua.net.advokat24.util.models.usermodels.MUser;
import ua.net.advokat24.util.utils.TimeUtils;
import ua.net.advokat24.util.utils.UserStatusUtils;
import ua.net.advokat24.util.utils.curent_location.LocationUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ua.net.advokat24.util.Function;

public class GetUsersImpl implements GetUsersInteractor {

    @Override
    public void getUsersForRunTimeList(@Nullable final String category, final String city, final GetUsersListener listener) {

        final List<MUser> lawyers = new ArrayList<>();
        FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database1.getReference(Fields.USERS_TABTE);
        myRef.orderByChild("client").equalTo(false);
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    try {
                        MUser mUser = postSnapshot.getValue(MUser.class);
                        if (mUser != null &&
                                mUser.isValid() &&
                                !mUser.isClient() &&
                                mUser.getCity() != null &&
                                mUser.getCity().equals(city)) {
                            if (category.length() != 0) {
                                if (mUser.getSkils() != null && mUser.getSkils().contains(category))
                                    lawyers.add(mUser);
                            } else {
                                lawyers.add(mUser);
                            }
                        }
                    } catch (Exception e){
                        Function.eLog(e.getMessage(), getClass().getName() + ".getUsersForRunTimeList");
                    }
                }
                listener.resultSearchUsers(lawyers);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.error();
            }
        });
    }

    @Override
    public void getUsersForTask(final MTask mTask, final GetUsersListener listener) {
        final List<MUser> lawyers = new ArrayList<>();
        FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database1.getReference(Fields.USERS_TABTE);
        myRef.orderByChild("client").equalTo(false);
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    try {
                        MUser mUser = postSnapshot.getValue(MUser.class);
                        if (mUser != null && mUser.isValid() && !TextUtils.isEmpty(mUser.getNotificationKay()) && validUser(mUser)) {
                            if(isCategory(mTask.getCategory(), mUser)){
                                Function.log(mUser.getEmail() + " user start", getClass().getName() + ".getUsersForTask");
                                addUser(mTask, mUser, lawyers);
                            }
                        }
                    } catch (Exception e){
                        Function.eLog(e.getMessage(), getClass().getName() + ".getUsersForTask");
                    }
                }
                sortingLists(lawyers, listener, mTask);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.error();
            }
        });
    }

    private void addUser(MTask mTask, MUser mUser, List<MUser> lawyers) {
        if (mTask.getLocatoinLat() != null && mTask.getLocatoinLon() != null) {
            if (mUser.getLocatoinLat() != null && mUser.getLocatoinLon() != null) {
                int a = LocationUtils.getMeatersBeetwenPoints(
                        new LatLng(Double.parseDouble(mUser.getLocatoinLat()), Double.parseDouble(mUser.getLocatoinLon())),
                        new LatLng(Double.parseDouble(mTask.getLocatoinLat()), Double.parseDouble(mTask.getLocatoinLon())));

                Function.log(mUser.getEmail() + " user before", getClass().getName() + ".getUsersForTask");
                if(a < 10000) {
                    Function.log(mUser.getEmail() + " user after", getClass().getName() + ".getUsersForTask");
                    lawyers.add(mUser);
                }

            }
        }
    }

    private void sortingLists(List<MUser> lawyers, GetUsersListener listener, MTask mTask) {

        StringBuilder tost = new StringBuilder();
        for (MUser user : lawyers){
            String b = "0";
            if (user.getLocatoinLat() != null && user.getLocatoinLon() != null) {
                int a = LocationUtils.getMeatersBeetwenPoints(
                        new LatLng(Double.parseDouble(user.getLocatoinLat()), Double.parseDouble(user.getLocatoinLon())),
                        new LatLng(Double.parseDouble(mTask.getLocatoinLat()), Double.parseDouble(mTask.getLocatoinLon())));
                b = String.valueOf(a);
            }
            String res = user.getEmail() + "\n" + user.getCity() + "\n" + b + "\n\n\n";
            tost.append(res);
        }

//        Function.showToast(tost.toString());
        listener.resultSearchUsers(lawyers);
    }

    private synchronized boolean validUser(MUser mUser) {
        return mUser.getStatus() == UserStatusUtils.USER_STATUS_FREE && validUserWorkTimeDays(mUser);
    }

//    private synchronized boolean validUserWorkTime(MUser mUser){
//        return validUserWorkTimeHours(mUser.getWorkTime().getTimeStart(), mUser.getWorkTime().getTimeEnd(), TimeUtils.getCurentHourTime())
//                || validUserWorkTimeHours(mUser.getWorkTime().getTimeDoubleStart(), mUser.getWorkTime().getTimeDoubleEnd(), TimeUtils.getCurentHourTime());
//    }

    private synchronized boolean validUserWorkTime(MUser mUser){
        return validUserWorkTimeHours(mUser.getWorkTime().getTimeStart(), mUser.getWorkTime().getTimeEnd(), TimeUtils.getCurentHourTime());
    }

    private synchronized boolean validUserWorkTimeDays(MUser mUser){
        boolean result = false;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        switch (calendar.get(Calendar.DAY_OF_WEEK)){
            case 1:
                if(mUser.getWorkTime().isMonday() && validUserWorkTime(mUser))
                    result =  true;
                break;
            case 2:
                if(mUser.getWorkTime().isTuesday() && validUserWorkTime(mUser))
                    result =  true;
                break;
            case 3:
                if(mUser.getWorkTime().isWednesday() && validUserWorkTime(mUser))
                    result =  true;
                break;
            case 4:
                if(mUser.getWorkTime().isThursday() && validUserWorkTime(mUser))
                    result =  true;
                break;
            case 5:
                if(mUser.getWorkTime().isFriday() && validUserWorkTime(mUser))
                    result =  true;
                break;
            case 6:
                if(mUser.getWorkTime().isSaturday() && validUserWorkTime(mUser))
                    result =  true;
                break;
            case 7:
                if(mUser.getWorkTime().isSunday() && validUserWorkTime(mUser))
                    result =  true;
                break;
        }
        return result;
    }

    private synchronized boolean validUserWorkTimeHours(String start, String end, String curent){
        boolean result = false;
        try {
            Date time1 = new SimpleDateFormat(Fields.HOUR_TIME_FORMAT).parse(start);
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(time1);
            calendar1.add(Calendar.DATE, 1);

            Date time2 = new SimpleDateFormat(Fields.HOUR_TIME_FORMAT).parse(end);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(time2);
            calendar2.add(Calendar.DATE, 1);

            Date d = new SimpleDateFormat(Fields.HOUR_TIME_FORMAT).parse(curent);
            Calendar calendar3 = Calendar.getInstance();
            calendar3.setTime(d);
            calendar3.add(Calendar.DATE, 1);

            Date x = calendar3.getTime();
            if (x.after(calendar1.getTime()) && x.before(calendar2.getTime())) {
                result = true;
            }
        } catch (Exception e) {
            Function.eLog(e.getMessage(), getClass().getName() + ".validUserWorkTimeHours");
        }

        return result;
    }

    private boolean isCategory(String categoty, MUser mUser){
        if(categoty == null || categoty.length() == 0){
            return true;
        }
        return mUser != null && mUser.getSkils() != null && skilsValid(categoty, mUser);
    }

    private boolean skilsValid(String categoty, MUser mUser){
        List<String> list = new ArrayList<>(Arrays.asList(mUser.getSkils().split(",")));
        for (int i = 0; i < list.size(); i++){
            String a = list.get(i);
            if(a.equals(categoty)){
                return true;
            }
        }
        return false;
    }
}

package ua.net.advokat24.repository.firebase.uploadfile;

import android.net.Uri;

public interface UploadFileInteractor {
    void uploadFile(String firebasePath, Uri uri, UploadFileListener listener);
}

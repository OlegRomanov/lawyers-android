package ua.net.advokat24.repository;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class SharedPreferencesManager {

    private static final String PREF_NAME = "t-9947.xml";
    private static final String EXIXT_IN = "t-2482";
    private static final String IS_REGISTER = "t-3442";
    private static final String IS_CLIENT = "t-2782";
    private static final String USER_EMAIL = "t-1982";
    private static final String USER_PASSWD = "t-2562";

    private static SharedPreferencesManager sInstance;
    private final SharedPreferences mPref;

    private SharedPreferencesManager(Context context) {
        mPref = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
    }

    public static synchronized void initializeInstance(Context context) {
        if (sInstance == null) {
            sInstance = new SharedPreferencesManager(context);
        }
    }

    public static synchronized SharedPreferencesManager getInstance(Context context) {
        if (sInstance == null) {
            initializeInstance(context);
        }
        return sInstance;
    }

    public void setExistIn(boolean value) {
        mPref.edit().putBoolean(EXIXT_IN, value)
                .apply();
    }

    public boolean isExistingIn() {
        return mPref.getBoolean(EXIXT_IN, false);
    }

    public void setIsRegister(boolean value) {
        mPref.edit().putBoolean(IS_REGISTER, value)
                .apply();
    }

    public boolean isRegister() {
        return mPref.getBoolean(IS_REGISTER, false);
    }

    public void setClient(boolean value) {
        mPref.edit().putBoolean(IS_CLIENT, value)
                .apply();
    }

    public boolean isClient() {
        return mPref.getBoolean(IS_CLIENT, false);
    }

    public void setUserEmail(String value) {
        mPref.edit().putString(USER_EMAIL, value)
                .apply();
    }

    public String getUserEmail() {
        return mPref.getString(USER_EMAIL, "");
    }

    public void setPasswd(String value) {
        mPref.edit().putString(USER_PASSWD, value)
                .apply();
    }

    public String getPasswd() {
        return mPref.getString(USER_PASSWD, "");
    }


}
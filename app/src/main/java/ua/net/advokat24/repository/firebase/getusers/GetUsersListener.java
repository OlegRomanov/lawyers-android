package ua.net.advokat24.repository.firebase.getusers;

import ua.net.advokat24.util.models.usermodels.MUser;

import java.util.List;

public interface GetUsersListener {
    void resultSearchUsers(List<MUser> lawyers);
    void error();
}

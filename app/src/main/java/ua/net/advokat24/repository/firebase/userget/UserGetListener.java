package ua.net.advokat24.repository.firebase.userget;

import ua.net.advokat24.util.events.EventError;
import ua.net.advokat24.util.events.EventUser;

public interface UserGetListener extends EventUser, EventError {

}

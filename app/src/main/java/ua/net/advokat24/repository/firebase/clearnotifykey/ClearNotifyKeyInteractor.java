package ua.net.advokat24.repository.firebase.clearnotifykey;

import ua.net.advokat24.repository.firebase.categoryimpl.OnCategorysListener;

public interface ClearNotifyKeyInteractor {

    void clearKeys(String key, OnClearKeyListener listener);

}

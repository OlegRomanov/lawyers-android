package ua.net.advokat24.repository.firebase.createtask;

import ua.net.advokat24.util.models.MTask;

public interface CreateTaskInteractor {
    void createTask(MTask mTask, CreateTaskListener listener);
    void close();
}

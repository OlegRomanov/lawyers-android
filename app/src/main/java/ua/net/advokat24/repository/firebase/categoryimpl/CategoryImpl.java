package ua.net.advokat24.repository.firebase.categoryimpl;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import ua.net.advokat24.util.Fields;
import ua.net.advokat24.util.models.CategoryListModel;

import java.util.ArrayList;
import java.util.List;

public class CategoryImpl implements CategorysInteractor {

    @Override
    public void getList(final OnCategorysListener listener) {
        final List<CategoryListModel> list = new ArrayList<>();
        FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database1.getReference(Fields.CATEGORYS_TABLE);
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    CategoryListModel category = postSnapshot.getValue(CategoryListModel.class);
                    list.add(category);
                }
                listener.resultList(list);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(Fields.TAG, getClass().getSimpleName() + " getList Categorys error "+ databaseError.toException());
                listener.error();
            }
        };
        myRef.addValueEventListener(postListener);
    }
}

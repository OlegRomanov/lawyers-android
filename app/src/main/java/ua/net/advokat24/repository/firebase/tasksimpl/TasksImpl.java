package ua.net.advokat24.repository.firebase.tasksimpl;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import ua.net.advokat24.util.Fields;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.MTask;
import ua.net.advokat24.util.utils.FirebaseUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TasksImpl implements TasksInteractor {

    @Override
    public void getList(final String id, final boolean isClient, final TasksListener listener) {
        final List<MTask> list = new ArrayList<>();
        ValueEventListener listen = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    MTask task = postSnapshot.getValue(MTask.class);
                    if(task != null){
                        list.add(task);
                    }
                }
                List<MTask> resultList = new ArrayList<>(list);
                Collections.reverse(resultList);
                listener.resultList(resultList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.error();
            }
        };

        DatabaseReference mDatabase =FirebaseDatabase.getInstance().getReference(Fields.TASKS_TABLE);
        if(isClient){
            mDatabase.orderByChild("clientId").equalTo(id).addListenerForSingleValueEvent(listen);
        } else {
            mDatabase.orderByChild("lawyerId").equalTo(id).addListenerForSingleValueEvent(listen);
        }
    }

    @Override
    public void getTasksRunTime(String id, boolean isClient, final TasksListener listener) {
        final List<MTask> list = new ArrayList<>();
        ValueEventListener listen = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    MTask task = postSnapshot.getValue(MTask.class);
                    if(task != null){
                        list.add(task);
                    }
                }
                List<MTask> resultList = new ArrayList<>(list);
                Collections.reverse(resultList);
                listener.resultList(resultList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.error();
            }
        };

        DatabaseReference mDatabase =FirebaseDatabase.getInstance().getReference(Fields.TASKS_TABLE);
        if(isClient){
            mDatabase.orderByChild("clientId").equalTo(id).addValueEventListener(listen);
        } else {
            mDatabase.orderByChild("lawyerId").equalTo(id).addValueEventListener(listen);
        }
    }
}

package ua.net.advokat24.repository.firebase.uploadfile;

import ua.net.advokat24.util.events.EventError;
import ua.net.advokat24.util.events.EventSaccess;

public interface UploadFileListener extends EventSaccess, EventError {
}

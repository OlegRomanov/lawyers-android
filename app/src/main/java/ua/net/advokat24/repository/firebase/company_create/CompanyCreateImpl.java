package ua.net.advokat24.repository.firebase.company_create;

import android.support.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import ua.net.advokat24.util.Fields;
import ua.net.advokat24.util.models.BaseModel;
import ua.net.advokat24.util.models.MTask;

public class CompanyCreateImpl implements CompanyCreateInteractor {


    @Override
    public void createCompany(final BaseModel model, final CompanyCreateListener listener) {
        ValueEventListener listen = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getChildrenCount() == 0)
                    create(model, listener);
                else
                    listener.isExist();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.error();
            }
        };

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference(Fields.COMPANY_TABLE);
        mDatabase.orderByChild("name").equalTo(model.getName()).addListenerForSingleValueEvent(listen);
    }

    private void create(BaseModel model, final CompanyCreateListener listener){
        ValueEventListener listen = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getChildrenCount() == 0)
                    listener.error();
                else
                    listener.saccess();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.error();
            }
        };


        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference(Fields.COMPANY_TABLE);
        String key = mDatabase.push().getKey();
        mDatabase.child(key).setValue(model);

        FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database1.getReference(Fields.COMPANY_TABLE);
        myRef.child(key).addListenerForSingleValueEvent(listen);
    }
}

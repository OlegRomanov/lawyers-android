package ua.net.advokat24.repository.firebase.categoryimpl;

import ua.net.advokat24.util.models.CategoryListModel;
import ua.net.advokat24.util.models.MTask;

import java.util.List;

public interface OnCategorysListener {
    void resultList(List<CategoryListModel> list);
    void error();
}

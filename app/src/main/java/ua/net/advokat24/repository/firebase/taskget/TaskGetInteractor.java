package ua.net.advokat24.repository.firebase.taskget;

public interface TaskGetInteractor {

    void getTask(String id, TaskGetListener listener);

    void getTaskRunTime(String id, TaskGetListener listener);
}

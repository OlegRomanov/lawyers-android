package ua.net.advokat24.repository.sendpushnotify;

import ua.net.advokat24.repository.sendpushnotify.interfaces.OnDataSendListener;
import ua.net.advokat24.util.models.MTask;

import ua.net.advokat24.repository.sendpushnotify.interfaces.OnDataSendListener;

public interface MessageSendInteractor {
    void sendNotifycation(String data, OnDataSendListener listener);
}

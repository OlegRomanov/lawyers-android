package ua.net.advokat24.repository.sendpushnotify;


import com.google.gson.Gson;
import ua.net.advokat24.repository.retrofit.RetrofitClient;
import ua.net.advokat24.repository.sendpushnotify.interfaces.OnDataSendListener;
import ua.net.advokat24.util.Fields;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.MData;
import ua.net.advokat24.util.models.MNotification;
import ua.net.advokat24.util.models.MPushSender;
import ua.net.advokat24.util.models.MTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.net.advokat24.repository.sendpushnotify.interfaces.OnDataSendListener;
import ua.net.advokat24.util.Function;

public class MessageSendImpl implements MessageSendInteractor {

    @Override
    public void sendNotifycation(String data, final OnDataSendListener listener) {
        Call<String> call = RetrofitClient.getInstance().getApiService().authValidatePin(Fields.KEY_FOR_POST_MESSAGE, data);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    if(listener != null){
                        listener.onSaccessReqwest();
                    }
                } else {
                    Function.eLog("response.isFail() " + new Gson().toJson(response), getClass().getName() + ".sendNotifycation.isSuccessful() == null");
                    if(listener != null){
                        listener.onFailedReqwest("response.isSuccessful() == null");
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Function.eLog("onFailure " + t.getMessage(), getClass().getName() + ".sendNotifycation.onFailure");
                listener.onFailedReqwest(t.getMessage());
            }
        });
    }
}
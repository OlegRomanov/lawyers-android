package ua.net.advokat24.repository.firebase.userupdate;

import ua.net.advokat24.util.events.EventError;
import ua.net.advokat24.util.events.EventSaccess;
import ua.net.advokat24.util.events.EventUser;
import ua.net.advokat24.util.models.usermodels.MUser;

public interface UserUpdateListener extends EventError, EventUser {

}

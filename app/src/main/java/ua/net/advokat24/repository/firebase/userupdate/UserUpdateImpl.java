package ua.net.advokat24.repository.firebase.userupdate;

import android.net.Uri;
import android.support.annotation.NonNull;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import ua.net.advokat24.repository.firebase.clearnotifykey.ClearNotifyKeyImpl;
import ua.net.advokat24.repository.firebase.clearnotifykey.ClearNotifyKeyInteractor;
import ua.net.advokat24.repository.firebase.clearnotifykey.OnClearKeyListener;
import ua.net.advokat24.repository.firebase.uploadfile.UploadFileImpl;
import ua.net.advokat24.repository.firebase.uploadfile.UploadFileInteractor;
import ua.net.advokat24.repository.firebase.uploadfile.UploadFileListener;
import ua.net.advokat24.util.Fields;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.usermodels.MUser;
import ua.net.advokat24.util.utils.FirebaseUtils;

import ua.net.advokat24.util.Function;

public class UserUpdateImpl implements UserUpdateInteractor {

    @Override
    public void updateUser(MUser mUser, UserUpdateListener listener) {
        finalUpdate(mUser, listener);
    }


    @Override
    public void updateUser(final MUser mUser, final Uri userPhoto, final UserUpdateListener listener) {
        UploadFileInteractor interactor = new UploadFileImpl();
        interactor.uploadFile(Fields.USER_PHOTO_PATH, userPhoto, new UploadFileListener() {
            @Override
            public void error(String error) {
                listener.error(error);
            }

            @Override
            public void saccess() {
                finalUpdate(mUser, listener);
            }
        });
    }

    @Override
    public void updateUser(final MUser mUser, final Uri userPhoto, final Uri documentsPhoto, final UserUpdateListener listener) {
        final UploadFileInteractor interactor = new UploadFileImpl();
        if(userPhoto != null){
            interactor.uploadFile(Fields.USER_PHOTO_PATH, userPhoto, new UploadFileListener() {
                @Override
                public void error(String error) {
                    listener.error(error);
                }
                @Override
                public void saccess() {
                    if(documentsPhoto != null){
                        interactor.uploadFile(Fields.DOCUMENT_PHOTO_PATH, documentsPhoto, new UploadFileListener() {
                            @Override
                            public void error(String error) {
                                listener.error(error);
                            }

                            @Override
                            public void saccess() {
                                finalUpdate(mUser, listener);
                            }
                        });
                    } else {
                        finalUpdate(mUser, listener);
                    }
                }
            });
        } else {
            finalUpdate(mUser, listener);
        }
    }

    private void finalUpdate(MUser mUser, final UserUpdateListener listener){
        if(mUser.getNotificationKay() == null || !mUser.getNotificationKay().equals(FirebaseInstanceId.getInstance().getToken())){
            mUser.setNotificationKay(FirebaseInstanceId.getInstance().getToken());
        }
        if(mUser.getId() == null || !mUser.getId().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())){
            mUser.setId(FirebaseAuth.getInstance().getCurrentUser().getUid());
        }
        DatabaseReference mDatabase = FirebaseUtils.getDatabase().getReference(Fields.USERS_TABTE);
        mDatabase.child(mUser.getId()).setValue(mUser);
        mDatabase.child(mUser.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                try {
                    if (snapshot.getValue() != null) {
                        try {
                            final MUser mUser = snapshot.getValue(MUser.class);
                            listener.user(mUser);

                        } catch (Exception e) {
                            Function.eLog(e.getMessage(), getClass().getName());
                            listener.error(e.getMessage());
                        }
                    } else {
                        Function.eLog("User not Updated", getClass().getName());
                        listener.error("User not Updated");
                    }
                } catch (Exception e) {
                    Function.eLog(e.getMessage(), getClass().getName());
                    listener.error(e.getMessage());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Function.eLog(databaseError.getMessage(), getClass().getName());
                listener.error(databaseError.getMessage());
            }
        });
    }
}

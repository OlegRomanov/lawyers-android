package ua.net.advokat24.util.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class MTask implements Parcelable {

    public MTask() { }

    private String id;
    private String clientId;
    private String lawyerId;
    private String clientPhoto;
    private String lawyerPhoto;
    private String clientName;
    private String lawyerName;
    private String descriprion;
    private String data;
    private String category;
    private String locatoinLat;
    private String locatoinLon;
    private String clientPhone;

    private boolean isActive;
    private boolean answerReqwest;
    private boolean isPremium;
    private boolean isPremiumRun;
    private boolean isClosed;

    protected MTask(Parcel in) {
        id = in.readString();
        clientId = in.readString();
        lawyerId = in.readString();
        clientPhoto = in.readString();
        lawyerPhoto = in.readString();
        clientName = in.readString();
        lawyerName = in.readString();
        descriprion = in.readString();
        data = in.readString();
        category = in.readString();
        locatoinLat = in.readString();
        locatoinLon = in.readString();
        clientPhone = in.readString();
        isActive = in.readByte() != 0;
        answerReqwest = in.readByte() != 0;
        isPremium = in.readByte() != 0;
        isPremiumRun = in.readByte() != 0;
        isClosed = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(clientId);
        dest.writeString(lawyerId);
        dest.writeString(clientPhoto);
        dest.writeString(lawyerPhoto);
        dest.writeString(clientName);
        dest.writeString(lawyerName);
        dest.writeString(descriprion);
        dest.writeString(data);
        dest.writeString(category);
        dest.writeString(locatoinLat);
        dest.writeString(locatoinLon);
        dest.writeString(clientPhone);
        dest.writeByte((byte) (isActive ? 1 : 0));
        dest.writeByte((byte) (answerReqwest ? 1 : 0));
        dest.writeByte((byte) (isPremium ? 1 : 0));
        dest.writeByte((byte) (isPremiumRun ? 1 : 0));
        dest.writeByte((byte) (isClosed ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MTask> CREATOR = new Creator<MTask>() {
        @Override
        public MTask createFromParcel(Parcel in) {
            return new MTask(in);
        }

        @Override
        public MTask[] newArray(int size) {
            return new MTask[size];
        }
    };

    public boolean isPremiumRun() {
        return isPremiumRun;
    }

    public void setPremiumRun(boolean premiumRun) {
        isPremiumRun = premiumRun;
    }

    public String getLocatoinLat() {
        return locatoinLat;
    }

    public void setLocatoinLat(String locatoinLat) {
        this.locatoinLat = locatoinLat;
    }

    public String getLocatoinLon() {
        return locatoinLon;
    }

    public void setLocatoinLon(String locatoinLon) {
        this.locatoinLon = locatoinLon;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public void setClosed(boolean closed) {
        isClosed = closed;
    }

    public String getClientPhone() {
        return clientPhone;
    }

    public void setClientPhone(String clientPhone) {
        this.clientPhone = clientPhone;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isAnswerReqwest() {
        return answerReqwest;
    }

    public void setAnswerReqwest(boolean answerReqwest) {
        this.answerReqwest = answerReqwest;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }


    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getLawyerId() {
        return lawyerId;
    }

    public void setLawyerId(String lawyerId) {
        this.lawyerId = lawyerId;
    }

    public String getClientPhoto() {
        return clientPhoto;
    }

    public void setClientPhoto(String clientPhoto) {
        this.clientPhoto = clientPhoto;
    }

    public String getLawyerPhoto() {
        return lawyerPhoto;
    }

    public void setLawyerPhoto(String lawyerPhoto) {
        this.lawyerPhoto = lawyerPhoto;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getLawyerName() {
        return lawyerName;
    }

    public void setLawyerName(String lawyerName) {
        this.lawyerName = lawyerName;
    }

    public String getDescriprion() {
        return descriprion;
    }

    public void setDescriprion(String descriprion) {
        this.descriprion = descriprion;
    }

    public boolean isPremium() {
        return isPremium;
    }

    public void setPremium(boolean premium) {
        isPremium = premium;
    }

}

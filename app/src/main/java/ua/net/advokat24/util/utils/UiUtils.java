package ua.net.advokat24.util.utils;

import android.view.View;
import android.widget.ImageView;

import ua.net.advokat24.MyApp;
import ua.net.advokat24.R;
import ua.net.advokat24.repository.SharedPreferencesManager;

import ua.net.advokat24.repository.SharedPreferencesManager;

public class UiUtils {

    public static final int HOME_FRAGMENT = 1;
    public static final int LIST_FRAGMENT = 2;
    public static final int FILTER_FRAGMENT = 3;
    public static final int USER_FRAGMENT = 4;
    public static final int START = 5;

    public static void setBottomBarStatus(int status, ImageView iv_home, ImageView iv_list, ImageView iv_filter, ImageView iv_profile){
        iv_home.setImageResource(status == HOME_FRAGMENT? R.drawable.ic_home_check_on:R.drawable.ic_home_check_off);
        iv_profile.setImageResource(status == USER_FRAGMENT?R.drawable.ic_user_check_on:R.drawable.ic_user_check_off);

        if(SharedPreferencesManager.getInstance(MyApp.getContext()).isClient()){
            iv_list.setImageResource(status == LIST_FRAGMENT?R.drawable.ic_list_check_on:R.drawable.ic_list_check_off);
        } else {
            iv_list.setImageResource(status == LIST_FRAGMENT?R.drawable.ic_tasks_on:R.drawable.ic_tasks_off);
        }
        if(SharedPreferencesManager.getInstance(MyApp.getContext()).isClient()){
            iv_filter.setImageResource(status == FILTER_FRAGMENT?R.drawable.ic_filter_check_on:R.drawable.ic_filter_check_off);
            iv_filter.setVisibility(View.VISIBLE);
        } else {
            iv_filter.setVisibility(View.GONE);
        }
    }
}

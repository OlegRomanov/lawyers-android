package ua.net.advokat24.util.utils;

import ua.net.advokat24.MyApp;
import ua.net.advokat24.R;
import ua.net.advokat24.util.models.BaseModel;

public class UserCompanyStatusUtils {

    private static final int USER_STATUS_SINGLE = 1;
    private static final int USER_STATUS_GROUP = 2;
    private static final int USER_STATUS_COMPANY = 3;

    private static final String NAME_STATUS_SINGLE = MyApp.getContext().getResources().getString(R.string.status_single);
    private static final String NAME_STATUS_GROUP = MyApp.getContext().getResources().getString(R.string.status_groupe);
    private static final String NAME_STATUS_COMPANY = MyApp.getContext().getResources().getString(R.string.status_company);

    public static int setStatusToUser(int viewId){
        int result = 0;
        switch (viewId){
            case R.id.rb_single:
                result = USER_STATUS_SINGLE;
                break;
            case R.id.rb_groupe:
                result = USER_STATUS_GROUP;
                break;
            case R.id.rb_company:
                result = USER_STATUS_COMPANY;
                break;
        }
        return result;
    }

    public static String printStatus(int viewId){
        String result = "";
        switch (viewId){
            case USER_STATUS_SINGLE:
                result = NAME_STATUS_SINGLE;
                break;
            case USER_STATUS_GROUP:
                result = NAME_STATUS_GROUP;
                break;
            case USER_STATUS_COMPANY:
                result = NAME_STATUS_COMPANY;
                break;
        }
        return result;
    }
}

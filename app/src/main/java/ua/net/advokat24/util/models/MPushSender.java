package ua.net.advokat24.util.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MPushSender {

    public MPushSender() {}

    public MPushSender(String to, MNotification notification, MData data) {
        this.to = to;
        this.notification = notification;
        this.data = data;
        collapseKey = "type_a";
    }

    @SerializedName("to")
    @Expose
    private String to;
    @SerializedName("collapse_key")
    @Expose
    private String collapseKey;
    @SerializedName("notification")
    @Expose
    private MNotification notification;
    @SerializedName("data")
    @Expose
    private MData data;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getCollapseKey() {
        return collapseKey;
    }

    public void setCollapseKey(String collapseKey) {
        this.collapseKey = collapseKey;
    }

    public MNotification getNotification() {
        return notification;
    }

    public void setNotification(MNotification notification) {
        this.notification = notification;
    }

    public MData getData() {
        return data;
    }

    public void setData(MData data) {
        this.data = data;
    }


}

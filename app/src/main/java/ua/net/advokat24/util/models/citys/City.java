package ua.net.advokat24.util.models.citys;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class City {

   @SerializedName("lat")
   @Expose
   private String lat;
   @SerializedName("lon")
   @Expose
   private String lon;
   @SerializedName("name")
   @Expose
   private String name;

   private boolean chach = false;

   public boolean isChach() {
      return chach;
   }

   public void setChach(boolean chach) {
      this.chach = chach;
   }

   public String getLat() {
      return lat;
   }

   public void setLat(String lat) {
      this.lat = lat;
   }

   public String getLon() {
      return lon;
   }

   public void setLon(String lon) {
      this.lon = lon;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getId(){
      return lat + lon + name;
   }
}

package ua.net.advokat24.util.models;

public class MassegFromService {

    public static final int ACTION_LAWYERS_DEFOULT = 0;
    public static final int ACTION_LAWYERS_NOT_FOUND = 1;
    public static final int ACTION_LAWYERS_IS_FOUND = 2;
    public static final int ACTION_FOUND_NEW_TASK = 3;
    public static final int ACTION_CLIENT_PAY_FOR_TASK = 4;

    public MassegFromService(int action, String postId) {
        this.action = action;
        this.postId = postId;
    }

    private boolean saccess;
    private int action;
    private String postId;

    public void setSaccess(boolean saccess) {
        this.saccess = saccess;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public boolean isSaccess(){
        return saccess;
    }
}

package ua.net.advokat24.util.utils;

import android.content.res.AssetManager;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import ua.net.advokat24.MyApp;
import ua.net.advokat24.util.models.BaseModel;
import ua.net.advokat24.util.models.citys.City;
import ua.net.advokat24.util.models.citys.GeoManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import fr.arnaudguyon.xmltojsonlib.XmlToJson;
import ua.net.advokat24.util.models.BaseModel;
import ua.net.advokat24.util.models.citys.City;
import ua.net.advokat24.util.models.citys.GeoManager;

public class CitysUtils {

    private static List<BaseModel> list;
    private static List<City> citys;

    private static List<BaseModel> getBuseList(){
        citys = new ArrayList<>();
        list = new ArrayList<>();
        AssetManager assetManager = MyApp.getContext().getAssets();
        try {
            InputStream inputStream = assetManager.open("city.xml");
            XmlToJson xmlToJson = new XmlToJson.Builder(inputStream, null).build();
            inputStream.close();
            GeoManager model = new Gson().fromJson(xmlToJson.toString(), GeoManager.class);
            citys = model.getRegions().getCity();

            Collections.sort(citys, new Comparator<City>() {
                 @Override
                 public int compare(City item, City t1) {
                     String s1 = item.getName();
                     String s2 = t1.getName();
                     return s1.compareToIgnoreCase(s2);
                 }
            });



        } catch (IOException e) {
            e.printStackTrace();
        }
        for(int i = 0; i < citys.size(); i++ ){
            list.add(new BaseModel(citys.get(i).getId(), citys.get(i).getName()));
        }


        Collections.sort(list, new Comparator<BaseModel>() {
            @Override
            public int compare(BaseModel item, BaseModel t1) {
                String s1 = item.getName();
                String s2 = t1.getName();
                return s1.compareToIgnoreCase(s2);
            }
        });

        return list;
    }


    public static List<BaseModel> getList() {
        if (list == null) {
            list = getBuseList();
        }
        return list;
    }

    public static List<City> getListCitys() {
        if (citys == null) {
            list = getBuseList();
        }
        return citys;
    }

    public static LatLng getCityCordinate(String cityName){
        LatLng latlon = null;


        for (int i = 0; i < getListCitys().size(); i++){
            if(cityName.equals(getListCitys().get(i).getName())){
                latlon = new LatLng(Double.parseDouble(getListCitys().get(i).getLat()), Double.parseDouble(getListCitys().get(i).getLon()));
            }
        }
        return latlon;

    }
}

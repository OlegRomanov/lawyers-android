package ua.net.advokat24.util.models.message_firebase;

import ua.net.advokat24.util.models.MassegFromService;

import ua.net.advokat24.util.models.MassegFromService;

public class Notification {

    public Notification() {

    }

    public Notification(int action) {
        switch (action){
            case MassegFromService.ACTION_CLIENT_PAY_FOR_TASK:
                title = "Клиент утвердил вашу кандидатуру";
                body = "Клиент оплатил дело и указал номер телефона, свяжитесь с ним";
                click_action = "TASK_ACTIVITY";
                break;
            case MassegFromService.ACTION_FOUND_NEW_TASK:
                title = "Вас ждёт новое дело";
                body = "Мы подобрали вам новое дело";
                click_action = "OPEN_CONFIRM_ACTIVITY";
                break;
        }

    }

    private String body;
    private String title;
    private String sound = "default";
    private String click_action;


    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public String getClick_action() {
        return click_action;
    }

    public void setClick_action(String click_action) {
        this.click_action = click_action;
    }
}

package ua.net.advokat24.util.utils.curent_location;

import android.app.Activity;

public interface LocationInteractor {

    void getCurentLocation(Activity activity, LocationListener listener);
}

package ua.net.advokat24.util;

public class Fields {


    /*LOG*/
    public static final String TAG = "misha";


    /*FIREBASE TABLES*/
    public static final String USERS_TABTE = "users";
    public static final String TASKS_TABLE = "tasks";
    public static final String CATEGORYS_TABLE = "category";
    public static final String COMPANY_TABLE = "company";
    public static final String USER_PHOTO_PATH = "userphoto";
    public static final String DOCUMENT_PHOTO_PATH = "document";


    /*TIME*/
    public static final String FULL_TIME_FORMAT = "yyyy.MM.dd HH:mm:ss";
    public static final String HOUR_TIME_FORMAT = "HH:mm";

    /*NOTIFICATION KEY FOR POST MESSAGE*/
    public static final String KEY_FOR_POST_MESSAGE = "key=AAAA8q9ZJyA:APA91bGTWDeqhd8e6lx2WXrcUpLCZyRXwRk3n6KQwtZA4vLz41XrQLHBk4mTy5T_kOQeqtddoMqIYyRlZx4P4-sp4LEbBVNuLWEG80eVqOLc7_Z7Uo1bP8b7lPvpic7pubrG0lWeXsRz";

    /*PAY*/
    public static final String PRIVATE_KEY_FOR_PAY = "ehKttSg8MrY3TGhBw4eegcl2pmBG92Lg840m2eNF";
    public static final String PUBLIC_KEY_FOR_PAY = "i28795142293";

}

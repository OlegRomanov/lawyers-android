package ua.net.advokat24.util.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.google.firebase.storage.FirebaseStorage;
import ua.net.advokat24.R;
import ua.net.advokat24.util.GlideApp;
import ua.net.advokat24.util.models.usermodels.MUser;

import ua.net.advokat24.util.models.usermodels.MUser;

public class ImageUtils {

    public static synchronized void loadPhoto(Context context, MUser mUser, ImageView imageView) {
        imageView.setImageDrawable(null);
        if (mUser.getPhoto() != null) {
            GlideApp.with(context)
                    .load(mUser.getPhoto())
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .transform(new MultiTransformation<Bitmap>(new CircleCrop()))
                    .into(imageView);

        } else {
            GlideApp.with(context)
                    .load(FirebaseStorage.getInstance().getReference().child("images/userphoto/" + mUser.getId() + ".jpg"))
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .transform(new MultiTransformation<Bitmap>(new CircleCrop()))
                    .error(R.drawable.ic_user_check_off)
                    .into(imageView);
        }
    }

    public static synchronized void loadPhoto(Context context, String photo, String id, ImageView imageView) {
        imageView.setImageDrawable(null);
        if (photo != null) {
            imageView.setImageDrawable(null);
            GlideApp.with(context)
                    .load(photo)
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .transform(new MultiTransformation<Bitmap>(new CircleCrop()))
                    .into(imageView);
        } else {
            GlideApp.with(context)
                    .load(FirebaseStorage.getInstance().getReference().child("images/userphoto/" + id + ".jpg"))
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .transform(new MultiTransformation<Bitmap>(new CircleCrop()))
                    .error(R.drawable.ic_user_check_off)
                    .into(imageView);
        }
    }

}

package ua.net.advokat24.util.models.message_firebase;

import ua.net.advokat24.util.models.MassegFromService;
import ua.net.advokat24.util.models.message_firebase.Data;
import ua.net.advokat24.util.models.message_firebase.Message;

import ua.net.advokat24.util.models.MassegFromService;

public class MessageUtils {

    public static final int ACTION_CONFIRM_TASK = 0;
    public static final int ACTION_FINISH_TASK = 1;

    public static Message getMessage(String id, String notifyKey, int action){


        Message message = new Message();
        Data data = new Data();
        Notification notification = new Notification();
        switch (action){
            case MassegFromService.ACTION_CLIENT_PAY_FOR_TASK:


                break;

            case MassegFromService.ACTION_FOUND_NEW_TASK:






                break;
        }




        return new Message();
    }

    public static String taskDone(String id, String notifyKey){
        return "{\n" +
                " \"to\" : \"" + notifyKey + "\"\n" +
                " \"priority\" : \"high\"\n" +
                " \"collapse_key\" : \"type_a\",\n" +
                " \"notification\" : {\n" +
                "     \"body\" : \"" + "Клиент утвердил вашу кандидатуру" + "\",\n" +
                "     \"title\": \"" + "Клиент оплатил услугу и указал номер телефона, свяжитесь с ним" + "\",\n" +
                "     \"sound\": \"default\",\n" +
                " \t \"click_action\": \"TASK_ACTIVITY\"\n" +
                " \n" +
                " },\n" +
                " \"data\": {\n" +
                " \t\"extra\":\"" + id + "\",\n" +
                " \t\"action\":\"" + MassegFromService.ACTION_CLIENT_PAY_FOR_TASK + "\"\n" +
                "}\n" +
                "}";
    }

    public static String proposeTask(String id, String notifyKey){
        return "{\n" +
                " \"to\" : \"" + notifyKey + "\"\n" +
                " \"priority\" : \"high\"\n" +
                " \"collapse_key\" : \"type_a\",\n" +
                " \"notification\" : {\n" +
                "     \"body\" : \"" + "Вас ждёт новое дело." + "\",\n" +
                "     \"title\": \"" + "Мы подобрали вам дело." + "\",\n" +
                "     \"sound\": \"default\",\n" +
                " \t \"click_action\": \"OPEN_CONFIRM_ACTIVITY\"\n" +
                " \n" +
                " },\n" +
                " \"data\": {\n" +
                " \t\"extra\":\"" + id + "\",\n" +
                " \t\"action\":\"" + MassegFromService.ACTION_FOUND_NEW_TASK + "\"\n" +
                "}\n" +
                "}";
    }
}

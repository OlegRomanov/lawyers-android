package ua.net.advokat24.util.models;

public class BaseModel {

   public BaseModel() {
   }

   public BaseModel(String id, String name) {
      Id = id;
      this.name = name;
   }

   private String Id;
   private String name;


   public String getId() {
      return Id;
   }

   public void setId(String id) {
      Id = id;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }
}

package ua.net.advokat24.util.utils.pay_utils;

import ua.net.advokat24.MyApp;
import ua.net.advokat24.util.Fields;
import ua.net.advokat24.util.Function;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.UUID;

import ua.net.advokat24.util.Function;
import ua.privatbank.paylibliqpay.ErrorCode;
import ua.privatbank.paylibliqpay.LiqPay;
import ua.privatbank.paylibliqpay.LiqPayCallBack;

public class PayImpl implements PayInteractor {

    @Override
    public void payForTask(String sum, final PayListener listener) {
        String uniqueID = UUID.randomUUID().toString();
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("public_key", Fields.PUBLIC_KEY_FOR_PAY);
        map.put("action", "auth");
        map.put("amount", sum);
        map.put("currency", "UAH");
        map.put("description", "Оплата за пользование программой");
        map.put("order_id", uniqueID);
        map.put("language", "ru");
        map.put("server_url", "https://advokat24.net.ua");
        LiqPay.checkout(MyApp.getContext(), map, Fields.PRIVATE_KEY_FOR_PAY, new LiqPayCallBack() {
            @Override
            public void onResponseSuccess(String s) {
                JSONObject object = null;
                if(listener != null){
                    try {
                        object = new JSONObject(s);
                    } catch (JSONException e) {
                        listener.error();
                        Function.eLog(e.getMessage(), getClass().getName() + ".payForTask.onResponseSuccess");
                    }
                    if("success".equals(object.optString("status"))){
                        listener.saccess();
                    }else {
                        listener.error();
                        Function.eLog("Ошибка оплаты", getClass().getName() + ".payForTask.onResponseSuccess");
                    }
                }

            }

            @Override
            public void onResponceError(ErrorCode errorCode) {
                listener.error();
                Function.eLog("Ошибка оплаты" + errorCode.name(), getClass().getName());
            }
        });
    }
}
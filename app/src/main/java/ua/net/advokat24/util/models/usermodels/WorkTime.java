package ua.net.advokat24.util.models.usermodels;

import android.content.Context;

import ua.net.advokat24.MyApp;
import ua.net.advokat24.R;

public class WorkTime {

    private String timeStart = "09:00";
    private String timeEnd = "18:00";
    private int prise = 0;

//    private String timeDoubleStart = "19:00";
//    private String timeDoubleEnd = "22:00";
//    private int priseDouble = 0;

    private boolean monday = true;
    private boolean tuesday = true;
    private boolean wednesday = true;
    private boolean thursday = true;
    private boolean friday = true;
    private boolean saturday = false;
    private boolean sunday = false;

    public WorkTime() {
    }

    public int getPrise() {
        return prise;
    }

    public void setPrise(int prise) {
        this.prise = prise;
    }

//    public String getTimeDoubleStart() {
//        return timeDoubleStart;
//    }
//
//    public void setTimeDoubleStart(String timeDoubleStart) {
//        this.timeDoubleStart = timeDoubleStart;
//    }
//
//    public String getTimeDoubleEnd() {
//        return timeDoubleEnd;
//    }
//
//    public void setTimeDoubleEnd(String timeDoubleEnd) {
//        this.timeDoubleEnd = timeDoubleEnd;
//    }
//
//    public int getPriseDouble() {
//        return priseDouble;
//    }
//
//    public void setPriseDouble(int priseDouble) {
//        this.priseDouble = priseDouble;
//    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public boolean isMonday() {
        return monday;
    }

    public void setMonday(boolean monday) {
        this.monday = monday;
    }

    public boolean isTuesday() {
        return tuesday;
    }

    public void setTuesday(boolean tuesday) {
        this.tuesday = tuesday;
    }

    public boolean isWednesday() {
        return wednesday;
    }

    public void setWednesday(boolean wednesday) {
        this.wednesday = wednesday;
    }

    public boolean isThursday() {
        return thursday;
    }

    public void setThursday(boolean thursday) {
        this.thursday = thursday;
    }

    public boolean isFriday() {
        return friday;
    }

    public void setFriday(boolean friday) {
        this.friday = friday;
    }

    public boolean isSaturday() {
        return saturday;
    }

    public void setSaturday(boolean saturday) {
        this.saturday = saturday;
    }

    public boolean isSunday() {
        return sunday;
    }

    public void setSunday(boolean sunday) {
        this.sunday = sunday;
    }

    public String toStringMy(Context context){
        String result = "C " + timeStart + " До " + timeEnd;
        if(monday)
            result = result + "\n - " + context.getResources().getString(R.string.time_day_monday);
        if(tuesday)
            result = result + "\n - " + context.getResources().getString(R.string.time_day_tuesdey);
        if(wednesday)
            result = result + "\n - " + context.getResources().getString(R.string.time_day_whenthday);
        if(thursday)
            result = result + "\n - " + context.getResources().getString(R.string.time_day_thursday);
        if(friday)
            result = result + "\n - " + context.getResources().getString(R.string.time_day_friday);
        if(saturday)
            result = result + "\n - " + context.getResources().getString(R.string.time_day_satarday);
        if(sunday)
            result = result + "\n - " + context.getResources().getString(R.string.time_day_sundey);




        return result;
    }
}

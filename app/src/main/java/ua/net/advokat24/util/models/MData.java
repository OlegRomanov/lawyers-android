package ua.net.advokat24.util.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MData {

    public MData() {
    }

    public MData(int res) {
        if (res > 0)
            body = "body";
            title = "title";
            key1 = "key1";
            key2 = "key2";

    }

    @SerializedName("body")
    @Expose
    private String body;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("key_1")
    @Expose
    private String key1;
    @SerializedName("key_2")
    @Expose
    private String key2;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKey1() {
        return key1;
    }

    public void setKey1(String key1) {
        this.key1 = key1;
    }

    public String getKey2() {
        return key2;
    }

    public void setKey2(String key2) {
        this.key2 = key2;
    }

}

package ua.net.advokat24.util.utils;

import android.widget.TextView;

import ua.net.advokat24.MyApp;
import ua.net.advokat24.R;
import ua.net.advokat24.util.models.BaseModel;

import java.util.ArrayList;
import java.util.List;

import ua.net.advokat24.util.models.BaseModel;

public class UserStatusUtils {

    private static final String NAME_STATUS_FREE = MyApp.getContext().getResources().getString(R.string.status_free);
    private static final String NAME_STATUS_BUSY = MyApp.getContext().getResources().getString(R.string.status_busy);
//    private static final String NAME_STATUS_EXTRA_BUSY = MyApp.getContext().getResources().getString(R.string.status_extra);
//    private static final String NAME_STATUS_WORK_TIME = MyApp.getContext().getResources().getString(R.string.work_time);

    public static final int USER_STATUS_FREE = 1;
    public static final int USER_STATUS_BUSY = 2;
    public static final int USER_STATUS_EXTRA_BUSY = 3;
    public static final int USER_STATUS_WORK_TIME = 4;

    public static void setStatus(int status, TextView view){
        switch (status){
            case USER_STATUS_FREE:
                view.setText(NAME_STATUS_FREE);
                break;
            case USER_STATUS_WORK_TIME:
            case USER_STATUS_EXTRA_BUSY:
            case USER_STATUS_BUSY:
                view.setText(NAME_STATUS_BUSY);
                break;
//            case USER_STATUS_WORK_TIME:
//                view.setText(NAME_STATUS_WORK_TIME);
//                break;
//            case USER_STATUS_EXTRA_BUSY:
//                view.setText(NAME_STATUS_EXTRA_BUSY);
//                break;
        }
    }

    public static List<BaseModel> getStatusList(){
        List<BaseModel> list = new ArrayList<>();
        list.add(new BaseModel(String.valueOf(USER_STATUS_BUSY), NAME_STATUS_BUSY));
        list.add(new BaseModel(String.valueOf(USER_STATUS_FREE), NAME_STATUS_FREE));
//        list.add(new BaseModel(String.valueOf(USER_STATUS_EXTRA_BUSY), NAME_STATUS_EXTRA_BUSY));
//        list.add(new BaseModel(String.valueOf(USER_STATUS_WORK_TIME), NAME_STATUS_WORK_TIME));
        return list;
    }
}

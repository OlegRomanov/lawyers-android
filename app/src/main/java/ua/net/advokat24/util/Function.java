package ua.net.advokat24.util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.TextInputEditText;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import ua.net.advokat24.MyApp;
import ua.net.advokat24.R;
import ua.net.advokat24.util.models.CategoryListModel;

import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class Function {

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static boolean checkEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    private static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );

    public static int getWordsCount(String text) {
        if (text == null || text.isEmpty()) {
            return 0;
        }
        StringTokenizer tokens = new StringTokenizer(text);
        return tokens.countTokens();
    }

    public static void setNewView(final View view, final boolean show) {
        if(show){
            view.setVisibility(View.VISIBLE);
        }
        view.animate()
                .alpha(show?1f:0f)
                .setDuration(500)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if(!show){
                            view.setVisibility(View.GONE);
                        }
                    }
                });
    }

    public static List<String> getListCategory(String categorys){
        return new ArrayList<String>(Arrays.asList(categorys.split(", ")));
    }

    public static String getCategoryString(List<String> list){
        String str = Arrays.toString(list.toArray());
        str = str.replace("[", "");
        str = str.replace("]", "");
        return str;
    }

    public static void printSkils(String data, TextInputEditText et_category, List<CategoryListModel> list){
        int category = Integer.parseInt(data);
        et_category.setText(list.get(category).getName());
    }

    public static String getStringSkilsMultiple(String data, List<CategoryListModel> list){
        if(data == null || data.length() == 0){
            return "";
        }

        String result = "";
        List<String>curentList = new ArrayList<>(Arrays.asList(data.split(",")));
        Collections.sort(curentList, new Comparator<String>() {
            @Override
            public int compare(String item, String t1) {
                return item.compareToIgnoreCase(t1);
            }
        });

        String curent = "";
        for (int i = 0; i < curentList.size();i++){
            String skil = curentList.get(i);

            String a = "";
            if(!curent.equals(list.get(Integer.parseInt(skil)).getName())){
                curent = list.get(Integer.parseInt(skil)).getName();
                a = a + " - " + list.get(Integer.parseInt(skil)).getName();
                if(i  != curentList.size() -1)
                    a = a + "\n";

            }
            result = result + a;
        }
        return result;
    }

    public static String getStringSkils(String data, List<CategoryListModel> list){
        if(data == null || data.length() == 0){
            return "";
        }
        int category = Integer.parseInt(data);
        return list.get(category).getName();
    }

    public static String getStringProfileSkils(String data, List<CategoryListModel> list){
        int category = Integer.parseInt(data);
        return list.get(category).getName();
    }

    public static String getDataAppFormat(String time, Context context) {

        String datalable = "";

        try {
            if (time != null && time.length() != 0) {
                SimpleDateFormat sdf = new SimpleDateFormat(Fields.FULL_TIME_FORMAT);
                String currentDateandTime = sdf.format(new Date());
                Date dateold = null;
                Date datethis = null;
                try {
                    dateold = sdf.parse(time);
                    datethis = sdf.parse(currentDateandTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                long minutes = TimeUnit.MINUTES.convert(datethis.getTime() - dateold.getTime(), TimeUnit.MILLISECONDS);

                if (minutes < 1) {
                    datalable = context.getResources().getString(R.string.time_moment_ago);
                } else if (minutes >= 1 && minutes < 60) {
                    if (minutes == 1) {
                        datalable = context.getResources().getString(R.string.time_minute_ago);
                    } else {
                        datalable = minutes + " " + context.getResources().getString(R.string.time_minutes_ago);
                    }
                } else if (minutes >= 60 && minutes < 1440) { // 1140 minute = 24 hours
                    minutes = (int) minutes / 60; // how many hours
                    if (minutes == 1) {
                        datalable = context.getResources().getString(R.string.time_hour_ago);
                    } else {
                        datalable = minutes + " " + context.getResources().getString(R.string.time_hours_ago);
                    }
                } else if (minutes >= 1440 && minutes < 10080) { // 10080 minutes = 1 week
                    minutes = (int) minutes / 60 / 24; // how many days
                    if (minutes == 1) {
                        datalable = context.getResources().getString(R.string.time_day_ago);
                    } else {
                        datalable = minutes + " " + context.getResources().getString(R.string.time_days_ago);
                    }
                } else if (minutes >= 10080 && minutes < 43200) { // 43200 minutes = 1 month
                    minutes = (int) minutes / 60 / 24 / 7; // how many weeks
                    if (minutes == 1) {
                        datalable = context.getResources().getString(R.string.time_week_ago);
                    } else {
                        datalable = minutes + " " + context.getResources().getString(R.string.time_weeks_ago);
                    }
                } else if (minutes >= 43200 && minutes < 518400) { // 518400 minutes = 1 year
                    minutes = (int) minutes / 60 / 24 / 7 / 4; // how many months
                    if (minutes == 1) {
                        datalable = context.getResources().getString(R.string.time_month_ago);
                    } else {
                        datalable = minutes + " " + context.getResources().getString(R.string.time_months_ago);
                    }
                } else if (minutes >= 518400) {
                    minutes = (int) minutes / 60 / 24 / 7 / 4 / 12; // how many years
                    if (minutes == 1) {
                        datalable = context.getResources().getString(R.string.time_year_ago);
                    } else {
                        datalable = context.getResources().getString(R.string.time_years_ago);
                    }
                }
            }
        } catch (Exception e) {
            datalable = time;
        }
        return datalable;
    }




    public static ProgressDialog startProgress(Context context){
        return startProgress(context, false);
    }

    public static ProgressDialog startProgress(Context context, boolean cancelable){
        return ProgressDialog.show(context,
                context.getResources().getString(R.string.loading),
                context.getResources().getString(R.string.loading_disc),
                cancelable, cancelable);
    }


    public static void showToast(){
        showToast("В разработке");
    }

    public static void showToast(String text){
        Toast toast = Toast.makeText(MyApp.getContext(), text, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP| Gravity.CENTER_HORIZONTAL, 0, 60);
        toast.show();
    }

//    public static void showToast(Context context){
//        showToast(context, "В разработке");
//    }
//
//    public static void showToast(Context context, String text){
//        Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
//        toast.setGravity(Gravity.TOP| Gravity.CENTER_HORIZONTAL, 0, 60);
//        toast.show();
//    }

    public static void log(String text, String location){
        Log.d(Fields.TAG, " " + location + " => " + text);
    }

    public static void eLog(String text, String location){
//        Log.w(Fields.TAG, "|> " + location + " => " + text);
        Log.e(Fields.TAG, "|> " + location + " => " + text);
    }

    public static void iLog(String text, String location){
        Log.w(Fields.TAG, "|> " + location + " => " + text);
    }

    public static void printHashKey() {
        try {
            PackageInfo info = MyApp.getContext().getPackageManager().getPackageInfo(MyApp.getContext().getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                iLog("Hash Key: " + hashKey, "Function");

            }
        } catch (Exception e) {
            eLog("printHashKey() e " + e.getMessage(), "Function");
        }
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}

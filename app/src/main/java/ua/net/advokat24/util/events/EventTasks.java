package ua.net.advokat24.util.events;
import ua.net.advokat24.util.models.MTask;
import java.util.List;

import ua.net.advokat24.util.models.MTask;

public interface EventTasks {
    void tasks(List<MTask> list);
}

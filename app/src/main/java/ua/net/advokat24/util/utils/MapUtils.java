package ua.net.advokat24.util.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import ua.net.advokat24.MyApp;
import ua.net.advokat24.R;

import java.io.IOException;
import java.util.List;

public class MapUtils {

    public static String getDistanceFromMeters(int meters){
        String distanse = "";
        if(meters < 1000){
            distanse = String.valueOf(meters) + " m";
        } else if (meters > 1000){
            distanse = String.valueOf((int)meters / 1000) + " km\n" + String.valueOf(meters % 1000) + " m";

        }
        return distanse;
    }

    public static BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public static String getAdressFromLocation(LatLng latLng, Context context){
        Geocoder gc = new Geocoder(context);
        String strAddress = MyApp.getContext().getResources().getString(R.string.map_unknow_adres);
        try {
            if(Geocoder.isPresent()){
                List<Address> list = null;
                try {
                    list = gc.getFromLocation(latLng.latitude, latLng.longitude,1);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                assert list != null;
                Address address = list.get(0);
                strAddress = address.getAddressLine(0).substring(0, address.getAddressLine(0).indexOf(address.getLocality())-2) + ", " +
                        address.getLocality() + ", " + address.getCountryName();
            }
        } catch (Exception e){
            strAddress = MyApp.getContext().getResources().getString(R.string.map_unknow_adres);
        }
        return strAddress;
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;
        try {
            locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return locationMode != Settings.Secure.LOCATION_MODE_OFF;
    }



}

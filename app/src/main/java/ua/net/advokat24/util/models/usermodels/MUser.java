package ua.net.advokat24.util.models.usermodels;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class MUser  {



    public MUser() {
    }

    private String id;
    private String email;
    private String name;
    private String photo;
    private String phone;
    private String lawyersDocumentsPhoto;
    private boolean client;
    private String city;
    private String company;
    private String position;
    private int status;
    private WorkTime workTime;
    private boolean premium;
    private String notificationKay;
    private String skils;
    private String locatoinLat;
    private String locatoinLon;
    private String locatoinAdress;
    private String distanceToTask;
    private int reiting;
    private int reitingCount;
    private int companyStatus;
    private boolean isValid;
    private boolean isConfirmStartMessage;

    public int getCompanyStatus() {
        return companyStatus;
    }

    public void setCompanyStatus(int companyStatus) {
        this.companyStatus = companyStatus;
    }

    public String getDistanceToTask() {
        return distanceToTask;
    }

    public void setDistanceToTask(String distanceToTask) {
        this.distanceToTask = distanceToTask;
    }

    public String getLocatoinLat() {
        return locatoinLat;
    }

    public void setLocatoinLat(String locatoinLat) {
        this.locatoinLat = locatoinLat;
    }

    public String getLocatoinLon() {
        return locatoinLon;
    }

    public void setLocatoinLon(String locatoinLon) {
        this.locatoinLon = locatoinLon;
    }

    public WorkTime getWorkTime() {
        if(workTime == null){
            workTime = new WorkTime();
        }
        return workTime;
    }

    public String getLocatoinAdress() {
        return locatoinAdress;
    }

    public void setLocatoinAdress(String locatoinAdress) {
        this.locatoinAdress = locatoinAdress;
    }

    public void setWorkTime(WorkTime workTime) {
        this.workTime = workTime;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getReiting() {
        return reiting;
    }

    public void setReiting(int reiting) {
        this.reiting = reiting;
    }

    public int getReitingCount() {
        return reitingCount;
    }

    public void setReitingCount(int reitingCount) {
        this.reitingCount = reitingCount;
    }

    public String getSkils() {
        return skils;
    }

    public void setSkils(String skils) {
        this.skils = skils;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public boolean isClient() {
        return client;
    }

    public void setClient(boolean client) {
        this.client = client;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public boolean isPremium() {
        return premium;
    }

    public void setPremium(boolean premium) {
        this.premium = premium;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getNotificationKay() {
        return notificationKay;
    }

    public void setNotificationKay(String notificationKay) {
        this.notificationKay = notificationKay;
    }

    public String getLawyersDocumentsPhoto() {
        return lawyersDocumentsPhoto;
    }

    public void setLawyersDocumentsPhoto(String lawyersDocumentsPhoto) {
        this.lawyersDocumentsPhoto = lawyersDocumentsPhoto;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public boolean isConfirmStartMessage() {
        return isConfirmStartMessage;
    }

    public void setConfirmStartMessage(boolean confirmStartMessage) {
        isConfirmStartMessage = confirmStartMessage;
    }
}

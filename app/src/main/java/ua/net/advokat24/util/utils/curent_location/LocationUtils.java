package ua.net.advokat24.util.utils.curent_location;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.provider.Settings;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;

public class LocationUtils {

    public static String getAdressFromLocation(LatLng latLng, Context context){
        Geocoder gc = new Geocoder(context);
        String strAddress = "Uncnown";
        try {
            if(Geocoder.isPresent()){
                List<Address> list = null;
                try {
                    list = gc.getFromLocation(latLng.latitude, latLng.longitude,1);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                assert list != null;
                Address address = list.get(0);
                strAddress = address.getAddressLine(0).substring(0, address.getAddressLine(0).indexOf(address.getLocality())-2) + ", " +
                        address.getLocality() + ", " + address.getCountryName();
            }
        } catch (Exception e){
            strAddress = "Неизвестный адрес";
        }
        return strAddress;
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        try {
            locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return locationMode != Settings.Secure.LOCATION_MODE_OFF;
    }

    public static synchronized String getDistanceForUI(LatLng myLocation, LatLng destintion){
        return getDistanceWithArticle(getMeatersBeetwenPoints(myLocation, destintion));
    }

    private static synchronized String getDistanceWithArticle(int meters){
        String distanse = "";
        if(meters < 1000){
            distanse = String.valueOf(meters) + " m";
        } else if (meters > 1000){
            distanse = String.valueOf((int)meters / 1000) + " km\n" + String.valueOf(meters % 1000) + " m";
        }
        return distanse;
    }

    public static synchronized int getMeatersBeetwenPoints(LatLng myLocation, LatLng destintion){
        Location selected_location = new Location("locationA");
        selected_location.setLatitude(myLocation.latitude);
        selected_location.setLongitude(myLocation.longitude);
        Location near_locations = new Location("locationB");
        near_locations.setLatitude(destintion.latitude);
        near_locations.setLongitude(destintion.longitude);
        return (int)selected_location.distanceTo(near_locations);
    }
}

package ua.net.advokat24.util.models.message_firebase;

import ua.net.advokat24.util.models.MassegFromService;

public class Message {

    public Message() {
    }

    public Message(String id, String notifyKey, int action) {
        to = notifyKey;
        notification = new Notification(action);
        data = new Data(id, String.valueOf(action));
    }

    private String to;
    private String priority = "high";
    private String collapse_key = "type_a";
    private Notification notification;
    private Data data;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getCollapse_key() {
        return collapse_key;
    }

    public void setCollapse_key(String collapse_key) {
        this.collapse_key = collapse_key;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}

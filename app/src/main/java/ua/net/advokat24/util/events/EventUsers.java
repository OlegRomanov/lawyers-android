package ua.net.advokat24.util.events;
import ua.net.advokat24.util.models.usermodels.MUser;
import java.util.List;

import ua.net.advokat24.util.models.usermodels.MUser;

public interface EventUsers {
    void users(List<MUser> list);
}

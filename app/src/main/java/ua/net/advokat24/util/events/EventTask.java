package ua.net.advokat24.util.events;
import ua.net.advokat24.util.models.MTask;

import ua.net.advokat24.util.models.MTask;

public interface EventTask {
    void task(MTask mTask);
}

package ua.net.advokat24.util.utils;

import android.text.TextUtils;

public class PfoneUtils {
    public static boolean validCellPhone(String number) {
        return number != null && number.length() == 13 && TextUtils.substring(number, 0, 4).equals("+380");
    }
}

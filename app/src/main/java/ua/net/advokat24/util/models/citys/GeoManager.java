package ua.net.advokat24.util.models.citys;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GeoManager {

   @SerializedName("regions")
   @Expose
   private Regions regions;

   public Regions getRegions() {
      return regions;
   }

   public void setRegions(Regions regions) {
      this.regions = regions;
   }

}

package ua.net.advokat24.util.utils;

import ua.net.advokat24.util.Fields;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtils {

    public static String getCurentFullTime(){
        return new SimpleDateFormat(Fields.FULL_TIME_FORMAT).format(new Date());
    }

    public static String getCurentHourTime(){
        return new SimpleDateFormat(Fields.HOUR_TIME_FORMAT).format(new Date());
    }

//    public void getHourFromTimeData(String){
//
//    }

}

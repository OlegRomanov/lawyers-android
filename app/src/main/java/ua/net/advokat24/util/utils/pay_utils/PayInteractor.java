package ua.net.advokat24.util.utils.pay_utils;

public interface PayInteractor {

    void payForTask(String sum, PayListener listener);
}

package ua.net.advokat24.util.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import ua.net.advokat24.MyApp;
import ua.net.advokat24.util.Function;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import ua.net.advokat24.util.Function;

public class FilesUtils {

    private static final int USER_PHOTO_SIZE = 45;
    private static final int USER_DOCUMENT_SIZE = 65;
    private static Bitmap mBitmap;
    private static int size;
    private static Uri resultUri = null;

    public static Uri convertUri(Uri uri){
        return convertUri(uri, true);
    }

    public static Uri convertUri(Uri uri, boolean isPhoto){
        size = USER_PHOTO_SIZE;
        if(!isPhoto)
            size = USER_DOCUMENT_SIZE;
        try {
            if (mBitmap != null) {
                mBitmap.recycle();
            }
            InputStream stream = MyApp.getContext().getContentResolver().openInputStream(uri);
            mBitmap = BitmapFactory.decodeStream(stream);
            stream.close();
            chackBitmap();
        } catch (IOException e) {
            Function.eLog(e.getMessage(), "FilesUtils.chackBitmap");
        }

        return resultUri;
    }

    private static void chackBitmap(){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] imageInByte = stream.toByteArray();
        long lengthbmp = imageInByte.length;
        lengthbmp = lengthbmp/1024;
        if(lengthbmp > size){
            mBitmap = Bitmap.createScaledBitmap(mBitmap,(int)(mBitmap.getWidth()*0.8), (int)(mBitmap.getHeight()*0.8), true);
            chackBitmap();
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = new Date();
            String name = formatter.format(date);
            name = name.replaceAll( "/", "" );
            name = name.replaceAll( ":", "" );
            name = name.replaceAll( " ", "" );
            File filesDir = MyApp.getContext().getFilesDir();
            File imageFile = new File(filesDir, name + ".jpg");
            OutputStream os;
            try {
                os = new FileOutputStream(imageFile);
                mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
                os.flush();
                os.close();
                resultUri = Uri.fromFile(imageFile);
            } catch (Exception e) {
                Function.eLog(e.getMessage(), "FilesUtils.chackBitmap");
            }
        }
    }

//    private static Uri persistImage(Bitmap bitmap, String name) {
//        Uri uri;
//        File filesDir = MyApp.getContext().getFilesDir();
//        File imageFile = new File(filesDir, name + ".jpg");
//        OutputStream os;
//        try {
//            os = new FileOutputStream(imageFile);
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
//            os.flush();
//            os.close();
//            uri = Uri.fromFile(imageFile);
//        } catch (Exception e) {
//            uri = null;
//        }
//        return uri;
//    }


}

package ua.net.advokat24.util.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class IntentUtils {

    public static void call(Context context, String phone){
        Intent intentmobileCall = new Intent(Intent.ACTION_DIAL);
        intentmobileCall.setData(Uri.parse("tel:" + Uri.encode(String.valueOf(phone))));
        intentmobileCall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intentmobileCall);
    }
}

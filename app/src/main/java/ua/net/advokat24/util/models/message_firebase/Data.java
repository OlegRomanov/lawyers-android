package ua.net.advokat24.util.models.message_firebase;

public class Data {

    public Data(String extra, String action) {
        this.extra = extra;
        this.action = action;
    }

    public Data() {
    }

    private String extra;
    private String action;

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}

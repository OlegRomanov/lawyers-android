package ua.net.advokat24.util.events;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

public interface EventPageInit  {
    @StateStrategyType(SkipStrategy.class)
    void initPageSaccess();
    void initPageError(String text);
}

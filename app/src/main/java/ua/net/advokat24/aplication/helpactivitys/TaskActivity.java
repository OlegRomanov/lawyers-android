package ua.net.advokat24.aplication.helpactivitys;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import ua.net.advokat24.R;
import ua.net.advokat24.aplication.dialigs.CloseTaskDialog;
import ua.net.advokat24.aplication.dialigs.EditTextDialog;
import ua.net.advokat24.repository.firebase.categoryimpl.CategoryImpl;
import ua.net.advokat24.repository.firebase.categoryimpl.CategorysInteractor;
import ua.net.advokat24.repository.firebase.categoryimpl.OnCategorysListener;
import ua.net.advokat24.repository.firebase.userget.UserGetImpl;
import ua.net.advokat24.repository.firebase.userget.UserGetInteractor;
import ua.net.advokat24.repository.firebase.userget.UserGetListener;
import ua.net.advokat24.util.Fields;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.CategoryListModel;
import ua.net.advokat24.util.models.MTask;
import ua.net.advokat24.util.models.usermodels.MUser;
import ua.net.advokat24.util.utils.ImageUtils;
import ua.net.advokat24.util.utils.IntentUtils;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.net.advokat24.repository.firebase.categoryimpl.CategoryImpl;
import ua.net.advokat24.repository.firebase.categoryimpl.CategorysInteractor;
import ua.net.advokat24.repository.firebase.categoryimpl.OnCategorysListener;
import ua.net.advokat24.repository.firebase.userget.UserGetImpl;
import ua.net.advokat24.repository.firebase.userget.UserGetInteractor;
import ua.net.advokat24.repository.firebase.userget.UserGetListener;
import ua.net.advokat24.util.Function;

public class TaskActivity extends AppCompatActivity {

    public static final String KEY = "TaskDetailsActivity";
    private MTask mTask;

    @BindView(R.id.tv_name_lawyer)
    TextView tv_name_lawyer;
    @BindView(R.id.tv_name_client)
    TextView tv_name_client;
    @BindView(R.id.tv_subname_lawyer)
    TextView tv_subname_lawyer;
    @BindView(R.id.tv_subname_client)
    TextView tv_subname_client;
    @BindView(R.id.tv_body)
    TextView tv_body;
    @BindView(R.id.tv_call)
    TextView tv_call;
    @BindView(R.id.tv_task_status)
    TextView tv_task_status;

    @BindView(R.id.iv_lawyer_photo)
    ImageView iv_lawyer_photo;
    @BindView(R.id.iv_client_photo)
    ImageView iv_client_photo;
    @BindView(R.id.iv_edit_mode)
    ImageView iv_edit_mode;

    @BindView(R.id.fl_call)
    LinearLayout fl_call;
    @BindView(R.id.fl_lawyer)
    FrameLayout fl_lawyer;
    @BindView(R.id.fl_task_action)
    FrameLayout fl_task_action;
    @BindView(R.id.fl_close_task)
    FrameLayout fl_close_task;
    @BindView(R.id.fl_click_client)
    FrameLayout fl_click_client;

    @OnClick(R.id.fl_click_client)
    void fl_click_client() {
        Intent activity = new Intent(this, ClientDetailsActivity.class);
        activity.putExtra(ClientDetailsActivity.KEY, mTask.getClientId());
        startActivity(activity);
    }

    @OnClick(R.id.fl_lawyer)
    void fl_lawyer() {
        Intent activity = new Intent(this, LawyerDetailsActivity.class);
        activity.putExtra(LawyerDetailsActivity.KEY, mTask.getLawyerId());
        startActivity(activity);
    }

    @OnClick(R.id.fl_task_action)
    void fl_task_action() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            if (mTask.getClientId() != null && mTask.getClientId().equals(user.getUid())) {
                if (mTask.getLawyerId() != null) {
                    fl_task_action.setClickable(false);
                    CloseTaskDialog dialog = CloseTaskDialog.newInstance();
                    dialog.registerEvent(new CloseTaskDialog.Event() {
                        @Override
                        public void confirm(final int reiting) {
                            UserGetInteractor iterator = new UserGetImpl();
                            iterator.getUser(mTask.getLawyerId(), new UserGetListener() {
                                @Override
                                public void error(String error) {
                                    save();
                                }

                                @Override
                                public void user(MUser mUser) {
                                    mUser.setReiting(mUser.getReiting() + reiting);
                                    mUser.setReitingCount(mUser.getReitingCount() + 1);
                                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference(Fields.USERS_TABTE);
                                    mDatabase.child(mUser.getId()).setValue(mUser);
                                    save();
                                }
                            });
                        }

                        @Override
                        public void dismiss() {
                            fl_task_action.setClickable(true);
                        }
                    });
                    dialog.show(Objects.requireNonNull(TaskActivity.this).getSupportFragmentManager(), dialog.getClass().getSimpleName());
                } else {
                    save();
                }
            }

            if (mTask.getLawyerId() != null && mTask.getLawyerId().equals(user.getUid())) {
                if (mTask.getClientId() == null) {
                    save();
                }
            }
        }
    }

    private void save() {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference(Fields.TASKS_TABLE);
        mDatabase.child(mTask.getId()).child("closed").setValue(true);
        tv_task_status.setText(getResources().getString(R.string.task_klosed));
        tv_task_status.setTextColor(getResources().getColor(R.color.colorAccent));
        fl_close_task.setVisibility(View.GONE);
    }

    @OnClick(R.id.iv_toolbar_back)
    void iv_toolbar_back() {
        this.finish();
    }

    @OnClick(R.id.fl_call)
    void fl_call() {
        if (iv_edit_mode.getVisibility() == View.VISIBLE) {
            fl_call.setClickable(false);
            EditTextDialog dialogText = EditTextDialog.newInstance(
                    TaskActivity.this.getResources().getString(R.string.enter_you_phone),
                    TaskActivity.this.getResources().getString(R.string.enter_you_sub_phone),
                    TaskActivity.this.getResources().getString(R.string.confirm),
                    TaskActivity.this.getResources().getString(R.string.cancel),
                    tv_call.getText().toString());

            dialogText.registerEvent(new EditTextDialog.Event() {
                @Override
                public void confirm(String text) {
                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference(Fields.TASKS_TABLE);
                    mDatabase.child(mTask.getId()).child("clientPhone").setValue(text);
                    tv_call.setText(text);
                }

                @Override
                public void dismiss() {
                    fl_call.setClickable(true);
                }
            });
            dialogText.show(Objects.requireNonNull(TaskActivity.this).getSupportFragmentManager(), dialogText.getClass().getSimpleName());
        } else {
            IntentUtils.call(this, mTask.getClientPhone());
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_task);
        ButterKnife.bind(this);

        Bundle b = getIntent().getExtras();
        String key = b.getString("extra", "");

        FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database1.getReference(Fields.TASKS_TABLE);
        myRef.child(key).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                try {
                    if (snapshot.getValue() != null) {
                        try {
                            mTask = snapshot.getValue(MTask.class);
                            initPage();
                        } catch (Exception e) {
                            Function.eLog(e.getMessage(), getClass().getName());
                        }
                    } else {
                        Function.eLog("snapshot.getValue() == null", getClass().getName());
                    }
                } catch (Exception e) {
                    Function.eLog(e.getMessage(), getClass().getName());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Function.eLog(databaseError.getMessage(), " DatabaseError ");
            }
        });
    }



    private void initPage() {
        tv_subname_client.setText(mTask.getData());
        tv_body.setText(mTask.getDescriprion());

        CategorysInteractor categorys = new CategoryImpl();
        categorys.getList(new OnCategorysListener() {
            @Override
            public void resultList(List<CategoryListModel> listcategorys) {
                if (!TextUtils.isEmpty(mTask.getCategory())) {
                    tv_subname_lawyer.setText(Function.getStringSkils(mTask.getCategory(), listcategorys));
                } else {
                    tv_subname_lawyer.setText(getResources().getString(R.string.anithery));
                }
            }

            @Override
            public void error() {
            }
        });

        if (mTask.getLawyerId() != null) {
            tv_name_lawyer.setText(mTask.getLawyerName());
            loadPhoto(mTask.getLawyerPhoto(), mTask.getLawyerId(), iv_lawyer_photo);
        } else {
            fl_lawyer.setClickable(false);
            tv_name_lawyer.setText("Текущий адвокат");
            iv_lawyer_photo.setImageResource(R.drawable.ic_uncnow_user);
        }

        if (mTask.getClientId() != null) {
            tv_name_client.setText(mTask.getClientName());
            loadPhoto(mTask.getClientPhoto(), mTask.getClientId(), iv_client_photo);
        } else {
            fl_click_client.setClickable(false);
            tv_name_client.setText(getResources().getString(R.string.no_register));
            iv_client_photo.setImageResource(R.drawable.ic_uncnow_user);
        }

        if (mTask.isClosed()) {
            tv_task_status.setText(getResources().getString(R.string.task_klosed));
            tv_task_status.setTextColor(getResources().getColor(R.color.colorAccent));
            fl_close_task.setVisibility(View.GONE);
            fl_call.setVisibility(View.GONE);
        } else {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user != null) {
                if (mTask.getClientId() != null && mTask.getClientId().equals(user.getUid())) {
                    fl_close_task.setVisibility(View.VISIBLE);
                    iv_edit_mode.setVisibility(View.VISIBLE);
                    fl_call.setVisibility(View.VISIBLE);
                    tv_call.setText(mTask.getClientPhone());
                }

                if (mTask.getLawyerId() != null && mTask.getLawyerId().equals(user.getUid())) {
                    iv_edit_mode.setVisibility(View.GONE);
                    if (mTask.getClientId() == null) {
                        fl_close_task.setVisibility(View.VISIBLE);
                    }
                    fl_call.setVisibility(View.VISIBLE);
                    tv_call.setText(mTask.getClientPhone());
                }
            }
            tv_task_status.setText(getResources().getString(R.string.task_not_klosed));
            tv_task_status.setTextColor(getResources().getColor(R.color.colorError));
        }
    }

    private void loadPhoto(String photo, String id, ImageView imageView) {
        ImageUtils.loadPhoto(this, photo, id, imageView);
    }
}
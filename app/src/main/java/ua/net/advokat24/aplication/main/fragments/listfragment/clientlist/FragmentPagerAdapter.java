package ua.net.advokat24.aplication.main.fragments.listfragment.clientlist;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ua.net.advokat24.aplication.main.fragments.listfragment.clientlist.ClientListFragment;
import ua.net.advokat24.aplication.main.fragments.listfragment.clientlist.ClientMapFragment;

public class FragmentPagerAdapter extends FragmentStatePagerAdapter {

    public FragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return ClientMapFragment.newInstance();
            case 1:
                return ClientListFragment.newInstance();
            default:
                break;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

}

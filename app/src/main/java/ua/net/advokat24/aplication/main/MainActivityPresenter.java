package ua.net.advokat24.aplication.main;

import android.net.Uri;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.iid.FirebaseInstanceId;
import ua.net.advokat24.MyApp;
import ua.net.advokat24.repository.SharedPreferencesManager;
import ua.net.advokat24.repository.firebase.categoryimpl.CategoryImpl;
import ua.net.advokat24.repository.firebase.categoryimpl.CategorysInteractor;
import ua.net.advokat24.repository.firebase.categoryimpl.OnCategorysListener;
import ua.net.advokat24.repository.firebase.clearnotifykey.ClearNotifyKeyImpl;
import ua.net.advokat24.repository.firebase.clearnotifykey.ClearNotifyKeyInteractor;
import ua.net.advokat24.repository.firebase.clearnotifykey.OnClearKeyListener;
import ua.net.advokat24.repository.firebase.getusers.GetUsersImpl;
import ua.net.advokat24.repository.firebase.getusers.GetUsersInteractor;
import ua.net.advokat24.repository.firebase.getusers.GetUsersListener;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksImpl;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksInteractor;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksListener;
import ua.net.advokat24.repository.firebase.uploadfile.UploadFileImpl;
import ua.net.advokat24.repository.firebase.uploadfile.UploadFileInteractor;
import ua.net.advokat24.repository.firebase.uploadfile.UploadFileListener;
import ua.net.advokat24.repository.firebase.userget.UserGetImpl;
import ua.net.advokat24.repository.firebase.userget.UserGetInteractor;
import ua.net.advokat24.repository.firebase.userget.UserGetListener;
import ua.net.advokat24.repository.firebase.userupdate.UserUpdateImpl;
import ua.net.advokat24.repository.firebase.userupdate.UserUpdateInteractor;
import ua.net.advokat24.repository.firebase.userupdate.UserUpdateListener;
import ua.net.advokat24.util.Fields;
import ua.net.advokat24.util.models.CategoryListModel;
import ua.net.advokat24.util.models.MTask;
import ua.net.advokat24.util.models.usermodels.MUser;
import ua.net.advokat24.util.utils.FirebaseUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ua.net.advokat24.repository.SharedPreferencesManager;
import ua.net.advokat24.repository.firebase.categoryimpl.CategoryImpl;
import ua.net.advokat24.repository.firebase.categoryimpl.CategorysInteractor;
import ua.net.advokat24.repository.firebase.categoryimpl.OnCategorysListener;
import ua.net.advokat24.repository.firebase.clearnotifykey.ClearNotifyKeyImpl;
import ua.net.advokat24.repository.firebase.clearnotifykey.ClearNotifyKeyInteractor;
import ua.net.advokat24.repository.firebase.clearnotifykey.OnClearKeyListener;
import ua.net.advokat24.repository.firebase.getusers.GetUsersImpl;
import ua.net.advokat24.repository.firebase.getusers.GetUsersInteractor;
import ua.net.advokat24.repository.firebase.getusers.GetUsersListener;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksImpl;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksInteractor;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksListener;
import ua.net.advokat24.repository.firebase.uploadfile.UploadFileImpl;
import ua.net.advokat24.repository.firebase.uploadfile.UploadFileInteractor;
import ua.net.advokat24.repository.firebase.uploadfile.UploadFileListener;
import ua.net.advokat24.repository.firebase.userget.UserGetImpl;
import ua.net.advokat24.repository.firebase.userget.UserGetInteractor;
import ua.net.advokat24.repository.firebase.userget.UserGetListener;
import ua.net.advokat24.repository.firebase.userupdate.UserUpdateImpl;
import ua.net.advokat24.repository.firebase.userupdate.UserUpdateInteractor;
import ua.net.advokat24.repository.firebase.userupdate.UserUpdateListener;

@InjectViewState
public class MainActivityPresenter extends MvpPresenter<MainActivityView> {

    private MUser mUser;
    private List<CategoryListModel> listCategory;
    private List<MUser> listUsers;
    private List<MTask> listMyTasks;
    private String searchCity;
    private String searckCategory;

    public MUser getmUser() {
        if(mUser != null){
            if(!Objects.equals(FirebaseInstanceId.getInstance().getToken(), mUser.getNotificationKay()) && !mUser.isClient()){
                ClearNotifyKeyInteractor clearKey = new ClearNotifyKeyImpl();
                clearKey.clearKeys(FirebaseInstanceId.getInstance().getToken(), new OnClearKeyListener() {
                    @Override
                    public void saccess() {
                        DatabaseReference mDatabase = FirebaseUtils.getDatabase().getReference(Fields.USERS_TABTE);
                        mDatabase.child(mUser.getId()).child("notificationKay").setValue(FirebaseInstanceId.getInstance().getToken());
                    }
                });
            }
        }
        return mUser;
    }

    public List<CategoryListModel> getListCategory() {
        return listCategory;
    }

    public List<MUser> getListUsers() {
        return listUsers;
    }

    public List<MTask> getListMyTasks() {
        return listMyTasks;
    }

    public String getSearchCity() {
        if(searchCity == null){
            if(mUser != null && mUser.getCity() != null){
                searchCity = mUser.getCity();
            } else {
                searchCity = "Киев";
            }
        }
        return searchCity;
    }
    public String getSearckCategory() {
        if(searckCategory == null){
            searckCategory = "";
        }
        return searckCategory;
    }

    private boolean isClient = false;
    private boolean isRegister = false;
    public boolean isClient() {
        return isClient;
    }
    public boolean isRegister() {
        return isRegister;
    }

    public MainActivityPresenter() {
        initCategorys();
        resetStatus();
        startMain();

    }

    public void startMain(){
        if(isRegister()){
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user != null){
                UserGetInteractor interactor = new UserGetImpl();
                interactor.getUser(user.getUid(), new UserGetListener() {
                    @Override
                    public void error(String error) {
                        getViewState().unregisterStart();
                    }

                    @Override
                    public void user(MUser mUser) {
                        if(mUser != null){
                            startApp();
                        } else {
                            getViewState().unregisterStart();
                        }
                    }
                });
            } else {
                SharedPreferencesManager.getInstance(MyApp.getContext()).setIsRegister(false);
                resetStatus();
                getViewState().unregisterStart();
            }
        } else {
            getViewState().unregisterStart();
        }
    }

    public void resetStatus(){
        isClient = SharedPreferencesManager.getInstance(MyApp.getContext()).isClient();
        isRegister = SharedPreferencesManager.getInstance(MyApp.getContext()).isRegister();
    }



    /*Registration and sign-in*/

    public void chackFacebookUser(){
        UserGetInteractor interactor = new UserGetImpl();
        interactor.getUser(FirebaseAuth.getInstance().getCurrentUser().getUid(), new UserGetListener() {
            @Override
            public void error(String error) {
                getViewState().facebookUserNotExist();
            }

            @Override
            public void user(MUser mUser) {
                if(mUser != null){
                    SharedPreferencesManager.getInstance(MyApp.getContext()).setIsRegister(true);
                    SharedPreferencesManager.getInstance(MyApp.getContext()).setClient(mUser.isClient());
                    resetStatus();
                    startApp();
                } else {
                    getViewState().facebookUserNotExist();
                }
            }
        });
    }

    public void singin(){
        UserGetInteractor interactor = new UserGetImpl();
        interactor.getUser(FirebaseAuth.getInstance().getCurrentUser().getUid(), new UserGetListener() {
            @Override
            public void error(String error) {
                getViewState().initError(error);
            }

            @Override
            public void user(MUser mUser) {
                SharedPreferencesManager.getInstance(MyApp.getContext()).setIsRegister(true);
                SharedPreferencesManager.getInstance(MyApp.getContext()).setClient(mUser.isClient());
                resetStatus();
                startApp();
            }
        });
    }

    public void registration(MUser mUser, final Uri userPhoto, final Uri documentsPhoto) {
        mUser.setId(FirebaseAuth.getInstance().getCurrentUser().getUid());
        mUser.setClient(SharedPreferencesManager.getInstance(MyApp.getContext()).isClient());
        UserUpdateInteractor interactor = new UserUpdateImpl();
        UserUpdateListener listener = new UserUpdateListener() {
            @Override
            public void error(String error) {
                getViewState().errorRegistration(error);
            }

            @Override
            public void user(MUser mUser) {
                if(!isSave){
                    SharedPreferencesManager.getInstance(MyApp.getContext()).setIsRegister(true);
                    resetStatus();
                    startApp();
                }

            }
        };
        interactor.updateUser(mUser, userPhoto, documentsPhoto, listener);

    }




    /*Start init*/

    private boolean restartInit = false;
    public void startApp(){
        restartInit = true;
        initUser();
        initCategorys();
        initMyTasks();
    }

    public void check(){
        if(mUser != null && listCategory != null && listMyTasks != null && listUsers != null ){
            if(restartInit){
                restartInit = false;
                getViewState().initSaccess();
            }
        }
    }

    public void initUser(){
        UserGetInteractor iterator = new UserGetImpl();
        iterator.getUserRunTime(FirebaseAuth.getInstance().getCurrentUser().getUid(), new UserGetListener() {
            @Override
            public void error(String error) {

            }

            @Override
            public void user(MUser mUser) {
                MainActivityPresenter.this.mUser = mUser;
                if(restartInit){
                    initLawyers();
                    check();
                }
            }
        });
    }

    public void initLawyers(){
        GetUsersInteractor iterator = new GetUsersImpl();
        iterator.getUsersForRunTimeList(getSearckCategory(), getSearchCity(),  new GetUsersListener() {
            @Override
            public void resultSearchUsers(List<MUser> lawyers) {
                MainActivityPresenter.this.listUsers = lawyers;
                if(restartInit){
                    check();
                }

            }

            @Override
            public void error() {

            }
        });
    }

    public void initCategorys(){
        CategorysInteractor iterator = new CategoryImpl();
        iterator.getList(new OnCategorysListener() {
            @Override
            public void resultList(List<CategoryListModel> list) {
                MainActivityPresenter.this.listCategory = list;
                check();
            }

            @Override
            public void error() {

            }
        });
    }

    public void initMyTasks(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if( user != null){
            TasksInteractor iterator = new TasksImpl();
            iterator.getTasksRunTime(FirebaseAuth.getInstance().getCurrentUser().getUid(), isClient, new TasksListener() {
                @Override
                public void resultList(List<MTask> list) {
                    if(MainActivityPresenter.this.listMyTasks == null){
                        MainActivityPresenter.this.listMyTasks = new ArrayList<>();
                    }
                    MainActivityPresenter.this.listMyTasks.clear();
                    MainActivityPresenter.this.listMyTasks = list;
                    if(restartInit){
                        check();
                    }
                }

                @Override
                public void error() {

                }
            });
        }
    }

    /*userUpdate*/

    private boolean isSave = false;
    public void saveUser(){
        isSave = true;
        registration(mUser, null, null);
    }

    public void apdateUserPhoto(Uri userPhoto){
        isSave = true;
        UploadFileInteractor interactor = new UploadFileImpl();
        interactor.uploadFile(Fields.USER_PHOTO_PATH, userPhoto, new UploadFileListener() {
            @Override
            public void error(String error) {
                getViewState().finishUpdateUserPhoto();
            }

            @Override
            public void saccess() {
                getViewState().finishUpdateUserPhoto();
            }
        });
    }

    public void searchUsers(String category, String city){
        searchCity = city;
        searckCategory = category;
        GetUsersInteractor iterator = new GetUsersImpl();
        iterator.getUsersForRunTimeList(getSearckCategory(), getSearchCity(),  new GetUsersListener() {
            @Override
            public void resultSearchUsers(List<MUser> lawyers) {
                MainActivityPresenter.this.listUsers = lawyers;
                getViewState().searchResult();
            }

            @Override
            public void error() {

            }
        });
    }
}

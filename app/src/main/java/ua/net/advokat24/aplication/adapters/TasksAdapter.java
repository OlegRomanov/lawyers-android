package ua.net.advokat24.aplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import ua.net.advokat24.R;
import ua.net.advokat24.util.models.CategoryListModel;
import ua.net.advokat24.util.models.MTask;

import java.util.ArrayList;
import java.util.List;

import ua.net.advokat24.aplication.helpactivitys.TaskActivity;
import ua.net.advokat24.util.Function;

public class TasksAdapter extends RecyclerView.Adapter<TasksAdapter.ViewHolder> {

    private List<MTask> list;
    private List<CategoryListModel> listCategory;
    private Context context;
    private boolean isClient;

    public TasksAdapter(Context context, boolean isClient) {
        this.list = new ArrayList<>();
        this.listCategory = new ArrayList<>();
        this.context = context;
        this.isClient = isClient;
    }

    public void initList(List<MTask> list, List<CategoryListModel> listCategory) {
        this.list.clear();
        this.list = list;
        this.listCategory = listCategory;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public TasksAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.i_task_field, parent, false);
        return new TasksAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final TasksAdapter.ViewHolder holder, int position) {
        final MTask model = list.get(holder.getAdapterPosition());

        holder.tv_body.setText(model.getDescriprion());
        holder.tv_time.setText(Function.getDataAppFormat(model.getData(), context));

        holder.iv_status.setImageResource(
                model.isClosed()
                        ? R.drawable.ic_status_done
                        : R.drawable.ic_status_not_done);

        holder.tv_creator.setText(
                isClient
                        ? !TextUtils.isEmpty(model.getLawyerName())
                        ? model.getLawyerName()
                        : "Адвокат не найден"
                        : !TextUtils.isEmpty(model.getClientName())
                        ? model.getClientName()
                        : context.getResources().getString(R.string.anonim));

        holder.tv_head.setText(
                !TextUtils.isEmpty(model.getCategory())
                        ? Function.getStringProfileSkils(model.getCategory(), listCategory)
                        : context.getResources().getString(R.string.anithery));


        holder.fl_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent activity = new Intent(context, TaskActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("extra", model.getId());
                activity.putExtras(bundle);
                context.startActivity(activity);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_head, tv_body, tv_creator, tv_time;
        private FrameLayout fl_click;
        private ImageView iv_status;

        ViewHolder(View itemView) {
            super(itemView);
            tv_creator = itemView.findViewById(R.id.tv_creator);
            tv_head = itemView.findViewById(R.id.tv_head);
            tv_body = itemView.findViewById(R.id.tv_body);
            fl_click = itemView.findViewById(R.id.fl_click);
            tv_time = itemView.findViewById(R.id.tv_time);
            iv_status = itemView.findViewById(R.id.iv_status);
        }
    }
}

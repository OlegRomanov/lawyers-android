package ua.net.advokat24.aplication.main.fragments.reglawyer;

import com.arellomobile.mvp.MvpView;
import ua.net.advokat24.util.events.EventError;
import ua.net.advokat24.util.events.EventPageInit;
import ua.net.advokat24.util.events.EventSaccess;

public interface LoginRegLawyerView extends MvpView, EventPageInit, EventSaccess, EventError {
    void userReady();
}

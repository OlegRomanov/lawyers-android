package ua.net.advokat24.aplication.main.fragments.clientpay;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import ua.net.advokat24.R;
import ua.net.advokat24.util.models.MTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PayConfirmFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public PayConfirmFragment() {

    }

    @BindView(R.id.ll_app_bar_user_info) LinearLayout ll_app_bar_user_info;
    @BindView(R.id.iv_toolbar_backgraond) ImageView iv_toolbar_backgraond;
    @BindView(R.id.app_bar) AppBarLayout app_bar;

    @OnClick(R.id.tv_btn_action) void tv_btn_action() {
        if (mListener != null) {
//            mListener.payGoToPhoneNamber();
        }
    }

    public static PayConfirmFragment newInstance() {
        return new PayConfirmFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.f_pay_confirm, container, false);
        ButterKnife.bind(this, v);
        app_bar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                int maxScroll = appBarLayout.getTotalScrollRange() / 2;
                float percentage = ((float) Math.abs(verticalOffset) / ((float) maxScroll / 2)) - 1.5f;
                iv_toolbar_backgraond.setAlpha(percentage);
                ll_app_bar_user_info.setAlpha(percentage * -1);    }
        });
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void backToHome();
    }
}

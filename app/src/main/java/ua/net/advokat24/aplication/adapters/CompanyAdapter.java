package ua.net.advokat24.aplication.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import ua.net.advokat24.R;
import ua.net.advokat24.util.models.BaseModel;
import ua.net.advokat24.util.models.citys.City;

import java.util.ArrayList;
import java.util.List;

public class CompanyAdapter extends RecyclerView.Adapter<CompanyAdapter.ViewHolder> {

    private List<BaseModel> companys;
    private String companyExist = "";

    private boolean onBind;

    public CompanyAdapter(String companyExist) {
        this.companyExist = companyExist == null?"":companyExist;
        this.companys = new ArrayList<>();
    }

    public void initListWithExist(List<BaseModel> companys, String companyExist){
        this.companyExist = companyExist;
        initList(companys);
    }

    public void initList(List<BaseModel> companys){
        this.companys = companys;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CompanyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.i_city, parent, false);
        return new CompanyAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final CompanyAdapter.ViewHolder holder , int position) {

        final BaseModel model = companys.get(holder.getAdapterPosition());
        holder.tv_name.setText(model.getName());
        onBind = true;
        holder.iv_status.setChecked(companyExist.equals(model.getName()));
        holder.iv_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                companyExist = model.getName();
                notifyDataSetChanged();
            }
        });

        holder.fl_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                companyExist = model.getName();
                notifyDataSetChanged();
            }
        });
        onBind = false;
    }

    @Override
    public int getItemCount() {
        return companys.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name;
        private FrameLayout fl_click;
        private AppCompatCheckBox iv_status;
        ViewHolder(View itemView) {
            super(itemView);

            fl_click = itemView.findViewById(R.id.fl_click);
            iv_status = itemView.findViewById(R.id.iv_status);
            tv_name = itemView.findViewById(R.id.tv_name);
        }
    }

    public String getCityExist() {
        return companyExist;
    }
}

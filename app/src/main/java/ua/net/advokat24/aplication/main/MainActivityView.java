package ua.net.advokat24.aplication.main;

import com.arellomobile.mvp.MvpView;

public interface MainActivityView extends MvpView {

    void initSaccess();
    void initError(String error);

    void unregisterStart();
    void registerStart();

    void facebookUserNotExist();
    void errorRegistration(String error);

    void searchResult();

    void finishUpdateUserPhoto();
}

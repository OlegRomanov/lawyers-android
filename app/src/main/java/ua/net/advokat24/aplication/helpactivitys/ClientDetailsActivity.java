package ua.net.advokat24.aplication.helpactivitys;

import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import ua.net.advokat24.R;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksListener;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksImpl;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksInteractor;
import ua.net.advokat24.repository.firebase.userget.UserGetListener;
import ua.net.advokat24.repository.firebase.userget.UserGetImpl;
import ua.net.advokat24.repository.firebase.userget.UserGetInteractor;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.MTask;
import ua.net.advokat24.util.models.usermodels.MUser;
import ua.net.advokat24.util.utils.ImageUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksImpl;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksInteractor;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksListener;
import ua.net.advokat24.repository.firebase.userget.UserGetImpl;
import ua.net.advokat24.repository.firebase.userget.UserGetInteractor;
import ua.net.advokat24.repository.firebase.userget.UserGetListener;
import ua.net.advokat24.util.Function;

public class ClientDetailsActivity extends AppCompatActivity {

    public static final String KEY = "MyUserClient";
    private String id;

    @BindView(R.id.ll_app_bar_user_info)
    LinearLayout ll_app_bar_user_info;
    @BindView(R.id.iv_toolbar_backgraond)
    ImageView iv_toolbar_backgraond;
    @BindView(R.id.app_bar)
    AppBarLayout app_bar;
    @BindView(R.id.tv_app_bar_user_name)
    TextView tv_app_bar_user_name;
    @BindView(R.id.iv_app_bar_user_photo) ImageView iv_app_bar_user_photo;

    @BindView(R.id.tv_field_name) TextView tv_field_name;
    @BindView(R.id.tv_field_city) TextView tv_field_city;
    @BindView(R.id.tv_field_sum_tasks) TextView tv_field_sum_tasks;

    @OnClick(R.id.iv_toolbar_back)
    void iv_toolbar_back() {
        this.finish();
    }

    @OnClick(R.id.tv_works)
    void tv_works() {
        if(tv_field_sum_tasks.getText().toString().equals("0")){
            Function.showToast(getResources().getString(R.string.empty_list_tasks));
        } else {
            Intent activity = new Intent(this, TasksActivity.class);
            activity.putExtra(TasksActivity.KEY, id);
            activity.putExtra(TasksActivity.KEY_IS_KIENT, true);
            startActivity(activity);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_client_details);
        ButterKnife.bind(this);
        id = getIntent().getStringExtra(KEY);

        app_bar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                int maxScroll = appBarLayout.getTotalScrollRange() / 2;
                float percentage = ((float) Math.abs(verticalOffset) / ((float) maxScroll / 2)) - 1.5f;
                iv_toolbar_backgraond.setAlpha(percentage);
                ll_app_bar_user_info.setAlpha(percentage * -1);
            }
        });

        UserGetInteractor userGetInteractor = new UserGetImpl();
        userGetInteractor.getUser(id, new UserGetListener() {
            @Override
            public void error(String error) {
                Function.eLog(error, getClass().getName());
            }

            @Override
            public void user(MUser mUser) {
                if (!TextUtils.isEmpty(mUser.getName()))
                    tv_app_bar_user_name.setText(mUser.getName());
                tv_field_name.setText(mUser.getName());
                tv_field_city.setText(mUser.getCity());
                loadPhoto(mUser);
                TasksInteractor init = new TasksImpl();
                init.getList(id, true, new TasksListener() {
                    @Override
                    public void resultList(final List<MTask> listtasks) {
                        tv_field_sum_tasks.setText(String.valueOf(listtasks.size()));
                    }

                    @Override
                    public void error() {

                    }
                });
            }
        });

    }

    private void loadPhoto(MUser mUser) {
        ImageUtils.loadPhoto(this, mUser, iv_app_bar_user_photo);
    }
}

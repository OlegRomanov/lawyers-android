package ua.net.advokat24.aplication.dialigs;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import ua.net.advokat24.R;
import ua.net.advokat24.aplication.adapters.CitysAdapter;
import ua.net.advokat24.aplication.adapters.EditSkilsAdapter;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.CategoryListModel;
import ua.net.advokat24.util.models.citys.City;
import ua.net.advokat24.util.utils.CitysUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ua.net.advokat24.util.Function;

public class EditSkilsDialog extends DialogFragment {

    private List<String> curentList;
    private List<CategoryListModel>category;
    private String categoryString;
    private EditSkilsAdapter adapter;
    private Unbinder unbinder;

    private static final String KEY = "EditSkilsDialogKey";
    private static final String KEY_VALUE = "EditSkilsDialogKeyValue";

    public static EditSkilsDialog newInstance(String curent, String list) {
        EditSkilsDialog fragment = new EditSkilsDialog();
        Bundle bundle = new Bundle();
        bundle.putString(KEY, curent);
        bundle.putString(KEY_VALUE, list);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            String curent = getArguments().getString(KEY);
            if(curent != null){
                curentList = new ArrayList<>(Arrays.asList(curent.split(",")));
            } else {
                curentList = new ArrayList<>();
            }

            categoryString = getArguments().getString(KEY_VALUE);
            category = new Gson().fromJson(categoryString, new TypeToken<List<CategoryListModel>>() {}.getType());
        }
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogTheme);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        Objects.requireNonNull(getDialog().getWindow()).setBackgroundDrawableResource(R.color.colorTrans);
        View v = inflater.inflate(R.layout.d_edit_skils, container, false);
        unbinder = ButterKnife.bind(this, v);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new EditSkilsAdapter(curentList, category);
        adapter.registerEvent(new EditSkilsAdapter.Event() {
            @Override
            public void refresh() {
                if(curentList.size() == 0)
                    tv_empty_list.setVisibility(View.VISIBLE);
            }
        });
        recyclerView.setAdapter(adapter);
        tv_empty_list.setVisibility(curentList.size() == 0?View.VISIBLE:View.INVISIBLE);
        return v;
    }


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.fab_add)
    FloatingActionButton fab_add;

    @BindView(R.id.tv_empty_list)
    TextView tv_empty_list;

    @OnClick(R.id.iv_toolbar_action) void iv_toolbar_action() {
        if(curentList.size() != 0){
            String result = TextUtils.join(",", curentList);
            event.confirm(result);
            dismiss();
        } else {
            Function.showToast("Выберите минимум одну специальность");
        }
    }

    @OnClick(R.id.iv_toolbar_back) void iv_toolbar_back() {
        dismiss();
    }

    @OnClick(R.id.fab_add) void fab_add() {
        if (curentList.size() <= 10){
            fab_add.setClickable(false);
            ChooseSkilsDialog dialog = ChooseSkilsDialog.newInstance(categoryString);
            dialog.registerEvent(new ChooseSkilsDialog.Event() {
                @Override
                public void result(String resutl) {
                    if(isExistSkis(resutl)){
                        Function.showToast("Такая специальность уже вибрана");
                    } else {
                        curentList.add(resutl);
                        adapter.notifyDataSetChanged();
                    }
                    tv_empty_list.setVisibility(curentList.size() == 0?View.VISIBLE:View.INVISIBLE);

                }

                @Override
                public void dismiss() {
                    fab_add.setClickable(true);
                }
            });
            dialog.show(getActivity().getSupportFragmentManager(), dialog.getClass().getSimpleName());
        } else {
            Function.showToast("Максимум 10 специальностей");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onStart() {
        super.onStart();
        Objects.requireNonNull(getDialog().getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        event.isDismis();
        super.onDismiss(dialog);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        Objects.requireNonNull(getDialog().getWindow()).getAttributes().windowAnimations = R.style.DialogAnimationNew;
    }

    public interface Event {
        void isDismis();
        void confirm(String city);
    }

    private Event event;

    public void registerEvent(Event event) {
        this.event = event;
    }

    private boolean isExistSkis(String curent){
        for (String skil : curentList){
            if(skil.equals(curent)){
                return true;
            }
        }
        return false;
    }
}
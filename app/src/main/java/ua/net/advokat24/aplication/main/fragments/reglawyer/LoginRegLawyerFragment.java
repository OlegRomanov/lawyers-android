package ua.net.advokat24.aplication.main.fragments.reglawyer;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import ua.net.advokat24.R;
import ua.net.advokat24.aplication.dialigs.ChooseCityDialog;
import ua.net.advokat24.aplication.dialigs.ChooseSkilsDialog;
import ua.net.advokat24.aplication.dialigs.EditSkilsDialog;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.GlideApp;
import ua.net.advokat24.util.MyTextInputLayout;
import ua.net.advokat24.util.models.usermodels.MUser;
import ua.net.advokat24.util.utils.PfoneUtils;
import ua.net.advokat24.util.utils.UserCompanyStatusUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;
import ua.net.advokat24.aplication.dialigs.ChooseCityDialog;
import ua.net.advokat24.aplication.dialigs.EditSkilsDialog;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.util.Function;

public class LoginRegLawyerFragment extends MvpAppCompatFragment implements LoginRegLawyerView {

    @InjectPresenter
    LoginRegLawyerPresenter presenter;

    private OnFragmentInteractionListener mListener;
    private ProgressDialog dialogWait;

    public LoginRegLawyerFragment() {
    }

    @BindView(R.id.et_name)
    TextInputEditText et_name;
    @BindView(R.id.et_email)
    TextInputEditText et_email;
    @BindView(R.id.et_passwd)
    TextInputEditText et_passwd;
    @BindView(R.id.et_repeat_passwd)
    TextInputEditText et_repeat_passwd;
    @BindView(R.id.et_city)
    TextInputEditText et_city;
    @BindView(R.id.et_phone)
    TextInputEditText et_phone;
    @BindView(R.id.et_category)
    TextInputEditText et_category;

    @BindView(R.id.til_name)
    MyTextInputLayout til_name;
    @BindView(R.id.til_email)
    MyTextInputLayout til_email;
    @BindView(R.id.til_passwd)
    MyTextInputLayout til_passwd;
    @BindView(R.id.til_repeat_passwd)
    MyTextInputLayout til_repeat_passwd;
    @BindView(R.id.til_city)
    MyTextInputLayout til_city;
    @BindView(R.id.til_phone)
    MyTextInputLayout til_phone;
    @BindView(R.id.til_category)
    MyTextInputLayout til_category;
    @BindView(R.id.fi_registrationFB)
    FrameLayout fi_registrationFB;

    @BindView(R.id.tv_user_photo)
    TextView tv_user_photo;
    @BindView(R.id.tv_document_photo)
    TextView tv_document_photo;

    @BindView(R.id.iv_photo_documents)
    ImageView iv_photo_documents;
    @BindView(R.id.iv_photo_user)
    ImageView iv_photo_user;

    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;

    @OnClick(R.id.iv_photo_documents)
    void iv_photo_documents() {
        if (mListener != null) {
            mListener.putPhoto(false, true);
        }
    }

    @OnClick(R.id.iv_photo_user)
    void iv_photo_user() {
        if (mListener != null) {
            mListener.putPhoto(false, false);
        }
    }

    @OnClick(R.id.iv_toolbar_back)
    void toolbarBack() {
        if (mListener != null) {
            Function.showToast("Регистрация отменена");
            mListener.goToLogin();
        }
    }

    @OnClick(R.id.tv_login)
    void registration() {

        if (et_name.getText().toString().length() > 0 &&
                et_email.getText().toString().length() > 0 &&
                et_passwd.getText().toString().length() > 0 &&
                et_repeat_passwd.getText().toString().length() > 0 &&
                et_phone.getText().toString().length() > 0 &&
                et_category.getText().toString().length() > 0 &&
                et_city.getText().toString().length() > 0 &&
                presenter.getDocument() != null) {
            if (Function.checkEmail(et_email.getText().toString())) {
                if (Function.getWordsCount(et_name.getText().toString()) >= 2 && Function.getWordsCount(et_name.getText().toString()) <= 4) {
                    if (et_passwd.getText().toString().length() > 5 &&
                            et_repeat_passwd.getText().toString().length() > 5) {
                        if (et_passwd.getText().toString().equals(et_repeat_passwd.getText().toString())) {
                            if (PfoneUtils.validCellPhone(et_phone.getText().toString())) {
                                if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                                    dialogWait = Function.startProgress(getContext());
                                    presenter.getmUser().setCompanyStatus(UserCompanyStatusUtils.setStatusToUser(radioGroup.getCheckedRadioButtonId()));
                                    presenter.updateUser();
                                } else {
                                    if (mListener != null) {
                                        presenter.getmUser().setCompanyStatus(UserCompanyStatusUtils.setStatusToUser(radioGroup.getCheckedRadioButtonId()));
                                        mListener.registration(et_passwd.getText().toString(), presenter.getGeadyUser(), presenter.getPhoto(), presenter.getDocument());
                                    }
                                }
                            } else {
                                til_phone.setError(" ");
                                Function.showToast(getContext().getResources().getString(R.string.invalid_phone));
                            }
                        } else {
                            til_passwd.setError(" ");
                            til_repeat_passwd.setError(" ");
                            Function.showToast(getContext().getResources().getString(R.string.pasword_not_eqalse));
                        }
                    } else {
                        til_passwd.setError(" ");
                        til_repeat_passwd.setError(" ");
                        Function.showToast(getResources().getString(R.string.pasword_litle));
                    }
                } else {
                    til_name.setError(" ");
                    Function.showToast(getContext().getResources().getString(R.string.enter_you_name_and_subname));
                }
            } else {
                til_email.setError(" ");
                Function.showToast(getResources().getString(R.string.invalid_eail));
            }
        } else {
            if (TextUtils.isEmpty(et_name.getText().toString())) {
                til_name.setError(" ");
            }
            if (TextUtils.isEmpty(et_email.getText().toString())) {
                til_email.setError(" ");
            }
            if (TextUtils.isEmpty(et_passwd.getText().toString())) {
                til_passwd.setError(" ");
            }
            if (TextUtils.isEmpty(et_repeat_passwd.getText().toString())) {
                til_repeat_passwd.setError(" ");
            }
            if (TextUtils.isEmpty(et_city.getText().toString())) {
                til_city.setError(" ");
            }
            if (TextUtils.isEmpty(et_phone.getText().toString())) {
                til_phone.setError(" ");
            }

            if (TextUtils.isEmpty(et_category.getText().toString())) {
                til_category.setError(" ");
            }

            if (presenter.getDocument() == null) {
                tv_document_photo.setTextColor(getContext().getResources().getColor(R.color.colorError));
            }
        }
    }

    public static LoginRegLawyerFragment newInstance() {
        return new LoginRegLawyerFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.f_login_reg_lawyer, container, false);
        ButterKnife.bind(this, v);
        LoginButton facebook_login = v.findViewById(R.id.facebook_login);
        mListener.facebookButton(facebook_login);

        et_name.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "sf_ui_display_dold.ttf"));
        et_email.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "sf_ui_display_dold.ttf"));
        et_passwd.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "sf_ui_display_dold.ttf"));
        et_repeat_passwd.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "sf_ui_display_dold.ttf"));
        et_city.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "sf_ui_display_dold.ttf"));
        et_phone.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "sf_ui_display_dold.ttf"));
        et_category.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "sf_ui_display_dold.ttf"));

        til_name.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "sf_uI_display_regular.ttf"));
        til_email.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "sf_uI_display_regular.ttf"));
        til_passwd.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "sf_uI_display_regular.ttf"));
        til_repeat_passwd.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "sf_uI_display_regular.ttf"));
        til_city.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "sf_uI_display_regular.ttf"));
        til_phone.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "sf_uI_display_regular.ttf"));
        til_category.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "sf_uI_display_regular.ttf"));

        et_name.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                presenter.getmUser().setName(s.toString());
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (til_name.isErrorEnabled()) {
                    til_name.setErrorEnabled(false);
                }
            }
        });

        et_email.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                presenter.getmUser().setEmail(s.toString());
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (til_email.isErrorEnabled()) {
                    til_email.setErrorEnabled(false);
                }
            }
        });

        et_passwd.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (til_passwd.isErrorEnabled()) {
                    til_passwd.setErrorEnabled(false);
                }
            }
        });

        et_repeat_passwd.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (til_repeat_passwd.isErrorEnabled()) {
                    til_repeat_passwd.setErrorEnabled(false);
                }
            }
        });

        et_city.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                presenter.getmUser().setCity(s.toString());
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (til_city.isErrorEnabled()) {
                    til_city.setErrorEnabled(false);
                }
            }
        });

        et_category.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (til_category.isErrorEnabled()) {
                    til_category.setErrorEnabled(false);
                }
            }
        });

        et_phone.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                presenter.getmUser().setPhone(s.toString());
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (til_phone.isErrorEnabled()) {
                    til_phone.setErrorEnabled(false);
                }
            }
        });

        et_city.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    chooseCity();
                }
            }
        });

        et_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseCity();
            }
        });

        et_category.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    chooseSkil();
                }
            }
        });

        et_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseSkil();
            }
        });

        radioGroup.check(R.id.rb_single);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.preinitUser(true);
    }


    private ChooseCityDialog cityDialog;
    private void chooseCity() {
        et_city.setClickable(false);
        if (cityDialog == null) {
            cityDialog = ChooseCityDialog.newInstance(et_city.getText().toString());
            cityDialog.registerEvent(new ChooseCityDialog.Event() {
                @Override
                public void isDismis() {
                    et_city.setClickable(true);
                    cityDialog = null;
                }

                @Override
                public void confirm(String city) {
                    et_city.setText(city);
                    presenter.getmUser().setCity(city);
                }
            });
            cityDialog.show(getActivity().getSupportFragmentManager(), cityDialog.getClass().getSimpleName());
        }
    }

    EditSkilsDialog dialog;
    private void chooseSkil() {

        if(dialog == null){
            dialog = EditSkilsDialog.newInstance(presenter.getmUser().getSkils(), new Gson().toJson(((MainActivity) getActivity()).getListCategory()));
            dialog.registerEvent(new EditSkilsDialog.Event() {
                @Override
                public void isDismis() {
                    dialog = null;
                }

                @Override
                public void confirm(String resutl) {
                    presenter.setSkils(resutl);
                    presenter.getmUser().setSkils(resutl);
                    et_category.setText(Function.getStringSkilsMultiple(resutl, ((MainActivity) getActivity()).getListCategory()));
                }
            });
            dialog.show(getActivity().getSupportFragmentManager(), dialog.getClass().getSimpleName());
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;

    }

    @Override
    public void initPageSaccess() {

    }

    @Override
    public void initPageError(String text) {
        Function.showToast(text);

    }

    @Override
    public void error(String error) {
        Function.showToast(error);
        dialogWait.dismiss();

    }

    @Override
    public void saccess() {
        dialogWait.dismiss();
        mListener.finishRegistrationInFragment();

    }

    @Override
    public void userReady() {
        MUser user = presenter.getmUser();
        if (!TextUtils.isEmpty(user.getName())) et_name.setText(user.getName());
        if (!TextUtils.isEmpty(user.getEmail())) et_email.setText(user.getEmail());
        if (!TextUtils.isEmpty(user.getCity())) et_city.setText(user.getCity());

        int padding = 14;
        if (presenter.getPhoto() == null && presenter.getmUser().getPhoto() == null) {
            iv_photo_user.setImageResource(R.drawable.ic_add_photo);
            iv_photo_user.setPadding(padding, padding, padding, padding);
        } else {
            tv_user_photo.setTextColor(getContext().getResources().getColor(R.color.colorTextLight));
            iv_photo_user.setPadding(0, 0, 0, 0);
            GlideApp.with(this)
                    .load(presenter.getPhoto() != null ? presenter.getPhoto() : presenter.getmUser().getPhoto())
                    .centerCrop()
                    .transform(new MultiTransformation<Bitmap>(new CircleCrop(), new RoundedCornersTransformation(30, 10)))
                    .into(iv_photo_user);
        }

        if (presenter.getDocument() != null) {
            setPhotoDocuments(presenter.getDocument());
            iv_photo_documents.setPadding(0, 0, 0, 0);
        } else {
            iv_photo_documents.setImageResource(R.drawable.ic_add_photo);
            iv_photo_documents.setPadding(padding, padding, padding, padding);
        }

        FirebaseUser suser = FirebaseAuth.getInstance().getCurrentUser();
        if (suser != null) {
            et_repeat_passwd.setText("12345678");
            et_passwd.setText("12345678");
            fi_registrationFB.setVisibility(View.GONE);
            et_passwd.setClickable(false);
            et_passwd.setFocusable(false);
            et_repeat_passwd.setClickable(false);
            et_repeat_passwd.setFocusable(false);
            et_name.setClickable(false);
            et_name.setFocusable(false);
            et_email.setClickable(false);
            et_email.setFocusable(false);
            if (suser.getPhotoUrl() != null) {
                iv_photo_user.setClickable(false);
            }
        }

    }

    public interface OnFragmentInteractionListener {
        void registration(String passwd, MUser mUser, Uri userPhoto, Uri documentsPhoto);

        void facebookButton(LoginButton facebook_login);

        void goToLogin();

        void finishRegistrationInFragment();

        void putPhoto(boolean isClient, boolean isDocuments);
    }

    public void setPhotoUser(Uri uri) {
        presenter.setPhoto(uri);
        tv_user_photo.setTextColor(getContext().getResources().getColor(R.color.colorTextLight));
        iv_photo_user.setPadding(0, 0, 0, 0);
        GlideApp.with(this)
                .load(uri)
                .centerCrop()
                .transform(new MultiTransformation<Bitmap>(new CircleCrop(), new RoundedCornersTransformation(30, 10)))
                .into(iv_photo_user);

    }

    public void setPhotoDocuments(Uri uri) {
        presenter.setDocument(uri);
        tv_document_photo.setTextColor(getContext().getResources().getColor(R.color.colorTextLight));
        iv_photo_documents.setPadding(0, 0, 0, 0);
        GlideApp.with(this)
                .load(uri)
                .centerCrop()
                .transform(new MultiTransformation<Bitmap>(new CircleCrop(), new RoundedCornersTransformation(30, 10)))
                .into(iv_photo_documents);

    }

    public void preinitUser() {
        presenter.preinitUser(true);
    }
}

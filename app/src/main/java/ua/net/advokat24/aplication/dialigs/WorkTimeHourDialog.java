package ua.net.advokat24.aplication.dialigs;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import ua.net.advokat24.MyApp;
import ua.net.advokat24.R;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.BaseModel;
import com.shawnlin.numberpicker.NumberPicker;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.net.advokat24.util.Function;

public class WorkTimeHourDialog extends DialogFragment {

    private static final String KEY_FROM = "workTimeStart";
    private static final String KEY_TO = "workTimeEnt";

    private int startHour = 0;
    private int startMinute = 0;
    private int endHour = 0;
    private int endMinute = 0;

    public static WorkTimeHourDialog newInstance(String from, String to) {
        WorkTimeHourDialog fragment = new WorkTimeHourDialog();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_FROM, from);
        bundle.putString(KEY_TO, to);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){

            String from = getArguments().getString(KEY_FROM);
            String to = getArguments().getString(KEY_TO);

            if (from != null) {
                startHour = Integer.parseInt(from.substring(0,2));
                startMinute = Integer.parseInt(from.substring(3,5));
            }

            if (to != null) {
                endHour = Integer.parseInt(to.substring(0,2));
                endMinute = Integer.parseInt(to.substring(3,5));
            }
        }
    }

    @BindView(R.id.np_hour_from)NumberPicker np_hour_from;
    @BindView(R.id.np_minutes_from)NumberPicker np_minutes_from;
    @BindView(R.id.np_hour_to)NumberPicker np_hour_to;
    @BindView(R.id.np_minutes_to)NumberPicker np_minutes_to;

    @OnClick(R.id.tv_ok) void tv_ok() {
        String mfrom = getStrTime(np_hour_from.getValue()) + getStrTime(np_minutes_from.getValue());
        String mto = getStrTime(np_hour_to.getValue()) + getStrTime(np_minutes_to.getValue());
        if(Integer.parseInt(mto) <= Integer.parseInt(mfrom) ){
            Function.showToast(getContext().getResources().getString(R.string.time_invalid));
        } else {
            String from = getStrTime(np_hour_from.getValue()) + ":" + getStrTime(np_minutes_from.getValue());
            String to = getStrTime(np_hour_to.getValue()) + ":" + getStrTime(np_minutes_to.getValue());
            event.result(from, to);
            dismiss();
        }
    }

    private String getStrTime(int input){
        String outPut = String.valueOf(input);
        if(outPut.length() == 1)
            outPut = "0" + outPut;
        return outPut;
    }

    @OnClick(R.id.tv_cancel) void tv_cancel() {
        dismiss();
    }

    @OnClick(R.id.fl_out_click) void fl_out_click() {
        dismiss();
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.backgraund_dialogs);
        View v = inflater.inflate(R.layout.d_edit_work_time_hour, container, false);
        ButterKnife.bind(this, v);

        np_hour_from.setValue(startHour);
        np_minutes_from.setValue(startMinute);
        np_hour_to.setValue(endHour);
        np_minutes_to.setValue(endMinute);

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }
    @Override
    public void onDismiss(DialogInterface dialog) {
        event.dismiss();
        super.onDismiss(dialog);
    }
    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimationNew;
    }

    public interface Event {
        void result(String from, String to);
        void dismiss();
    }
    private Event event;
    public void registerEvent(Event event){
        this.event = event;
    }
}
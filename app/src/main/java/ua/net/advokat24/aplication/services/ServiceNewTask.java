package ua.net.advokat24.aplication.services;

import android.app.ActivityManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

import com.google.gson.Gson;
import ua.net.advokat24.MyApp;
import ua.net.advokat24.R;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.repository.firebase.createtask.CreateTaskImpl;
import ua.net.advokat24.repository.firebase.createtask.CreateTaskInteractor;
import ua.net.advokat24.repository.firebase.createtask.CreateTaskListener;
import ua.net.advokat24.repository.firebase.getusers.GetUsersImpl;
import ua.net.advokat24.repository.firebase.getusers.GetUsersInteractor;
import ua.net.advokat24.repository.firebase.getusers.GetUsersListener;
import ua.net.advokat24.repository.sendpushnotify.MessageSendImpl;
import ua.net.advokat24.repository.sendpushnotify.MessageSendInteractor;
import ua.net.advokat24.repository.sendpushnotify.interfaces.OnDataSendListener;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.MTask;
import ua.net.advokat24.util.models.MassegFromService;
import ua.net.advokat24.util.models.message_firebase.Message;
import ua.net.advokat24.util.models.usermodels.MUser;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import ua.net.advokat24.repository.firebase.createtask.CreateTaskImpl;
import ua.net.advokat24.repository.firebase.createtask.CreateTaskInteractor;
import ua.net.advokat24.repository.firebase.getusers.GetUsersImpl;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.MassegFromService;
import ua.net.advokat24.util.models.message_firebase.Message;

public class ServiceNewTask extends IntentService {

    private Timer timer;
    private Intent intent;
    public final static String KEY = "serviceNewTask";
    public final static String KEY_ACTIONS = "actionsKey";
    public final static String KEY_POST_ID = "postIdKey";
    public static boolean serviceIsRun = false;
    private List<MUser> lawyers;
    private List<MUser> lawyersDuble;
    private boolean isFirstSearch = false;
    private MTask mTask;
    private MessageSendInteractor messageSendInteractor;
    private CreateTaskInteractor createTaskInteractor;

    public ServiceNewTask() {
        super(".aplication.services.ServiceNewTask");
    }

    @Override
    public void onDestroy() {
        Function.log("onDestroy()", getClass().getName());
        stopSelf();
        stopService(intent);
        stopForeground(true);
        super.onDestroy();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        this.intent = intent;
        mTask = intent.getParcelableExtra(KEY);
        if (mTask != null) {
            messageSendInteractor = new MessageSendImpl();
            GetUsersInteractor getUsersInteractor = new GetUsersImpl();
            createTaskInteractor = new CreateTaskImpl();
            getUsersInteractor.getUsersForTask(mTask, new GetUsersListener() {
                @Override
                public void resultSearchUsers(List<MUser> lawyers) {
                    Function.log(String.valueOf(lawyers.size()), getClass().getName());
                    if(lawyers.size() == 0){
                        sendResultToClient(MassegFromService.ACTION_LAWYERS_NOT_FOUND, mTask.getId());
                        onDestroy();
                    } else {
                        ServiceNewTask.this.lawyers = lawyers;
                        ServiceNewTask.this.lawyersDuble = new ArrayList<>(lawyers);
                        isFirstSearch = true;
                        createTaskInteractor.createTask(mTask, new CreateTaskListener() {
                            @Override
                            public void saccess(String lawyerId) {
                                sendResultToClient(MassegFromService.ACTION_LAWYERS_IS_FOUND, lawyerId);
                                onDestroy();
                            }

                            @Override
                            public void lawyerDeny() {
                                if (timer != null) {
                                    timer.cancel();
                                }
                                startSending();
                            }
                        });
                        sendResultToClient(MassegFromService.ACTION_LAWYERS_DEFOULT, mTask.getId());
                        startSending();
                    }
                }

                @Override
                public void error() {
                    sendResultToClient(MassegFromService.ACTION_LAWYERS_NOT_FOUND, mTask.getId());
                    onDestroy();
                }
            });
        }
    }

    private void sendResultToClient(int action, String subjectId) {
        if (timer != null) {
            timer.cancel();
        }
        if (isForeground(getPackageName())) {
            EventBus.getDefault().post(new MassegFromService(action, subjectId));
        } else {
            startNotification(action, subjectId);
        }
    }

    public boolean isForeground(String myPackage) {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfo = manager.getRunningTasks(1);
        ComponentName componentInfo = runningTaskInfo.get(0).topActivity;
        return componentInfo.getPackageName().equals(myPackage);
    }

    private void startSending() {
        if(CreateTaskImpl.isRun){
            timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    if (lawyers.size() != 0) {
                        messageSendInteractor.sendNotifycation(new Gson().toJson(new Message(mTask.getId(), lawyers.get(0).getNotificationKay(), MassegFromService.ACTION_FOUND_NEW_TASK)), new OnDataSendListener() {
                            @Override
                            public void onFailedReqwest(String message) {
                                timer.cancel();
                                startSending();
                            }

                            @Override
                            public void onSaccessReqwest() {}
                        });
                        lawyers.remove(0);
                    } else {
                        if(isFirstSearch){
                            isFirstSearch = false;
                            lawyers = lawyersDuble;
                            startSending();
                        } else {
                            if (timer != null) {
                                timer.cancel();
                            }
                            if(CreateTaskImpl.isRun){
                                createTaskInteractor.close();
                                sendResultToClient(MassegFromService.ACTION_LAWYERS_NOT_FOUND, mTask.getId());
                                onDestroy();
                            }
                        }
                    }
                }
            }, 0, 150000);//5000 = 5 Seconds
        }
    }

    private void startNotification(int action, String subjectId) {
        String myTitle = "";
        String myContentText = "";
        switch (action){
            case MassegFromService.ACTION_LAWYERS_DEFOULT:
                break;
            case MassegFromService.ACTION_LAWYERS_NOT_FOUND:
                myTitle = MyApp.getContext().getResources().getString(R.string.notify_lawyer_not_found);
                myContentText = MyApp.getContext().getResources().getString(R.string.notify_lawyer_not_found_sub);
                break;
            case MassegFromService.ACTION_LAWYERS_IS_FOUND:
                myTitle = MyApp.getContext().getResources().getString(R.string.notify_lawyer_found);
                myContentText = MyApp.getContext().getResources().getString(R.string.notify_lawyer_found_sub);
                break;
        }

        int mNotificationId = 001;
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_user_check_off)
                        .setContentTitle(myTitle)
                        .setContentText(myContentText)
                        .setAutoCancel(true)
                        .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.drawable.lawyer_button)) // большая
                        .setOngoing(true);



        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(KEY_ACTIONS, action);
        intent.putExtra(KEY_POST_ID, subjectId);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(contentIntent);
        NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(mNotificationId, mBuilder.build());
    }
}
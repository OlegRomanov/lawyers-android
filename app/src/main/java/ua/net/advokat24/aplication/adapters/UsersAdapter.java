package ua.net.advokat24.aplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import ua.net.advokat24.R;
import ua.net.advokat24.aplication.helpactivitys.LawyerDetailsActivity;
import ua.net.advokat24.util.models.usermodels.MUser;
import ua.net.advokat24.util.utils.ImageUtils;

import java.util.ArrayList;
import java.util.List;

import ua.net.advokat24.aplication.helpactivitys.LawyerDetailsActivity;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> {

    private List<MUser> list;
    private Context context;

    public UsersAdapter(Context context) {
        this.list = new ArrayList<>();
        this.context = context;
    }

    @NonNull
    @Override
    public UsersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.i_user_field, parent, false);
        return new UsersAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final UsersAdapter.ViewHolder holder , int position) {
        final MUser model = list.get(position);

        holder.tv_name.setText(model.getName());

        if (model.getReiting() != 0 && model.getReitingCount() != 0) {
            holder.tv_reiting.setText(String.valueOf((double) model.getReiting() / model.getReitingCount()));
        }

        ImageUtils.loadPhoto(context, model, (holder.iv_user_photo));
//        if(model.getPhoto() != null){
//            GlideApp.with(context)
//                    .load(model.getPhoto())
//                    .centerCrop()
//                    .transform(new MultiTransformation<Bitmap>(new CircleCrop()))
//                    .into(holder.iv_user_photo);
//        } else {
//            GlideApp.with(context)
//                    .load(FirebaseStorage.getInstance().getReference().child("images/userphoto/" + model.getId() + ".jpg"))
//                    .centerCrop()
//                    .transform(new MultiTransformation<Bitmap>(new CircleCrop()))
//                    .error(R.drawable.ic_user_check_off)
//                    .into(holder.iv_user_photo);
//        }

        holder.fl_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent activity = new Intent(context, LawyerDetailsActivity.class);
                activity.putExtra(LawyerDetailsActivity.KEY, model.getId());
                context.startActivity(activity);
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_name, tv_reiting;
        private ImageView iv_user_photo;
        private FrameLayout fl_click;

        ViewHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_reiting = itemView.findViewById(R.id.tv_reiting);
            iv_user_photo = itemView.findViewById(R.id.iv_user_photo);
            fl_click = itemView.findViewById(R.id.fl_click);
        }
    }

    public void setList(List<MUser> list){
        this.list = list;
        notifyDataSetChanged();
    }

}

package ua.net.advokat24.aplication.main.fragments.listfragment.clientlist;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ua.net.advokat24.R;
import ua.net.advokat24.util.fragmentAnimator.MFragmentManager;
import ua.net.advokat24.util.fragmentAnimator.NewFragment;
import ua.net.advokat24.util.fragmentAnimator.ReplaceToLeft;
import ua.net.advokat24.util.fragmentAnimator.ReplaceToRight;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.net.advokat24.util.fragmentAnimator.MFragmentManager;
import ua.net.advokat24.util.fragmentAnimator.NewFragment;
import ua.net.advokat24.util.fragmentAnimator.ReplaceToLeft;
import ua.net.advokat24.util.fragmentAnimator.ReplaceToRight;

public class SearchResultFragment extends Fragment {

    private FragmentPagerAdapter adapter;

    private ClientMapFragment clientMapFragment;
    private ClientListFragment clientListFragment;

    public SearchResultFragment() {}
    public static SearchResultFragment newInstance() {
        return new SearchResultFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        clientMapFragment = ClientMapFragment.newInstance();
        clientListFragment = ClientListFragment.newInstance();
    }

    @BindView(R.id.tv_map) TextView tv_map;
    @OnClick(R.id.tv_map) void goTomap() {

        tv_map.setBackground(getContext().getResources().getDrawable(R.drawable.img_tab_toolbar_select));
        tv_map.setTextColor(getContext().getResources().getColor(R.color.colorToolbar));
        tv_list.setBackground(null);
        tv_list.setTextColor(getContext().getResources().getColor(R.color.colorWhite));

        mFragmentManager.setAction(new ReplaceToRight());
        mFragmentManager.doTransaction(getChildFragmentManager(), clientMapFragment, R.id.map_conteiner);
    }

    @BindView(R.id.tv_list) TextView tv_list;
    @OnClick(R.id.tv_list) void goToList() {

        tv_list.setBackground(getContext().getResources().getDrawable(R.drawable.img_tab_toolbar_select));
        tv_list.setTextColor(getContext().getResources().getColor(R.color.colorToolbar));
        tv_map.setBackground(null);
        tv_map.setTextColor(getContext().getResources().getColor(R.color.colorWhite));

        mFragmentManager.setAction(new ReplaceToLeft());
        mFragmentManager.doTransaction(getChildFragmentManager(), clientListFragment, R.id.map_conteiner);
    }

    private MFragmentManager mFragmentManager = new MFragmentManager();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.f_search_result, container, false);
        ButterKnife.bind(this, v);



        mFragmentManager.setAction(new NewFragment());
        mFragmentManager.doTransaction(getChildFragmentManager(), clientMapFragment, R.id.map_conteiner);
        tv_map.setBackground(getContext().getResources().getDrawable(R.drawable.img_tab_toolbar_select));
        tv_map.setTextColor(getContext().getResources().getColor(R.color.colorToolbar));
        tv_list.setBackground(null);
        tv_list.setTextColor(getContext().getResources().getColor(R.color.colorWhite));


//        if(adapter != null){
//            rl_client_page.setVisibility(View.VISIBLE);
//            view_pager.setAdapter(adapter);
//            view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//                public void onPageScrollStateChanged(int state) {}
//                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
//                public void onPageSelected(int position) {
//                    if (position == 0){
//                        tv_map.setBackground(getContext().getResources().getDrawable(R.drawable.img_tab_toolbar_select));
//                        tv_map.setTextColor(getContext().getResources().getColor(R.color.colorToolbar));
//                        tv_list.setBackground(null);
//                        tv_list.setTextColor(getContext().getResources().getColor(R.color.colorWhite));
//                    } else {
//                        tv_list.setBackground(getContext().getResources().getDrawable(R.drawable.img_tab_toolbar_select));
//                        tv_list.setTextColor(getContext().getResources().getColor(R.color.colorToolbar));
//                        tv_map.setBackground(null);
//                        tv_map.setTextColor(getContext().getResources().getColor(R.color.colorWhite));
//                    }
//                }
//            });
//        } else {
//            fl_lawyer_page.setVisibility(View.VISIBLE);
//            MFragmentManager mFragmentManager = new MFragmentManager();
//            mFragmentManager.setAction(new NewFragment());
//            mFragmentManager.doTransaction(getChildFragmentManager(), LawyerListFragment.newInstance(), R.id.fl_conteiner_fragments);
//        }
        return v;
    }
}

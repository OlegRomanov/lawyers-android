package ua.net.advokat24.aplication.main.fragments.lawyerinfo;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.provider.Settings;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DatabaseReference;
import com.google.gson.Gson;
import ua.net.advokat24.aplication.dialigs.ChooseCityDialog;
import ua.net.advokat24.aplication.dialigs.ChooseCompanyDialog;
import ua.net.advokat24.aplication.dialigs.ChooseStatusDialog;
import ua.net.advokat24.aplication.dialigs.ConfirmDialog;
import ua.net.advokat24.aplication.dialigs.EditSkilsDialog;
import ua.net.advokat24.aplication.dialigs.LocationDialog;
import ua.net.advokat24.aplication.dialigs.WorkTimeDialog;
import ua.net.advokat24.util.Fields;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.R;
import ua.net.advokat24.aplication.dialigs.ChooseSkilsDialog;
import ua.net.advokat24.aplication.dialigs.EditTextDialog;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.util.models.BaseModel;
import ua.net.advokat24.util.models.usermodels.MUser;
import ua.net.advokat24.util.utils.FirebaseUtils;
import ua.net.advokat24.util.utils.ImageUtils;
import ua.net.advokat24.util.utils.curent_location.LocationUtils;
import ua.net.advokat24.util.utils.UserStatusUtils;
import ua.net.advokat24.util.models.usermodels.WorkTime;
import ua.net.advokat24.util.utils.curent_location.LocationInteractor;
import ua.net.advokat24.util.utils.curent_location.LocationImpl;
import ua.net.advokat24.util.utils.curent_location.LocationListener;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.net.advokat24.util.Function;

public class LawyerInfoFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private MUser mUser;

    public LawyerInfoFragment() {}

    @BindView(R.id.ll_app_bar_user_info) LinearLayout ll_app_bar_user_info;
    @BindView(R.id.iv_toolbar_backgraond) ImageView iv_toolbar_backgraond;
    @BindView(R.id.app_bar) AppBarLayout app_bar;

    @BindView(R.id.tv_app_bar_user_name) TextView tv_app_bar_user_name;
    @BindView(R.id.tv_app_bar_user_discription) TextView tv_app_bar_user_discription;
    @BindView(R.id.iv_app_bar_user_photo) ImageView iv_app_bar_user_photo;

    @BindView(R.id.fl_field_name) FrameLayout fl_field_name;
    @BindView(R.id.fl_field_company) FrameLayout fl_field_company;
    @BindView(R.id.fl_field_position) FrameLayout fl_field_position;
    @BindView(R.id.fl_field_work_time) FrameLayout fl_field_work_time;
    @BindView(R.id.fl_skils) FrameLayout fl_skils;
    @BindView(R.id.fl_location) FrameLayout fl_location;

    @BindView(R.id.tv_field_name) TextView tv_field_name;
    @BindView(R.id.tv_field_company) TextView tv_field_company;
    @BindView(R.id.tv_field_position) TextView tv_field_position;
    @BindView(R.id.tv_field_work_time) TextView tv_field_work_time;
    @BindView(R.id.tv_city) TextView tv_city;
    @BindView(R.id.tv_skils) TextView tv_skils;
    @BindView(R.id.tv_status) TextView tv_status;
    @BindView(R.id.tv_location) TextView tv_location;

    @BindView(R.id.fl_city) FrameLayout fl_city;

    @OnClick(R.id.iv_toolbar_action) void exit() {
        ConfirmDialog dialog = ConfirmDialog.newInstance(
                getContext().getResources().getString(R.string.go_out), getContext().getResources().getString(R.string.you_exit_dialog),
                getContext().getResources().getString(R.string.go_out), getContext().getResources().getString(R.string.cancel));
        dialog.registerEvent(new ConfirmDialog.Event() {
            @Override
            public void confirm() {
                DatabaseReference mDatabase = FirebaseUtils.getDatabase().getReference(Fields.USERS_TABTE);
                mDatabase.child(mUser.getId()).child("notificationKay").setValue(null);
                if (mListener != null) {
                    mListener.logOut();
                }
            }

            @Override
            public void fail() {}

            @Override
            public void isDismis() {}
        });
        dialog.show(getActivity().getSupportFragmentManager(), dialog.getClass().getSimpleName());
    }

    @OnClick(R.id.fl_status) void fl_status() {
        ChooseStatusDialog dialog = ChooseStatusDialog.newInstance(new Gson().toJson(UserStatusUtils.getStatusList()));
        dialog.registerEvent(new ChooseStatusDialog.Event() {
            @Override
            public void result(BaseModel baseModel) {
                ((MainActivity)getActivity()).getmUser().setStatus(Integer.parseInt(baseModel.getId()));
                ((MainActivity)getActivity()).saveUser();
                UserStatusUtils.setStatus(((MainActivity)getActivity()).getmUser().getStatus(), tv_status);
            }

            @Override
            public void dismiss() {

            }
        });
        dialog.show(getActivity().getSupportFragmentManager(), dialog.getClass().getSimpleName());
    }

    @OnClick(R.id.fl_field_name) void fl_field_name() {
        fl_field_name.setClickable(false);
        EditTextDialog dialogText = EditTextDialog.newInstance(getContext().getResources().getString(R.string.enter_you_name),
                getContext().getResources().getString(R.string.enter_you_name_and_subname_norm),
                getContext().getResources().getString(R.string.confirm),
                getContext().getResources().getString(R.string.cancel),
                tv_field_name.getText().toString());
        dialogText.registerEvent(new EditTextDialog.Event() {
            @Override
            public void confirm(String text) {
                tv_app_bar_user_name.setText(text + getContext().getResources().getString(R.string.my_profile_you));
                tv_field_name.setText(text);
                ((MainActivity)getActivity()).getmUser().setName(text);
                ((MainActivity)getActivity()).saveUser();
            }

            @Override
            public void dismiss() {
                fl_field_name.setClickable(true);
            }
        });
        dialogText.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), dialogText.getClass().getSimpleName());
    }


    @OnClick(R.id.fl_city) void clickCity() {
        fl_city.setClickable(false);
        ChooseCityDialog cityDialog = ChooseCityDialog.newInstance(mUser.getCity());
        cityDialog.registerEvent(new ChooseCityDialog.Event() {
            @Override
            public void isDismis() {
                fl_city.setClickable(true);
            }

            @Override
            public void confirm(String city) {
                tv_city.setText(city);
                ((MainActivity)getActivity()).getmUser().setCity(city);
                ((MainActivity)getActivity()).saveUser();

            }
        });
        cityDialog.show(getActivity().getSupportFragmentManager(), cityDialog.getClass().getSimpleName());
    }

    @OnClick(R.id.iv_toolbar_back) void iv_toolbar_back() {
        if (mListener != null) {
            mListener.backToHome();
        }
    }

    @OnClick(R.id.iv_app_bar_user_photo) void putImage() {
        if (mListener != null) {
            mListener.putPhoto(false, false);
        }
    }


    @OnClick(R.id.fl_field_company) void editCompany() {
        fl_field_company.setClickable(false);
        final ChooseCompanyDialog dialog = ChooseCompanyDialog.newInstance(tv_field_company.getText().toString(), mUser.getId());
        dialog.registerEvent(new ChooseCompanyDialog.Event() {
            @Override
            public void isDismis() {
                fl_field_company.setClickable(true);
            }

            @Override
            public void confirm(String company) {

                tv_field_company.setText(company);
                ((MainActivity)getActivity()).getmUser().setCompany(company);
                ((MainActivity)getActivity()).saveUser();
                dialog.dismiss();
            }
        });
        dialog.show(getActivity().getSupportFragmentManager(), dialog.getClass().getSimpleName());
    }

    @OnClick(R.id.fl_field_position) void editPosition() {
        fl_field_position.setClickable(false);
        EditTextDialog dialogText = EditTextDialog.newInstance(getContext().getResources().getString(R.string.you_position),
                getContext().getResources().getString(R.string.user_position_subtext),
                getContext().getResources().getString(R.string.confirm),
                getContext().getResources().getString(R.string.cancel),
                tv_field_position.getText().toString());
        dialogText.registerEvent(new EditTextDialog.Event() {
            @Override
            public void confirm(String text) {
                tv_field_position.setText(text);
                ((MainActivity)getActivity()).getmUser().setPosition(text);
                ((MainActivity)getActivity()).saveUser();
            }

            @Override
            public void dismiss() {
                fl_field_position.setClickable(true);
            }
        });
        dialogText.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), dialogText.getClass().getSimpleName());
    }

    @OnClick(R.id.fl_field_work_time) void editWorkTime() {
        fl_field_work_time.setClickable(true);
        WorkTimeDialog dialog = WorkTimeDialog.newInstance(new Gson().toJson(((MainActivity) getActivity()).getmUser().getWorkTime()));
        dialog.registerEvent(new WorkTimeDialog.Event() {
            @Override
            public void confirm(WorkTime workTime) {
                tv_field_work_time.setText(workTime.toStringMy(getContext()));
                ((MainActivity) getActivity()).getmUser().setWorkTime(workTime);
                ((MainActivity) getActivity()).saveUser();
            }

            @Override
            public void dismiss() {
                fl_field_work_time.setClickable(true);
            }
        });
        dialog.show(getActivity().getSupportFragmentManager(), dialog.getClass().getSimpleName());
    }

    @OnClick(R.id.fl_skils) void editSkils() {
        fl_skils.setClickable(false);
        EditSkilsDialog dialog = EditSkilsDialog.newInstance(mUser.getSkils(), new Gson().toJson(((MainActivity) getActivity()).getListCategory()));
        dialog.registerEvent(new EditSkilsDialog.Event() {
            @Override
            public void isDismis() {
                fl_skils.setClickable(true);
            }

            @Override
            public void confirm(String city) {
                tv_skils.setText(Function.getStringSkilsMultiple(city, ((MainActivity) getActivity()).getListCategory()));
                mUser.setSkils(city);
                ((MainActivity) getActivity()).getmUser().setSkils(city);
                ((MainActivity) getActivity()).saveUser();
            }
        });
        dialog.show(getActivity().getSupportFragmentManager(), dialog.getClass().getSimpleName());
    }

    @OnClick(R.id.fl_location) void fl_location() {
        fl_location.setClickable(true);
        LocationDialog dialog = LocationDialog.newInstance();
        dialog.registerEvent(new LocationDialog.Event() {
            @Override
            public void confirm(boolean isGPS) {
                if(isGPS){
                    Function.log("isGPS start", getClass().getName());
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                        Function.log("isGPS if", getClass().getName());
                        setUserLocation();
                    } else {
                        Function.log("isGPS else", getClass().getName());
                        ((MainActivity) getActivity()).startPermissionDialog(MainActivity.LOCATION_PERMISSION_USER);
                    }
                } else {
                    ((MainActivity) getActivity()).startAutocompile(MainActivity.LOCATION_PERMISSION_USER);
                }
            }

            @Override
            public void dismiss() {
                fl_location.setClickable(true);
            }
        });
        dialog.show(getActivity().getSupportFragmentManager(), dialog.getClass().getSimpleName());
    }

    public void setUserLocation(){
        final ProgressDialog dialogWait = ProgressDialog.show(getContext(), "", getContext().getResources().getString(R.string.loading), false, false);
        LocationInteractor interactor = new LocationImpl();
        interactor.getCurentLocation(getActivity(), new LocationListener() {
            @Override
            public void licationIs(Location location) {
                ((MainActivity) getActivity()).getmUser().setLocatoinLat(String.valueOf(location.getLatitude()));
                ((MainActivity) getActivity()).getmUser().setLocatoinLon(String.valueOf(location.getLongitude()));
                String adress = LocationUtils.getAdressFromLocation(new LatLng(Double.parseDouble(String.valueOf(location.getLatitude())), Double.parseDouble(String.valueOf(location.getLongitude()))), getActivity());
                tv_location.setText(adress);
                ((MainActivity) getActivity()).getmUser().setLocatoinAdress(adress);
                ((MainActivity) getActivity()).saveUser();
                if (dialogWait.isShowing())
                    dialogWait.dismiss();
            }

            @Override
            public void permissionMiss() {
                if (dialogWait.isShowing())
                    dialogWait.dismiss();
                Function.showToast(getResources().getString(R.string.error_gps_permission_reqwest));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            }

            @Override
            public void error() {
                if (dialogWait.isShowing())
                    dialogWait.dismiss();
                Function.showToast(getResources().getString(R.string.error_gps_reqwest));
            }
        });
    }

    public void setAdress(Place place){
        ((MainActivity) getActivity()).getmUser().setLocatoinLat(String.valueOf(place.getLatLng().latitude));
        ((MainActivity) getActivity()).getmUser().setLocatoinLon(String.valueOf(place.getLatLng().longitude));
        ((MainActivity) getActivity()).getmUser().setLocatoinAdress(place.getAddress().toString());
        tv_location.setText(place.getAddress().toString());
        ((MainActivity) getActivity()).saveUser();
    }

    public static LawyerInfoFragment newInstance() {
        return new LawyerInfoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
         View v = inflater.inflate(R.layout.f_lawyer_info, container, false);
         ButterKnife.bind(this, v);
         mUser = ((MainActivity)getActivity()).getmUser();
         if(!TextUtils.isEmpty(mUser.getName())) tv_app_bar_user_name.setText(mUser.getName() + getContext().getResources().getString(R.string.my_profile_you));
         if(!TextUtils.isEmpty(mUser.getName())) tv_field_name.setText(mUser.getName());
         loadPhoto(mUser);
         if(!TextUtils.isEmpty(mUser.getCity())) tv_city.setText(mUser.getCity());
         if(!TextUtils.isEmpty(mUser.getCompany())) tv_field_company.setText(mUser.getCompany());
         if(!TextUtils.isEmpty(mUser.getPosition())) tv_field_position.setText(mUser.getPosition());
         if(!TextUtils.isEmpty(mUser.getLocatoinAdress())) tv_location.setText(mUser.getLocatoinAdress());
//         tv_app_bar_user_discription.setText(Function.getStringProfileSkils(mUser.getSkils(), ((MainActivity)getActivity()).getListCategory()));

         tv_field_work_time.setText(mUser.getWorkTime().toStringMy(getContext()));
         UserStatusUtils.setStatus(mUser.getStatus(), tv_status);
         tv_skils.setText(Function.getStringSkilsMultiple(mUser.getSkils(), ((MainActivity)getActivity()).getListCategory()));
//
         app_bar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
             @Override
             public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                 int maxScroll = appBarLayout.getTotalScrollRange() / 2;
                 float percentage = ((float) Math.abs(verticalOffset) / ((float) maxScroll / 2)) - 1.5f;
                 iv_toolbar_backgraond.setAlpha(percentage);
                 ll_app_bar_user_info.setAlpha(percentage * -1);
             }
         });
         return v;
    }

    public void updatePhoto(){
        Glide.get(getActivity()).clearMemory();
        iv_app_bar_user_photo.setImageDrawable(null);
        loadPhoto(mUser);
    }

    private void loadPhoto(MUser mUser){
        ImageUtils.loadPhoto(getContext(), mUser, iv_app_bar_user_photo);
    }


   @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void logOut();
        void putPhoto(boolean isClient, boolean isDocuments);
        void backToHome();
    }

}
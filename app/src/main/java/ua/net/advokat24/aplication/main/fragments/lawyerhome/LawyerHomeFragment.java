package ua.net.advokat24.aplication.main.fragments.lawyerhome;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.gson.Gson;
import ua.net.advokat24.R;
import ua.net.advokat24.aplication.dialigs.ChooseStatusDialog;
import ua.net.advokat24.aplication.dialigs.ConfirmDialog;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.util.Fields;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.BaseModel;
import ua.net.advokat24.util.models.usermodels.MUser;
import ua.net.advokat24.util.utils.FirebaseUtils;
import ua.net.advokat24.util.utils.ImageUtils;
import ua.net.advokat24.util.utils.UiUtils;
import ua.net.advokat24.util.utils.UserStatusUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.net.advokat24.aplication.dialigs.ChooseStatusDialog;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.util.Function;

public class LawyerHomeFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    @BindView(R.id.tv_app_bar_user_name)
    TextView tv_app_bar_user_name;
    @BindView(R.id.tv_app_bar_user_discription)
    TextView tv_app_bar_user_discription;
    @BindView(R.id.iv_app_bar_user_photo)
    ImageView iv_app_bar_user_photo;

    public LawyerHomeFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.tv_curent_status)
    TextView tv_curent_status;
    @BindView(R.id.tv_lawyer_tasks)
    TextView tv_lawyer_tasks;

    @OnClick(R.id.fl_click)
    void fl_click() {
        if (mListener != null) {
            ChooseStatusDialog dialog = ChooseStatusDialog.newInstance(new Gson().toJson(UserStatusUtils.getStatusList()));
            dialog.registerEvent(new ChooseStatusDialog.Event() {
                @Override
                public void result(BaseModel baseModel) {
                    ((MainActivity) getActivity()).getmUser().setStatus(Integer.parseInt(baseModel.getId()));
                    ((MainActivity) getActivity()).saveUser();
                    UserStatusUtils.setStatus(((MainActivity) getActivity()).getmUser().getStatus(), tv_curent_status);
                }

                @Override
                public void dismiss() {

                }
            });
            dialog.show(getActivity().getSupportFragmentManager(), dialog.getClass().getSimpleName());
        }
    }

    @OnClick(R.id.ll_lawyer_tasks)
    void ll_lawyer_tasks() {
        if (mListener != null) {
            mListener.autoClick(UiUtils.LIST_FRAGMENT);
        }
    }

    @OnClick(R.id.iv_toolbar_back)
    void iv_toolbar_back() {
        ConfirmDialog dialog = ConfirmDialog.newInstance(
                getContext().getResources().getString(R.string.go_out), getContext().getResources().getString(R.string.you_exit_dialog),
                getContext().getResources().getString(R.string.go_out), getContext().getResources().getString(R.string.cancel));
        dialog.registerEvent(new ConfirmDialog.Event() {
            @Override
            public void confirm() {
                getActivity().finish();
            }

            @Override
            public void fail() {}

            @Override
            public void isDismis() {}
        });
        dialog.show(getActivity().getSupportFragmentManager(), dialog.getClass().getSimpleName());
    }

    public static LawyerHomeFragment newInstance() {
        return new LawyerHomeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.f_lawyer_home, container, false);
        ButterKnife.bind(this, v);
        MUser mUser = ((MainActivity) getActivity()).getmUser();
        if (!TextUtils.isEmpty(((MainActivity) getActivity()).getmUser().getName()))
            tv_app_bar_user_name.setText(((MainActivity) getActivity()).getmUser().getName() + " (Вы)");
//        tv_app_bar_user_discription.setText(Function.getStringProfileSkils(mUser.getSkils(), ((MainActivity) getActivity()).getListCategory()));
        tv_lawyer_tasks.setText(String.valueOf(((MainActivity) getActivity()).getListMyTasks().size()));
        UserStatusUtils.setStatus(mUser.getStatus(), tv_curent_status);
        loadPhoto(mUser);
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void autoClick(int page);
    }

    private void loadPhoto(MUser mUser) {
        ImageUtils.loadPhoto(getContext(), mUser, iv_app_bar_user_photo);
    }

}
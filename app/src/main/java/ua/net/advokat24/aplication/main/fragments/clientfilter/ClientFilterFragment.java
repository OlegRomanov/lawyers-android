package ua.net.advokat24.aplication.main.fragments.clientfilter;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.google.gson.Gson;
import ua.net.advokat24.R;
import ua.net.advokat24.aplication.dialigs.ChooseCityDialog;
import ua.net.advokat24.aplication.dialigs.ChooseSkilsDialog;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.util.Function;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.net.advokat24.aplication.dialigs.ChooseCityDialog;
import ua.net.advokat24.aplication.dialigs.ChooseSkilsDialog;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.util.Function;

public class ClientFilterFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private String categoru = "";

    @BindView(R.id.range_seekbar)
    CrystalRangeSeekbar range_seekbar;
    @BindView(R.id.et_skils)
    TextInputEditText et_skils;
    @BindView(R.id.et_city)
    TextInputEditText et_city;

    @OnClick(R.id.tv_search)
    void tv_search() {
        if (mListener != null) {
            mListener.searchLawyers(categoru, et_city.getText().toString());
        }
    }

    public ClientFilterFragment() {}

    public static ClientFilterFragment newInstance() {
        return new ClientFilterFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.f_client_filter, container, false);
        ButterKnife.bind(this, v);

        et_city.setText(((MainActivity) getActivity()).getSearchCity());
        et_skils.setText(Function.getStringSkils(((MainActivity) getActivity()).getSearckCategory(), ((MainActivity) getActivity()).getListCategory()));

        range_seekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
//                ((ViewHolderSettings) viewHolder).tv_from.setText(String.valueOf(minValue));
//                ((ViewHolderSettings) viewHolder).tv_to.setText(String.valueOf(maxValue));
            }
        });
        range_seekbar.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number minValue, Number maxValue) {
//                clickCallback.click();
            }
        });


        et_city.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    chooseCity();
                }
            }
        });

        et_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseCity();
            }
        });

        et_skils.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    chooseSkil();
                }
            }
        });

        et_skils.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseSkil();
            }
        });

        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void searchLawyers(String category, String city);
    }

    private ChooseCityDialog cityDialog;
    private void chooseCity() {
        et_city.setClickable(false);
        if (cityDialog == null) {
            cityDialog = ChooseCityDialog.newInstance(et_city.getText().toString());
            cityDialog.registerEvent(new ChooseCityDialog.Event() {
                @Override
                public void isDismis() {
                    et_city.setClickable(true);
                    cityDialog = null;
                }

                @Override
                public void confirm(String city) {
                    et_city.setText(city);
                    ((MainActivity) getActivity()).getmUser().setCity(city);
                    ((MainActivity) getActivity()).saveUser();

                }
            });
            cityDialog.show(getActivity().getSupportFragmentManager(), cityDialog.getClass().getSimpleName());
        }
    }

    ChooseSkilsDialog dialogSkils;
    private void chooseSkil() {
        et_skils.setClickable(false);
        if(dialogSkils == null){
            dialogSkils = ChooseSkilsDialog.newInstance(new Gson().toJson(((MainActivity) getActivity()).getListCategory()));
            dialogSkils.registerEvent(new ChooseSkilsDialog.Event() {
                @Override
                public void result(String resutl) {
                    et_skils.setText(Function.getStringSkils(resutl, ((MainActivity) getActivity()).getListCategory()));
                    Function.printSkils(resutl, et_skils, ((MainActivity) getActivity()).getListCategory());
                    categoru = resutl;
                }

                @Override
                public void dismiss() {
                    et_skils.setClickable(true);
                    dialogSkils = null;
                }
            });
            dialogSkils.show(getActivity().getSupportFragmentManager(), dialogSkils.getClass().getSimpleName());
        }
    }
}

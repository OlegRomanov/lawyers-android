package ua.net.advokat24.aplication.services;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import ua.net.advokat24.MyApp;
import ua.net.advokat24.aplication.helpactivitys.TaskConfirmActivity;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.util.Fields;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.MassegFromService;

import org.greenrobot.eventbus.EventBus;

import ua.net.advokat24.util.models.MassegFromService;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    public MyFirebaseMessagingService() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(Fields.TAG, "MyFirebaseMessagingService.onMessageReceived ");
        String postId = remoteMessage.getData().get("extra");
        int action = Integer.parseInt(remoteMessage.getData().get("action"));
        EventBus.getDefault().post(new MassegFromService(action, postId));
    }
}

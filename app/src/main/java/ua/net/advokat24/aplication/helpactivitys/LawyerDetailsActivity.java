package ua.net.advokat24.aplication.helpactivitys;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksImpl;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksInteractor;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksListener;
import ua.net.advokat24.util.Fields;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.R;
import ua.net.advokat24.repository.firebase.categoryimpl.CategoryImpl;
import ua.net.advokat24.repository.firebase.categoryimpl.CategorysInteractor;
import ua.net.advokat24.repository.firebase.categoryimpl.OnCategorysListener;
import ua.net.advokat24.repository.firebase.userget.UserGetListener;
import ua.net.advokat24.repository.firebase.userget.UserGetImpl;
import ua.net.advokat24.repository.firebase.userget.UserGetInteractor;
import ua.net.advokat24.util.models.CategoryListModel;
import ua.net.advokat24.util.models.MTask;
import ua.net.advokat24.util.models.usermodels.MUser;
import ua.net.advokat24.util.utils.ImageUtils;
import ua.net.advokat24.util.utils.IntentUtils;
import ua.net.advokat24.util.utils.UserStatusUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.net.advokat24.util.Function;

public class LawyerDetailsActivity extends AppCompatActivity {

    public static final String KEY = "MyUserLawyer";

    @BindView(R.id.ll_app_bar_user_info)
    LinearLayout ll_app_bar_user_info;
    @BindView(R.id.iv_toolbar_backgraond)
    ImageView iv_toolbar_backgraond;
    @BindView(R.id.app_bar)
    AppBarLayout app_bar;
    @BindView(R.id.tv_app_bar_user_name)
    TextView tv_app_bar_user_name;
    @BindView(R.id.tv_app_bar_user_discription)
    TextView tv_app_bar_user_discription;
    @BindView(R.id.iv_app_bar_user_photo)
    ImageView iv_app_bar_user_photo;
    @BindView(R.id.tv_field_name)
    TextView tv_field_name;
    @BindView(R.id.tv_field_company)
    TextView tv_field_company;
    @BindView(R.id.tv_field_position)
    TextView tv_field_position;
    @BindView(R.id.tv_field_work_time)
    TextView tv_field_work_time;
    @BindView(R.id.tv_city)
    TextView tv_city;
    @BindView(R.id.tv_skils)
    TextView tv_skils;
    @BindView(R.id.tv_status)
    TextView tv_status;

    @BindView(R.id.tv_reiting_app_bar)
    TextView tv_reiting_app_bar;
    @BindView(R.id.tv_location_app_bar)
    TextView tv_location_app_bar;
    @BindView(R.id.tv_lawyer_tasks)
    TextView tv_lawyer_tasks;

    private MUser user;
    private boolean isYouClient = false;

    @OnClick(R.id.fl_call)
    void fl_call() {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if(firebaseUser != null){
            if (isYouClient) {
                IntentUtils.call(this, user.getPhone());
            } else {
                Function.showToast(this.getResources().getString(R.string.error_you_can_not_call_to_lawyer));
            }
        } else {
            Function.showToast("Зарегестрируйтесь в системе");
        }
    }

    @OnClick(R.id.iv_toolbar_back)
    void iv_toolbar_back() {
        this.finish();
    }

//    @OnClick(R.id.ll_lawyer_tasks)
//    void ll_lawyer_tasks() {
//        Intent activity = new Intent(this, TasksActivity.class);
//        activity.putExtra(TasksActivity.KEY, user.getId());
//        activity.putExtra(TasksActivity.KEY_IS_KIENT, false);
//        startActivity(activity);
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_lawyers_details);
        ButterKnife.bind(this);
        String id = getIntent().getStringExtra(KEY);

        app_bar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                int maxScroll = appBarLayout.getTotalScrollRange() / 2;
                float percentage = ((float) Math.abs(verticalOffset) / ((float) maxScroll / 2)) - 1.5f;
                iv_toolbar_backgraond.setAlpha(percentage);
                ll_app_bar_user_info.setAlpha(percentage * -1);
            }
        });

        UserGetInteractor userGetInteractor = new UserGetImpl();
        userGetInteractor.getUser(id, new UserGetListener() {
            @Override
            public void user(final MUser mUser) {
                user = mUser;
                if (!TextUtils.isEmpty(mUser.getName()))
                    tv_app_bar_user_name.setText(mUser.getName());
                if (!TextUtils.isEmpty(mUser.getName())) tv_field_name.setText(mUser.getName());
                loadPhoto(mUser);
                if (!TextUtils.isEmpty(mUser.getCity())) tv_city.setText(mUser.getCity());
                if (!TextUtils.isEmpty(mUser.getCity()))
                    tv_location_app_bar.setText(mUser.getCity());
                if (!TextUtils.isEmpty(mUser.getCompany()))
                    tv_field_company.setText(mUser.getCompany());
                if (!TextUtils.isEmpty(mUser.getPosition()))
                    tv_field_position.setText(mUser.getPosition());
                if (mUser.getReiting() != 0 && mUser.getReitingCount() != 0) {
                    tv_reiting_app_bar.setText(String.valueOf(mUser.getReiting() / mUser.getReitingCount()) + "/10");
                }

                tv_field_work_time.setText(mUser.getWorkTime().toStringMy(LawyerDetailsActivity.this));

                UserStatusUtils.setStatus(mUser.getStatus(), tv_status);
                final CategorysInteractor categorys = new CategoryImpl();
                categorys.getList(new OnCategorysListener() {
                    @Override
                    public void resultList(List<CategoryListModel> listcategorys) {
                        tv_skils.setText(Function.getStringSkilsMultiple(mUser.getSkils(), listcategorys));
//                        tv_app_bar_user_discription.setText(Function.getStringProfileSkils(mUser.getSkils(), listcategorys));
                    }

                    @Override
                    public void error() {
                        Log.d(Fields.TAG, getClass().getSimpleName() + "initUser saccess.");
                    }
                });

                TasksInteractor tasksInteractor = new TasksImpl();
                tasksInteractor.getList(mUser.getId(), false, new TasksListener() {
                    @Override
                    public void resultList(List<MTask> list) {
                        tv_lawyer_tasks.setText(String.valueOf(list.size()));
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        if(user != null){
                            String qwery = user.getUid();
                            for(int i = 0; i < list.size(); i++){
                                if(list.get(i).getClientId() != null){
                                    if(list.get(i).getClientId().equals(qwery)){
                                        isYouClient = true;
                                        break;
                                    }
                                }

                            }
                        }
                    }

                    @Override
                    public void error() {

                    }
                });
            }

            @Override
            public void error(String error) {
                Function.eLog(error, getClass().getName());
            }
        });
    }

    private void loadPhoto(MUser mUser) {
        ImageUtils.loadPhoto(this, mUser, iv_app_bar_user_photo);
//        if (mUser.getPhoto() != null) {
//            iv_app_bar_user_photo.setImageDrawable(null);
//            GlideApp.with(this)
//                    .load(mUser.getPhoto())
//                    .centerCrop()
//                    .diskCacheStrategy(DiskCacheStrategy.NONE)
//                    .skipMemoryCache(true)
//                    .transform(new MultiTransformation<Bitmap>(new CircleCrop()))
//                    .into(iv_app_bar_user_photo);
//
//        } else {
//            GlideApp.with(this)
//                    .load(FirebaseStorage.getInstance().getReference().child("images/userphoto/" + mUser.getId() + ".jpg"))
//                    .centerCrop()
//                    .diskCacheStrategy(DiskCacheStrategy.NONE)
//                    .skipMemoryCache(true)
//                    .transform(new MultiTransformation<Bitmap>(new CircleCrop()))
//                    .error(R.drawable.ic_user_check_off)
//                    .into(iv_app_bar_user_photo);
//        }
    }
}

package ua.net.advokat24.aplication.dialigs;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import ua.net.advokat24.MyApp;
import ua.net.advokat24.R;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConfirmDialog extends DialogFragment {

    private String head = MyApp.getContext().getResources().getString(R.string.input_data);
    private String body = MyApp.getContext().getResources().getString(R.string.for_continue_input_text);
    private String positive = MyApp.getContext().getResources().getString(R.string.confirm);
    private String negative = MyApp.getContext().getResources().getString(R.string.cancel);

    private static final String HEAD = "head";
    private static final String BODY = "body";
    private static final String POSITIVE = "positive";
    private static final String NEGATIVE = "negative";

    public static ConfirmDialog newInstance() {
        return new ConfirmDialog();
    }

    public static ConfirmDialog newInstance(String head, String body, String positive, String negative) {
        ConfirmDialog fragment = new ConfirmDialog();
        Bundle bundle = new Bundle();
        bundle.putString(HEAD, head);
        bundle.putString(BODY, body);
        bundle.putString(POSITIVE, positive);
        bundle.putString(NEGATIVE, negative);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            head = getArguments().getString(HEAD, head);
            body = getArguments().getString(BODY, body);
            positive = getArguments().getString(POSITIVE, positive);
            negative = getArguments().getString(NEGATIVE, negative);
        }
    }

    @BindView(R.id.tv_positive) TextView tv_positive;
    @BindView(R.id.tv_negative) TextView tv_negative;
    @BindView(R.id.tv_head) TextView tv_head;
    @BindView(R.id.tv_body) TextView tv_body;

    @OnClick(R.id.fl_ok) void fl_ok() {
        if (event != null)
            event.confirm();
        dismiss();
    }

    @OnClick(R.id.fl_cancel) void fl_cancel() {
        if (event != null)
            event.fail();
        dismiss();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        Objects.requireNonNull(getDialog().getWindow()).setBackgroundDrawableResource(R.color.colorTrans);
        View v = inflater.inflate(R.layout.d_confirm, container, false);
        ButterKnife.bind(this, v);
        tv_positive.setText(positive);
        tv_negative.setText(negative);
        tv_head.setText(head);
        tv_body.setText(body);
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        Objects.requireNonNull(getDialog().getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        event.isDismis();
        super.onDismiss(dialog);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        Objects.requireNonNull(getDialog().getWindow()).getAttributes().windowAnimations = R.style.DialogAnimationNew;
    }

    public interface Event {
        void confirm();
        void fail();
        void isDismis();
    }

    private Event event;

    public void registerEvent(Event event) {
        this.event = event;
    }
}
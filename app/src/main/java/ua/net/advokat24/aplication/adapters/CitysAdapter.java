package ua.net.advokat24.aplication.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import ua.net.advokat24.R;
import ua.net.advokat24.aplication.dialigs.ChooseCityDialog;
import ua.net.advokat24.aplication.helpactivitys.TaskActivity;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.CategoryListModel;
import ua.net.advokat24.util.models.MTask;
import ua.net.advokat24.util.models.citys.City;
import ua.net.advokat24.util.utils.CitysUtils;

import java.util.ArrayList;
import java.util.List;

public class CitysAdapter extends RecyclerView.Adapter<CitysAdapter.ViewHolder> {

    private List<City> citys;
    private Context context;
    private LinearLayoutManager layoutManager;
    private String cityExist;

    private boolean onBind;

    public CitysAdapter(Context context, List<City> citys, LinearLayoutManager layoutManager, String cityExist) {
        this.context = context;
        this.cityExist = cityExist == null?"":cityExist;
        this.layoutManager = layoutManager;
        this.citys = new ArrayList<>();
    }

    public void initList(List<City> citys){
        this.citys.clear();
        this.citys = citys;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CitysAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.i_city, parent, false);
        return new CitysAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final CitysAdapter.ViewHolder holder , int position) {

        final City cityModel = citys.get(holder.getAdapterPosition());
        holder.tv_name.setText(cityModel.getName());
        onBind = true;
        holder.iv_status.setChecked(cityExist.equals(cityModel.getName()));
        holder.iv_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cityExist = cityModel.getName();
                notifyDataSetChanged();
            }
        });

        holder.fl_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cityExist = cityModel.getName();
                notifyDataSetChanged();
            }
        });
        onBind = false;
    }

    @Override
    public int getItemCount() {
        return citys.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name;
        private FrameLayout fl_click;
        private AppCompatCheckBox iv_status;
        ViewHolder(View itemView) {
            super(itemView);

            fl_click = itemView.findViewById(R.id.fl_click);
            iv_status = itemView.findViewById(R.id.iv_status);
            tv_name = itemView.findViewById(R.id.tv_name);
        }
    }

    public String getCityExist() {
        if (cityExist == null)
            return "";
        return cityExist;
    }
}

package ua.net.advokat24.aplication.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;

import android.provider.Settings;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.ImageView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.bumptech.glide.Glide;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import ua.net.advokat24.aplication.dialigs.PrivatePoliticDialog;
import ua.net.advokat24.aplication.helpactivitys.LawyerDetailsActivity;
import ua.net.advokat24.aplication.helpactivitys.TaskActivity;
import ua.net.advokat24.aplication.helpactivitys.TaskConfirmActivity;
import ua.net.advokat24.aplication.main.fragments.clientcreatetask.ClientCreateTaskFragment;
import ua.net.advokat24.aplication.main.fragments.clientpay.PayConfirmFragment;
import ua.net.advokat24.aplication.main.fragments.clientpay.PayPhoneNumberFragment;
import ua.net.advokat24.aplication.main.fragments.clientpay.PayPriseFragment;
import ua.net.advokat24.aplication.main.fragments.listfragment.clientlist.SearchResultFragment;
import ua.net.advokat24.aplication.services.ServiceNewTask;
import ua.net.advokat24.util.Fields;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.R;
import ua.net.advokat24.aplication.helpactivitys.LoginActivity;
import ua.net.advokat24.aplication.main.fragments.signin.LoginFragment;
import ua.net.advokat24.aplication.main.fragments.regclient.LoginRegClientFragment;
import ua.net.advokat24.aplication.main.fragments.reglawyer.LoginRegLawyerFragment;
import ua.net.advokat24.aplication.main.fragments.clienthome.ClientHomeFragment;
import ua.net.advokat24.aplication.main.fragments.clientinfo.ClientInfoFragment;
import ua.net.advokat24.aplication.main.fragments.clientfilter.ClientFilterFragment;
import ua.net.advokat24.aplication.main.fragments.lawyerhome.LawyerHomeFragment;
import ua.net.advokat24.aplication.main.fragments.listfragment.clientlist.ClientListFragment;
import ua.net.advokat24.aplication.main.fragments.listfragment.clientlist.ClientMapFragment;
import ua.net.advokat24.aplication.main.fragments.listfragment.lawyerlist.LawyerListFragment;
import ua.net.advokat24.aplication.main.fragments.lawyerinfo.LawyerInfoFragment;
import ua.net.advokat24.repository.SharedPreferencesManager;
import ua.net.advokat24.util.utils.FilesUtils;
import ua.net.advokat24.util.utils.UiUtils;
import ua.net.advokat24.util.fragmentAnimator.MFragmentManager;
import ua.net.advokat24.util.fragmentAnimator.NewFragment;
import ua.net.advokat24.util.fragmentAnimator.ReplaceToLeft;
import ua.net.advokat24.util.fragmentAnimator.ReplaceToRight;
import ua.net.advokat24.util.models.CategoryListModel;
import ua.net.advokat24.util.models.MTask;
import ua.net.advokat24.util.models.usermodels.MUser;
import ua.net.advokat24.util.models.MassegFromService;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.net.advokat24.aplication.main.fragments.clientcreatetask.ClientCreateTaskFragment;
import ua.net.advokat24.aplication.main.fragments.clienthome.ClientHomeFragment;
import ua.net.advokat24.aplication.main.fragments.clientinfo.ClientInfoFragment;
import ua.net.advokat24.aplication.main.fragments.lawyerinfo.LawyerInfoFragment;
import ua.net.advokat24.aplication.services.ServiceNewTask;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.MassegFromService;

public class MainActivity extends MvpAppCompatActivity implements MainActivityView,
        ClientHomeFragment.OnFragmentInteractionListener,
        LawyerInfoFragment.OnFragmentInteractionListener,
        ClientCreateTaskFragment.OnFragmentInteractionListener,
        PayPriseFragment.OnFragmentInteractionListener,
        PayConfirmFragment.OnFragmentInteractionListener,
        PayPhoneNumberFragment.OnFragmentInteractionListener,
        LawyerHomeFragment.OnFragmentInteractionListener,
        LoginFragment.OnFragmentInteractionListener,
        ClientInfoFragment.OnFragmentInteractionListener,
        LoginRegLawyerFragment.OnFragmentInteractionListener,
        ClientFilterFragment.OnFragmentInteractionListener,
        ClientListFragment.OnFragmentInteractionListener,
        LawyerListFragment.OnFragmentInteractionListener,
        LoginRegClientFragment.OnFragmentInteractionListener {


    @InjectPresenter
    MainActivityPresenter presenter;

    @BindView(R.id.iv_home)
    ImageView iv_home;
    @BindView(R.id.iv_list)
    ImageView iv_list;
    @BindView(R.id.iv_filter)
    ImageView iv_filter;
    @BindView(R.id.iv_profile)
    ImageView iv_profile;

    private boolean isDocumentPhoto = false;
    private boolean isClientsPhoto = false;
    private int action = MassegFromService.ACTION_LAWYERS_DEFOULT;
    private String actionSubjectId = "";

    private ProgressDialog dialogWait;
    public String getActionSubjectId() {
        return actionSubjectId;
    }
    private int mCurentFragment;

    private ClientInfoFragment clientInfoFragment;
    private LawyerInfoFragment lawyerInfoFragment;
    private ClientListFragment clientListFragment;
    private ClientHomeFragment clientHomeFragment;
    private LawyerHomeFragment lawyerHomeFragment;
    private LawyerListFragment lawyerListFragment;
    private SearchResultFragment searchResultFragment;
    private ClientCreateTaskFragment clientCreateTaskFragment;
    private PayPriseFragment payPriseFragment;
    private PayConfirmFragment payConfirmFragment;
    private PayPhoneNumberFragment payPhoneNumberFragment;
    private ClientMapFragment clientMapFragment;
    private LoginRegClientFragment loginRegClientFragment;
    private LoginRegLawyerFragment loginRegLawyerFragment;
    private ClientFilterFragment clientFilterFragment;
    private LoginFragment loginFragment;
    private MFragmentManager mFragmentManager = new MFragmentManager();
    private CallbackManager callbackManager;
    private LoginButton facebook_login;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.a_main);
        ButterKnife.bind(this);
        mAuth = FirebaseAuth.getInstance();
        action = getIntent().getIntExtra(ServiceNewTask.KEY_ACTIONS, 0);
        actionSubjectId = getIntent().getStringExtra(ServiceNewTask.KEY_POST_ID);

        clientHomeFragment = ClientHomeFragment.newInstance();
        clientListFragment = ClientListFragment.newInstance();
        clientInfoFragment = ClientInfoFragment.newInstance();
        lawyerHomeFragment = LawyerHomeFragment.newInstance();
        lawyerListFragment = LawyerListFragment.newInstance();
        lawyerInfoFragment = LawyerInfoFragment.newInstance();
        searchResultFragment = SearchResultFragment.newInstance();
        clientCreateTaskFragment = ClientCreateTaskFragment.newInstance();
        payPriseFragment = PayPriseFragment.newInstance();
        payConfirmFragment = PayConfirmFragment.newInstance();
        payPhoneNumberFragment = PayPhoneNumberFragment.newInstance();
        clientMapFragment = ClientMapFragment.newInstance();
        loginRegClientFragment = LoginRegClientFragment.newInstance();
        loginRegLawyerFragment = LoginRegLawyerFragment.newInstance();
        clientFilterFragment = ClientFilterFragment.newInstance();
        loginFragment = LoginFragment.newInstance();
        dialogWait = ProgressDialog.show(MainActivity.this,
                getResources().getString(R.string.loading),
                getResources().getString(R.string.loading_disc),
                true, false);
    }


    /*Sheat !!! TODO rewrite*/

    @Override
    public void unregisterStart() {
        dialogWait.dismiss();
        mFragmentManager.setAction(new NewFragment());
        if(presenter.isClient()){
            mCurentFragment = UiUtils.HOME_FRAGMENT;
            mFragmentManager.doTransaction(getSupportFragmentManager(), clientHomeFragment, R.id.fl_conteiner_fragments);
        } else {
            mCurentFragment = UiUtils.USER_FRAGMENT;
            mFragmentManager.doTransaction(getSupportFragmentManager(), loginFragment, R.id.fl_conteiner_fragments);
        }

        UiUtils.setBottomBarStatus(mCurentFragment, iv_home, iv_list, iv_filter, iv_profile);
    }

    @Override
    public void registerStart() {
        dialogWait.dismiss();
        mFragmentManager.setAction(new NewFragment());
        if(action == MassegFromService.ACTION_LAWYERS_DEFOULT || action == MassegFromService.ACTION_LAWYERS_NOT_FOUND){
            if(presenter.isClient()){
                mFragmentManager.doTransaction(getSupportFragmentManager(), clientHomeFragment, R.id.fl_conteiner_fragments);
            } else {
                mFragmentManager.doTransaction(getSupportFragmentManager(), lawyerHomeFragment, R.id.fl_conteiner_fragments);
                checkOnPrivatPolitic();
            }
            mCurentFragment = UiUtils.HOME_FRAGMENT;
            UiUtils.setBottomBarStatus(mCurentFragment, iv_home, iv_list, iv_filter, iv_profile);
        } else if(action == MassegFromService.ACTION_LAWYERS_IS_FOUND) {
            mCurentFragment = UiUtils.HOME_FRAGMENT;
            mFragmentManager.doTransaction(getSupportFragmentManager(), payPriseFragment, R.id.fl_conteiner_fragments);
        }


    }

    @Override
    public void backToHome() {
        mFragmentManager.setAction(new ReplaceToRight());
        if(presenter.isClient()){
            mFragmentManager.doTransaction(getSupportFragmentManager(), clientHomeFragment, R.id.fl_conteiner_fragments);
        } else {
            mFragmentManager.doTransaction(getSupportFragmentManager(), lawyerHomeFragment, R.id.fl_conteiner_fragments);
        }
        mCurentFragment = UiUtils.HOME_FRAGMENT;
        UiUtils.setBottomBarStatus(mCurentFragment, iv_home, iv_list, iv_filter, iv_profile);

    }

    @Override
    public void searchResult() {
        if(dialogWait != null){
            dialogWait.dismiss();
        }
        navigationClick(UiUtils.LIST_FRAGMENT);
    }

    @Override
    public void goToLogin() {
        mFragmentManager.setAction(new ReplaceToRight());
        mFragmentManager.doTransaction(getSupportFragmentManager(), loginFragment, R.id.fl_conteiner_fragments);
    }

    @Override
    public void goToRegistration() {
        mFragmentManager.setAction(new ReplaceToLeft());
        if(presenter.isClient()){
            mFragmentManager.doTransaction(getSupportFragmentManager(), loginRegClientFragment, R.id.fl_conteiner_fragments);
        } else {
            mFragmentManager.doTransaction(getSupportFragmentManager(), loginRegLawyerFragment, R.id.fl_conteiner_fragments);
        }
    }

    @Override
    public void initSaccess() {
        Function.hideKeyboard(this);
        mCurentFragment = UiUtils.HOME_FRAGMENT;
        UiUtils.setBottomBarStatus(mCurentFragment, iv_home, iv_list, iv_filter, iv_profile);
        if(action == MassegFromService.ACTION_LAWYERS_DEFOULT || action == MassegFromService.ACTION_LAWYERS_NOT_FOUND){
            mFragmentManager.setAction(new ReplaceToLeft());
            if(presenter.isClient()){
                mFragmentManager.doTransaction(getSupportFragmentManager(), clientHomeFragment, R.id.fl_conteiner_fragments);
            } else {
                checkOnPrivatPolitic();
                mFragmentManager.doTransaction(getSupportFragmentManager(), lawyerHomeFragment, R.id.fl_conteiner_fragments);
            }
        } else if(action == MassegFromService.ACTION_LAWYERS_IS_FOUND) {
            Intent activity1 = new Intent(MainActivity.this, LawyerDetailsActivity.class);
            activity1.putExtra(LawyerDetailsActivity.KEY, actionSubjectId);
            startActivity(activity1);
            Function.showToast("Это ваш адвокат, вы можете с ним связаться");
        }
        if(dialogWait != null){
            dialogWait.dismiss();
        }
    }

    @Override
    public void initError(String error) {

    }

    @Override
    public void searchLawyers(String category, String city) {
        presenter.searchUsers(category, city);
        dialogWait = ProgressDialog.show(MainActivity.this,
                "Поиск",
                getResources().getString(R.string.loading_disc),
                true, false);

    }

    private void checkOnPrivatPolitic(){
        if(!presenter.getmUser().isConfirmStartMessage()){
            PrivatePoliticDialog dialog = PrivatePoliticDialog.newInstance();
            dialog.registerEvent(new PrivatePoliticDialog.Event() {
                @Override
                public void confirm() {
                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference(Fields.USERS_TABTE);
                    mDatabase.child(presenter.getmUser().getId()).child("confirmStartMessage").setValue(true);
                    presenter.initUser();
                }

                @Override
                public void fail() {
                    MainActivity.this.finish();
                }

                @Override
                public void isDismis() { }
            });
            dialog.show(Objects.requireNonNull(MainActivity.this).getSupportFragmentManager(), dialog.getClass().getSimpleName());
        }
    }


    /*ButtomMenu*/

    @Override
    public void autoClick(int page) {
        navigationClick(page);
    }

    public void homeClick(View view) {
        navigationClick(UiUtils.HOME_FRAGMENT);
    }

    public void listClick(View view) {
        navigationClick(UiUtils.LIST_FRAGMENT);
    }

    public void filterClick(View view) {
        navigationClick(UiUtils.FILTER_FRAGMENT);
    }

    public void profileClick(View view) {
        navigationClick(UiUtils.USER_FRAGMENT);
    }

    public void navigationClick(int clickId){
        switch (clickId){
            case UiUtils.HOME_FRAGMENT:
                mFragmentManager.setAction(new ReplaceToRight());
                if(presenter.isClient()){
                    mFragmentManager.doTransaction(getSupportFragmentManager(), clientHomeFragment, R.id.fl_conteiner_fragments);
                } else {
                    if(presenter.isRegister()){
                        mFragmentManager.doTransaction(getSupportFragmentManager(), lawyerHomeFragment, R.id.fl_conteiner_fragments);
                    } else {
                        mFragmentManager.doTransaction(getSupportFragmentManager(), loginFragment, R.id.fl_conteiner_fragments);
                    }
                }
                break;
            case UiUtils.LIST_FRAGMENT:
                if(mCurentFragment == UiUtils.HOME_FRAGMENT){
                    mFragmentManager.setAction(new ReplaceToLeft());
                } else {
                    mFragmentManager.setAction(new ReplaceToRight());
                }
                if(presenter.isClient()){
                    if(presenter.isRegister()){
                        mFragmentManager.doTransaction(getSupportFragmentManager(), searchResultFragment, R.id.fl_conteiner_fragments);
                    } else {
                        mFragmentManager.doTransaction(getSupportFragmentManager(), loginFragment, R.id.fl_conteiner_fragments);
                    }
                } else {
                    if(presenter.isRegister()){
                        mFragmentManager.doTransaction(getSupportFragmentManager(), lawyerListFragment, R.id.fl_conteiner_fragments);
                    } else {
                        mFragmentManager.doTransaction(getSupportFragmentManager(), loginFragment, R.id.fl_conteiner_fragments);
                    }
                }

                break;
            case UiUtils.FILTER_FRAGMENT:
                if(mCurentFragment == UiUtils.USER_FRAGMENT){
                    mFragmentManager.setAction(new ReplaceToRight());
                } else {
                    mFragmentManager.setAction(new ReplaceToLeft());
                }
                if(presenter.isClient()){
                    if(presenter.isRegister()){
                        mFragmentManager.doTransaction(getSupportFragmentManager(), clientFilterFragment, R.id.fl_conteiner_fragments);
                    } else {

                        mFragmentManager.doTransaction(getSupportFragmentManager(), loginFragment, R.id.fl_conteiner_fragments);
                    }
                } else {
                    if(presenter.isRegister()){
                        mFragmentManager.doTransaction(getSupportFragmentManager(), clientFilterFragment, R.id.fl_conteiner_fragments);
                    } else {
                        mFragmentManager.doTransaction(getSupportFragmentManager(), loginFragment, R.id.fl_conteiner_fragments);
                    }
                }
                break;
            case UiUtils.USER_FRAGMENT:
                mFragmentManager.setAction(new ReplaceToLeft());
                if(presenter.isClient()){
                    if(presenter.isRegister()){
                        mFragmentManager.doTransaction(getSupportFragmentManager(), clientInfoFragment, R.id.fl_conteiner_fragments);
                    } else {
                        mFragmentManager.doTransaction(getSupportFragmentManager(), loginFragment, R.id.fl_conteiner_fragments);
                    }
                } else {
                    if(presenter.isRegister()){
                        mFragmentManager.doTransaction(getSupportFragmentManager(), lawyerInfoFragment, R.id.fl_conteiner_fragments);
                    } else {
                        mFragmentManager.doTransaction(getSupportFragmentManager(), loginFragment, R.id.fl_conteiner_fragments);
                    }
                }
                break;
            default:
                break;
        }
        mCurentFragment = clickId;
        UiUtils.setBottomBarStatus(mCurentFragment, iv_home, iv_list, iv_filter, iv_profile);
    }


    /*Lifecicle methods*/

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MassegFromService event){
        actionSubjectId = event.getPostId();
        switch (event.getAction()){
            case MassegFromService.ACTION_LAWYERS_DEFOULT:
                mFragmentManager.setAction(new ReplaceToRight());
                mFragmentManager.doTransaction(getSupportFragmentManager(), clientHomeFragment, R.id.fl_conteiner_fragments);
                break;
            case MassegFromService.ACTION_LAWYERS_NOT_FOUND:
                if (clientHomeFragment.isVisible()){
                    clientHomeFragment.chengeStatus();
                } else {
                    mFragmentManager.setAction(new ReplaceToLeft());
                    mFragmentManager.doTransaction(getSupportFragmentManager(), clientHomeFragment, R.id.fl_conteiner_fragments);
                }
                Function.showToast("Адвоката не найдено");
                break;
            case MassegFromService.ACTION_LAWYERS_IS_FOUND:
                Intent activity1 = new Intent(MainActivity.this, LawyerDetailsActivity.class);
                activity1.putExtra(LawyerDetailsActivity.KEY, event.getPostId());
                startActivity(activity1);
                Function.showToast("Это ваш адвокат вы можете с ним связаться");
                break;
            case MassegFromService.ACTION_FOUND_NEW_TASK:
                Intent activity = new Intent(MainActivity.this, TaskConfirmActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("extra", event.getPostId());
                activity.putExtras(bundle);
                startActivity(activity);
                break;
            case MassegFromService.ACTION_CLIENT_PAY_FOR_TASK:
                Function.showToast("Клиент оплатил дело и указал номер телефона, свяжитесь с ним");
                Intent activityTAsk = new Intent(MainActivity.this, TaskActivity.class);
                Bundle bundletask = new Bundle();
                bundletask.putString("extra", event.getPostId());
                activityTAsk.putExtras(bundletask);
                startActivity(activityTAsk);
                break;
            default:
                break;
        }
        if(dialogWait != null){
            dialogWait.dismiss();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);

    }

    @Override
    public void onBackPressed() {
        if(!presenter.isRegister()){
            logOut();
        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        if(!presenter.isRegister()){
            FirebaseAuth.getInstance().signOut();
            LoginManager.getInstance().logOut();
            SharedPreferencesManager.getInstance(this).setIsRegister(false);
            SharedPreferencesManager.getInstance(this).setClient(false);
        }
        super.onDestroy();
    }


    /*Registration and sign-in*/

    @Override
    public void facebookUserNotExist() {
        if(dialogWait != null){
            dialogWait.dismiss();
        }
        if (loginRegClientFragment != null && loginRegClientFragment.isVisible()){
            loginRegClientFragment.preinitUser();
        } else if (loginRegLawyerFragment != null && loginRegLawyerFragment.isVisible()){
            loginRegLawyerFragment.preinitUser();
        } else if (loginFragment != null && loginFragment.isVisible()){
            goToRegistration();
        }
    }

    @Override
    public void registration(final String passwd, final MUser mUser, final Uri userPhoto, final Uri documentsPhoto) {
        dialogWait = Function.startProgress(MainActivity.this);
        mAuth.createUserWithEmailAndPassword(mUser.getEmail(), passwd).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()) {
                    presenter.registration(mUser, userPhoto, documentsPhoto);
                } else {
                    dialogWait.dismiss();
//                    Function.showToast(MainActivity.this.getResources().getString(R.string.error_register));
                    Function.showToast("Пользователь с такой почтой уже существует");
                }
            }
        });
    }

    @Override
    public void signin(String email , String password) {
        dialogWait = ProgressDialog.show(MainActivity.this, "", MainActivity.this.getResources().getString(R.string.loading), false, false);
        mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()) {
                    presenter.singin();
                }else{
                    dialogWait.dismiss();
                    Function.showToast(MainActivity.this.getResources().getString(R.string.error_signin));
                }
            }
        });
    }

    @Override
    public void finishRegistrationInFragment() {
        dialogWait = ProgressDialog.show(MainActivity.this,
                getResources().getString(R.string.loading),
                getResources().getString(R.string.loading_disc),
                true, false);
        presenter.resetStatus();
        presenter.startMain();
    }

    @Override
    public void facebookButton(LoginButton facebook_login1) {
        facebook_login = facebook_login1;
        facebook_login.setReadPermissions("email");
        facebook_login.setAlpha(0.0f);
        facebook_login.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                dialogWait = ProgressDialog.show(MainActivity.this, "", MainActivity.this.getResources().getString(R.string.loading), false, false);
                AuthCredential credential = FacebookAuthProvider.getCredential(loginResult.getAccessToken().getToken());
                mAuth.signInWithCredential(credential)
                        .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    presenter.chackFacebookUser();
                                } else {
                                    Function.showToast("Ошибка при регистрации Через Facebook. Обратитесь в тех поддержку");
                                    Function.eLog(task.getException().getMessage(), getClass().getName());
                                    dialogWait.dismiss();
                                }
                            }
                        });
            }

            @Override
            public void onCancel() { Function.showToast( "Регистрация отменена"); }

            @Override
            public void onError(FacebookException exception) {
                Function.showToast("Ошибка при регистрации");
                Function.eLog(exception.getMessage(), getClass().getName() + "initFacebook");
            }
        });
    }

    @Override
    public void errorRegistration(String error) {
        if(dialogWait != null){
            dialogWait.dismiss();
        }
    }

    @Override
    public void logOut() {
        FirebaseAuth.getInstance().signOut();
        LoginManager.getInstance().logOut();
        SharedPreferencesManager.getInstance(this).setIsRegister(false);
        SharedPreferencesManager.getInstance(this).setClient(false);

        if(!presenter.isRegister()){
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        } else {
            MainActivity.this.finishAndRemoveTask();
        }

    }


    /*Presenter Values*/

    public MUser getmUser() {
        return presenter.getmUser();
    }

    public List<CategoryListModel> getListCategory() {
        return presenter.getListCategory();
    }

    public List<MUser> getListUsers() {
        return presenter.getListUsers();
    }

    public List<MTask> getListMyTasks() {
        return presenter.getListMyTasks();
    }

    public String getSearchCity() {
        return presenter.getSearchCity();
    }

    public String getSearckCategory() {
        return presenter.getSearckCategory();
    }

    public void saveUser(){
        presenter.saveUser();
    }


    /*Location*/

    public static final int LOCATION_PERMISSION_USER = 1;
    public static final int LOCATION_PERMISSION_TASK = 2;
    private int curentLocationObject = 0;


    /*Permission on GPS*/

    public void startPermissionDialog(int curentLocationObject) {
        this.curentLocationObject = curentLocationObject;
        ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,}, 1);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    switch (curentLocationObject){
                        case LOCATION_PERMISSION_USER:
                            if (lawyerInfoFragment != null && lawyerInfoFragment.isVisible())
                                lawyerInfoFragment.setUserLocation();
                            break;
                        case LOCATION_PERMISSION_TASK:
                            if (clientHomeFragment != null && clientHomeFragment.isVisible())
                                clientHomeFragment.setUserLocation();
                            break;
                    }
                } else {
                    Function.showToast(getResources().getString(R.string.open_permission_activity));
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                }
            }
        }
    }


    /*Avtocompile adress*/

    private final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 5;
    public void startAutocompile (int curentLocationObject){
        this.curentLocationObject = curentLocationObject;
        try {
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            Function.eLog(e.getMessage(), getClass().getSimpleName() + ".startAutocompile");
            Function.showToast(e.getMessage());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri resultUri = result.getUri();
                    Uri myuri = FilesUtils.convertUri(resultUri);
                    drow(myuri);
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    Exception error = result.getError();
                    Function.eLog(error.getMessage(), getClass().getName() + ".onActivityResult");
                }
                break;
            case PLACE_AUTOCOMPLETE_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    switch (curentLocationObject){
                        case LOCATION_PERMISSION_USER:
                            if (lawyerInfoFragment != null && lawyerInfoFragment.isVisible())
                                lawyerInfoFragment.setAdress(PlaceAutocomplete.getPlace(this, data));
                            break;
                        case LOCATION_PERMISSION_TASK:
                            break;
                    }
                } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                    Status status = PlaceAutocomplete.getStatus(this, data);
                    Function.showToast(status.getStatusMessage());
                    Function.eLog(status.getStatusMessage(), getClass().getName() + ".onActivityResult");
                } else if (resultCode == RESULT_CANCELED) {
                    Function.eLog("resultCode == RESULT_CANCELED", getClass().getName() + ".onActivityResult");
                }
                break;
            default:
                break;
        }
    }


    /*Work with photo*/

    @Override
    public void finishUpdateUserPhoto() {
        dialogWait.dismiss();
        Glide.get(this).clearMemory();
        if (clientInfoFragment != null && clientInfoFragment.isVisible()){
            clientInfoFragment.updatePhoto();
        } else if (lawyerInfoFragment != null && lawyerInfoFragment.isVisible()){
            lawyerInfoFragment.updatePhoto();
        }
    }

    @Override
    public void putPhoto(boolean isClient, boolean isDocuments) {
        isDocumentPhoto = isDocuments;
        isClientsPhoto = isClient;
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }

    private void drow(Uri uri){
        if (loginRegClientFragment != null && loginRegClientFragment.isVisible()){
            loginRegClientFragment.setPhotoUser(uri);
        } else if (loginRegLawyerFragment != null && loginRegLawyerFragment.isVisible()){
            if (isDocumentPhoto){
                loginRegLawyerFragment.setPhotoDocuments(uri);
            } else {
                loginRegLawyerFragment.setPhotoUser(uri);
            }
        } else if ((clientInfoFragment != null && clientInfoFragment.isVisible()) || (lawyerInfoFragment != null && lawyerInfoFragment.isVisible())){
            dialogWait = ProgressDialog.show(MainActivity.this, "", MainActivity.this.getResources().getString(R.string.loading), false, false);
            presenter.getmUser().setPhoto(null);
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference(Fields.USERS_TABTE);
            mDatabase.child(presenter.getmUser().getId()).child("photo").setValue(null);
            presenter.apdateUserPhoto(uri);
            Glide.get(this).clearMemory();
        }
    }

    /*Create Task*/

    @Override
    public void payGoToPhoneNamber(MTask mTask) {
        payPhoneNumberFragment.setTask(mTask);
        mFragmentManager.setAction(new ReplaceToLeft());
        mFragmentManager.doTransaction(getSupportFragmentManager(), payPhoneNumberFragment, R.id.fl_conteiner_fragments);
    }

    @Override
    public void createTask(Location location) {
        clientCreateTaskFragment.setTaskLocation(location);
        mFragmentManager.setAction(new ReplaceToLeft());
        mFragmentManager.doTransaction(getSupportFragmentManager(), clientCreateTaskFragment, R.id.fl_conteiner_fragments);
    }

    @Override
    public void createTask(MTask mTask) {

        if(!ServiceNewTask.serviceIsRun){
            if(presenter.getmUser() != null){
                mTask.setClientId(presenter.getmUser().getId());
                mTask.setClientName(presenter.getmUser().getName());
                if(presenter.getmUser().getPhoto() != null){
                    mTask.setClientPhoto(presenter.getmUser().getPhoto());
                }
            }
            Intent intent = new Intent(this, ServiceNewTask.class);
            intent.putExtra(ServiceNewTask.KEY, mTask);
            startService(intent);
            dialogWait = ProgressDialog.show(MainActivity.this,
                    getResources().getString(R.string.loading),
                    getResources().getString(R.string.loading_disc),
                    true, false);
        }

    }

//    @Override
//    public void finishTaskCreating(final MTask mTask, String phonenumber) {
//        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference(Fields.TASKS_TABLE);
//        mDatabase.child(getActionSubjectId()).child("clientPhone").setValue(phonenumber);
//        mFragmentManager.setAction(new ReplaceToRight());
//        mFragmentManager.doTransaction(getSupportFragmentManager(), clientHomeFragment, R.id.fl_conteiner_fragments);
//        mCurentFragment = UiUtils.HOME_FRAGMENT;
//        UiUtils.setBottomBarStatus(mCurentFragment, iv_home, iv_list, iv_filter, iv_profile);
//        if(mTask != null && mTask.getLawyerId() != null){
//            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
//            if(user != null){
//
//                UserGetInteractor interactor = new UserGetImpl();
//                interactor.getUser(mTask.getLawyerId(), new UserGetListener() {
//                    @Override
//                    public void error(String error) {}
//
//                    @Override
//                    public void user(MUser mUser) {
//                        MessageSendInteractor messageInteractor = new MessageSendImpl();
//                        messageInteractor.sendNotifycation(new Gson().toJson(new Message(mTask.getId(), mUser.getNotificationKay(), MassegFromService.ACTION_CLIENT_PAY_FOR_TASK)), null);
//                        final Handler myHandler = new Handler(); // автоматически привязывается к текущему потоку.
//                        Thread myThread = new Thread(new Runnable() {
//                            @Override
//                            public void run() {
//                                try {
//                                    Thread.sleep(900);
//                                } catch (InterruptedException e) {
//                                    e.printStackTrace();
//                                }
//
//                                myHandler.post(new Runnable() {  // используя Handler, привязанный к UI-Thread
//                                    @Override
//                                    public void run() {
//                                        Intent activity = new Intent(MainActivity.this, LawyerDetailsActivity.class);
//                                        activity.putExtra(LawyerDetailsActivity.KEY, mTask.getLawyerId());
//                                        startActivity(activity);
//                                        Function.showToast("Это ваш адвокат, вы можете с ним связаться");
//                                    }
//                                });
//                            }
//                        });
//                        myThread.start();
//                    }
//                });
//            } else {
//                Function.showToast("Наш адвокат с вами свяжетса");
//            }
//        }
//    }
}
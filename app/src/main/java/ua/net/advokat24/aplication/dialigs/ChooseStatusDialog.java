package ua.net.advokat24.aplication.dialigs;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatDelegate;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import ua.net.advokat24.R;
import ua.net.advokat24.util.models.BaseModel;
import com.shawnlin.numberpicker.NumberPicker;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChooseStatusDialog extends DialogFragment {

   private static final String KEY = "KEY";

    @BindView(R.id.number_picker)NumberPicker number_picker;

    @OnClick(R.id.tv_ok) void tv_ok() {
        event.result(list.get(number_picker.getValue() - 1));
        dismiss();
    }

    @OnClick(R.id.tv_cancel) void tv_cancel() {
        dismiss();
    }

    @OnClick(R.id.fl_out_click) void fl_out_click() {
        dismiss();
    }


    private List<BaseModel> list;
    private String[] listNames;

    public static ChooseStatusDialog newInstance(String gson) {
        ChooseStatusDialog fragment = new ChooseStatusDialog();
        Bundle bundle = new Bundle();
        bundle.putString(KEY, gson);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            String dlist = getArguments().getString(KEY);
            list = new Gson().fromJson(dlist, new TypeToken<List<BaseModel>>() {}.getType());
            listNames = new String[list.size()];
            for (int i = 0; i < list.size(); i++){
               listNames[i] = list.get(i).getName();
            }
        }
    }

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.backgraund_dialogs);
        View v = inflater.inflate(R.layout.d_choose_base_list, container, false);
        ButterKnife.bind(this, v);

        number_picker.setMinValue(1);
        number_picker.setMaxValue(listNames.length);
        number_picker.setDisplayedValues(listNames);
        number_picker.setValue(2);

        return v;
    }


    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }
    @Override
    public void onDismiss(DialogInterface dialog) {
        event.dismiss();
        super.onDismiss(dialog);
    }
    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimationNew;
    }

    public interface Event {
        void result(BaseModel baseModel);
        void dismiss();
    }
    private Event event;
    public void registerEvent(Event event){
        this.event = event;
    }
}
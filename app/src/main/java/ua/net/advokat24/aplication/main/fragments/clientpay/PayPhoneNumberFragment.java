package ua.net.advokat24.aplication.main.fragments.clientpay;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import ua.net.advokat24.R;
import ua.net.advokat24.aplication.helpactivitys.LawyerDetailsActivity;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.util.Fields;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.MyTextInputLayout;
import ua.net.advokat24.util.models.MTask;
import ua.net.advokat24.util.utils.PfoneUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.net.advokat24.util.Function;

public class PayPhoneNumberFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private MTask mTask;
    public PayPhoneNumberFragment() { }

    @BindView(R.id.ll_app_bar_user_info) LinearLayout ll_app_bar_user_info;
    @BindView(R.id.iv_toolbar_backgraond) ImageView iv_toolbar_backgraond;
    @BindView(R.id.app_bar) AppBarLayout app_bar;
    @BindView(R.id.til_phone) MyTextInputLayout til_phone;
    @BindView(R.id.et_phone) TextInputEditText et_phone;
    @OnClick(R.id.tv_btn_action) void tv_btn_action() {
        if(et_phone.getText().toString().length() != 0){
            if (PfoneUtils.validCellPhone(et_phone.getText().toString())){
                if (mListener != null) {
                    mTask.setClientPhone(et_phone.getText().toString());
                    mListener.createTask(mTask);
                }
            } else {
                Function.showToast(getActivity().getResources().getString(R.string.invalid_phone));
                til_phone.setError(" ");
            }
        } else {
            Function.showToast("Введите номер телефона");
            til_phone.setError(" ");
        }
    }

    public static PayPhoneNumberFragment newInstance() {
        return new PayPhoneNumberFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.f_pay_phone, container, false);
        ButterKnife.bind(this, v);
        app_bar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                int maxScroll = appBarLayout.getTotalScrollRange() / 2;
                float percentage = ((float) Math.abs(verticalOffset) / ((float) maxScroll / 2)) - 1.5f;
                iv_toolbar_backgraond.setAlpha(percentage);
                ll_app_bar_user_info.setAlpha(percentage * -1);
            }
        });

//        String idTask = ((MainActivity) Objects.requireNonNull(getActivity())).getActionSubjectId();
//        FirebaseDatabase database1 = FirebaseDatabase.getInstance();
//        DatabaseReference myRef = database1.getReference(Fields.TASKS_TABLE);
//        myRef.child(idTask).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot snapshot) {
//                try {
//                    if (snapshot.getValue() != null) {
//                        try {
//                            mTask = snapshot.getValue(MTask.class);
//                        } catch (Exception e) {
//                            Function.eLog(e.getMessage(), getClass().getName());
//                        }
//                    } else {
//                        Function.eLog("snapshot.getValue() == null", getClass().getName());
//                    }
//                } catch (Exception e) {
//                    Function.eLog(e.getMessage(), getClass().getName());
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//                Function.eLog(databaseError.getMessage(), " DatabaseError");
//            }
//        });
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void createTask(MTask mTask);
    }

    public void setTask(MTask mTask){
        this.mTask = mTask;
    }
}

package ua.net.advokat24.aplication.dialigs;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import ua.net.advokat24.MyApp;
import ua.net.advokat24.R;
import ua.net.advokat24.util.Function;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.net.advokat24.util.Function;

public class EditPriseDialog extends DialogFragment {

    private String head = "Введите цену";
    private String body = "Пример: 5 (5 грн)";
    private String positive = MyApp.getContext().getResources().getString(R.string.confirm);
    private String negative = MyApp.getContext().getResources().getString(R.string.cancel);
    private String curentText = "";

    private static final String HEAD = "head";
    private static final String BODY = "body";
    private static final String POSITIVE = "positive";
    private static final String NEGATIVE = "negative";
    private static final String CURENT_TEXT = "curentText";

    public static EditPriseDialog newInstance() {
        return new EditPriseDialog();
    }

    public static EditPriseDialog newInstance(String head, String body, String positive, String negative, String curentText) {
        EditPriseDialog fragment = new EditPriseDialog();
        Bundle bundle = new Bundle();
        bundle.putString(HEAD, head);
        bundle.putString(BODY, body);
        bundle.putString(POSITIVE, positive);
        bundle.putString(NEGATIVE, negative);
        bundle.putString(CURENT_TEXT, curentText);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            head = getArguments().getString(HEAD, head);
            body = getArguments().getString(BODY, body);
            positive = getArguments().getString(POSITIVE, positive);
            negative = getArguments().getString(NEGATIVE, negative);
            curentText = getArguments().getString(CURENT_TEXT, curentText);
        }
    }

    @BindView(R.id.iv_action) ImageView iv_action;
    @BindView(R.id.tv_positive) TextView tv_positive;
    @BindView(R.id.tv_negative) TextView tv_negative;
    @BindView(R.id.tv_head) TextView tv_head;
    @BindView(R.id.tv_body) TextView tv_body;
    @BindView(R.id.et_text) TextInputEditText et_text;

    public void showError(String text){
        iv_action.setBackgroundColor(getResources().getColor(R.color.colorError));
        Function.showToast(text);
    }

    private void sendResult(String text){
        if(event != null){
            event.confirm(text);
        }
        dismiss();
    }

    @OnClick(R.id.fl_ok) void fl_ok() {
        if (!TextUtils.isEmpty(et_text.getText().toString()) && et_text.getText().toString().length() < 7) {
            sendResult(et_text.getText().toString());
        } else {
            showError("Введите корректную сумму сумму");
        }
    }

    @OnClick(R.id.fl_cancel) void fl_cancel() {
        if(eventCancelable != null){
            eventCancelable.cancel();
        }
        dismiss();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        Objects.requireNonNull(getDialog().getWindow()).setBackgroundDrawableResource(R.color.colorTrans);
        View v = inflater.inflate(R.layout.d_edit_text, container, false);
        ButterKnife.bind(this, v);


        tv_positive.setText(positive);
        tv_negative.setText(negative);
        tv_head.setText(head);
        tv_body.setText(body);
        et_text.setText(curentText);

        et_text.setInputType(InputType.TYPE_CLASS_NUMBER);

        et_text.requestFocus();


        if(head.equals(getContext().getResources().getString(R.string.enter_you_phone)))
            et_text.setInputType(InputType.TYPE_CLASS_NUMBER);

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        Objects.requireNonNull(getDialog().getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        event.dismiss();
        super.onDismiss(dialog);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        Objects.requireNonNull(getDialog().getWindow()).getAttributes().windowAnimations = R.style.DialogAnimationNew;
    }

    public interface Event {
        void confirm(String text);
        void dismiss();
    }

    private Event event;

    public void registerEvent(Event event) {
        this.event = event;
    }


    public interface EventCancelable {
        void cancel();
    }

    private EventCancelable eventCancelable;

    public void registerEventCancelable(EventCancelable eventCancelable) {
        this.eventCancelable = eventCancelable;
    }
}
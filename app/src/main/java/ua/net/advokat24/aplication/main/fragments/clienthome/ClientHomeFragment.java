package ua.net.advokat24.aplication.main.fragments.clienthome;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.MvpView;
import ua.net.advokat24.R;
import ua.net.advokat24.aplication.dialigs.ConfirmDialog;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.aplication.services.ServiceNewTask;
import ua.net.advokat24.repository.firebase.createtask.CreateTaskImpl;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.utils.curent_location.LocationImpl;
import ua.net.advokat24.util.utils.curent_location.LocationInteractor;
import ua.net.advokat24.util.utils.curent_location.LocationListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.net.advokat24.repository.firebase.createtask.CreateTaskImpl;
import ua.net.advokat24.util.Function;

public class ClientHomeFragment extends MvpAppCompatFragment implements MvpView {

    private OnFragmentInteractionListener mListener;

    @BindView(R.id.tv_search_before)
    TextView tv_search_before;
    @BindView(R.id.iv_search_before)
    ImageView iv_search_before;
    @BindView(R.id.ll_is_search)
    LinearLayout ll_is_search;
    @BindView(R.id.fl_progress)
    FrameLayout fl_progress;

    @BindView(R.id.fl_click)
    FrameLayout fl_click;

    @OnClick(R.id.fl_click)
    void startSearch() {
        if (!CreateTaskImpl.isRun) {
            fl_click.setClickable(false);
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                setUserLocation();
            } else {
                ((MainActivity) getActivity()).startPermissionDialog(MainActivity.LOCATION_PERMISSION_TASK);
            }
        }
    }

    public void setUserLocation(){
        final ProgressDialog dialogWait = ProgressDialog.show(getContext(), "", getContext().getResources().getString(R.string.loading), false, false);
        LocationInteractor interactor = new LocationImpl();
        interactor.getCurentLocation(getActivity(), new LocationListener() {
            @Override
            public void licationIs(Location location) {
                if (dialogWait.isShowing()) dialogWait.dismiss();
                if (mListener != null) mListener.createTask(location);
                fl_click.setClickable(true);
            }

            @Override
            public void permissionMiss() {
                Function.showToast(getResources().getString(R.string.error_gps_permission_reqwest));
                if (dialogWait.isShowing()) dialogWait.dismiss();
                Function.showToast(getResources().getString(R.string.open_permission_activity));
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
                fl_click.setClickable(true);

            }

            @Override
            public void error() {
                Function.showToast(getResources().getString(R.string.error_gps_reqwest));
                if (dialogWait.isShowing()) dialogWait.dismiss();
                fl_click.setClickable(true);
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        });
    }



    public ClientHomeFragment() { }

    private static ClientHomeFragment instance;

    public static ClientHomeFragment newInstance() {
        if (instance == null) {
            instance = new ClientHomeFragment();
        }
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.f_client_home, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {
            onResume();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getUserVisibleHint()) {
            return;
        }
        chengeStatus();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void createTask(Location location);
    }

    public void chengeStatus() {
        fl_progress.clearAnimation();
        if (CreateTaskImpl.isRun) {
            ll_is_search.setVisibility(View.VISIBLE);
            tv_search_before.setVisibility(View.GONE);
            iv_search_before.setVisibility(View.GONE);

            RotateAnimation rotateAnimation = new RotateAnimation(360f, 0,
                    Animation.RELATIVE_TO_SELF, 0.5f,
                    Animation.RELATIVE_TO_SELF, 0.5f);

            rotateAnimation.setInterpolator(new LinearInterpolator());
            rotateAnimation.setDuration(5000);
            rotateAnimation.setRepeatCount(Animation.INFINITE);
            fl_progress.startAnimation(rotateAnimation);

        } else {
            ll_is_search.setVisibility(View.GONE);
            tv_search_before.setVisibility(View.VISIBLE);
            iv_search_before.setVisibility(View.VISIBLE);
        }
    }

    public  void closeLoading(){

    }
}

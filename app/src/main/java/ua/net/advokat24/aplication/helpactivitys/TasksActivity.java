package ua.net.advokat24.aplication.helpactivitys;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import ua.net.advokat24.R;
import ua.net.advokat24.aplication.adapters.TasksAdapter;
import ua.net.advokat24.repository.firebase.categoryimpl.CategoryImpl;
import ua.net.advokat24.repository.firebase.categoryimpl.CategorysInteractor;
import ua.net.advokat24.repository.firebase.categoryimpl.OnCategorysListener;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksImpl;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksInteractor;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksListener;
import ua.net.advokat24.util.models.CategoryListModel;
import ua.net.advokat24.util.models.MTask;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.net.advokat24.repository.firebase.categoryimpl.CategoryImpl;
import ua.net.advokat24.repository.firebase.categoryimpl.CategorysInteractor;
import ua.net.advokat24.repository.firebase.categoryimpl.OnCategorysListener;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksImpl;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksInteractor;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksListener;

public class TasksActivity extends AppCompatActivity {

    public static final String KEY = "TasksActivity";
    public static final String KEY_IS_KIENT = "ClientTasksActivityisbppl";

    @BindView(R.id.tv_empty_list)
    TextView tv_empty_list;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @OnClick(R.id.iv_toolbar_back)
    void iv_toolbar_back() {
        this.finish();
    }

    private TasksAdapter adapter;
    private List<MTask> mTaskList;
    private List<CategoryListModel> categoryList;

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_client_tasks);
        ButterKnife.bind(this);

        id = getIntent().getStringExtra(KEY);
        isClient = getIntent().getBooleanExtra(KEY_IS_KIENT, false);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new TasksAdapter(this, isClient);
        recyclerView.setAdapter(adapter);

        CategorysInteractor categorys = new CategoryImpl();
        categorys.getList(new OnCategorysListener() {
            @Override
            public void resultList(List<CategoryListModel> listcategorys) {
                categoryList = listcategorys;
                TasksInteractor init = new TasksImpl();
                init.getTasksRunTime(id, isClient, new TasksListener() {
                    @Override
                    public void resultList(final List<MTask> listtasks) {
                        mTaskList = listtasks;
                        showResult();
                    }

                    @Override
                    public void error() {

                    }
                });
            }

            @Override
            public void error() {
                tv_empty_list.setVisibility(View.VISIBLE);
            }
        });
    }

    private String id;
    private boolean isClient = false;

    public void showResult() {
        if (mTaskList == null || mTaskList.size() == 0) {
            tv_empty_list.setVisibility(View.VISIBLE);
        } else {
            adapter.initList(mTaskList, categoryList);
        }
    }
}
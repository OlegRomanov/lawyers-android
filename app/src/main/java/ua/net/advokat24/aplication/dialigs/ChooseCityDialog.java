package ua.net.advokat24.aplication.dialigs;

import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import ua.net.advokat24.R;
import ua.net.advokat24.aplication.adapters.CitysAdapter;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.citys.City;
import ua.net.advokat24.util.models.usermodels.WorkTime;
import ua.net.advokat24.util.utils.CitysUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ua.net.advokat24.aplication.adapters.CitysAdapter;
import ua.net.advokat24.util.Function;

public class ChooseCityDialog extends DialogFragment {

    private String city;

    private static final String KEY = "cityDialogKey";

    public static ChooseCityDialog newInstance(String city) {
        ChooseCityDialog fragment = new ChooseCityDialog();
        Bundle bundle = new Bundle();
        bundle.putString(KEY, city);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            city = getArguments().getString(KEY);
        }
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogTheme);
    }


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @OnClick(R.id.iv_toolbar_action) void iv_toolbar_action() {
        if(adapter.getCityExist().length() != 0){
            event.confirm(adapter.getCityExist());
            dismiss();
        } else {
            Function.showToast(getContext().getResources().getString(R.string.city_not_choose));
        }

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        Objects.requireNonNull(getDialog().getWindow()).setBackgroundDrawableResource(R.color.colorTrans);
        View v = inflater.inflate(R.layout.d_choose_city, container, false);
        unbinder = ButterKnife.bind(this, v);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new CitysAdapter(getContext(), CitysUtils.getListCitys(), layoutManager, city);
        recyclerView.setAdapter(adapter);
        initToolbar();
        search("");
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private List<City> citysMain = CitysUtils.getListCitys();
    private CitysAdapter adapter;
    private Unbinder unbinder;

    public void search(String qwerty){
        qwerty = qwerty.toLowerCase();
        final ArrayList<City> filteredModelList = new ArrayList<>();
        for (City model : citysMain) {
            final String text = model.getName().toLowerCase();
            if (text.contains(qwerty)) {
                filteredModelList.add(model);
            }
        }
        adapter.initList(filteredModelList);
    }

    private void initToolbar() {
        toolbar.inflateMenu(R.menu.m_search);
        toolbar.setTitle(getContext().getResources().getString(R.string.city_choose));
        MenuItem searchItem = toolbar.getMenu().findItem(R.id.btn_search);
        searchItem.getIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setQueryHint(getContext().getResources().getString(R.string.search));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                search(newText);
                return true;
            }
        });
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Objects.requireNonNull(getDialog().getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        event.isDismis();
        super.onDismiss(dialog);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        Objects.requireNonNull(getDialog().getWindow()).getAttributes().windowAnimations = R.style.DialogAnimationNew;
    }

    public interface Event {
        void isDismis();
        void confirm(String city);
    }

    private Event event;

    public void registerEvent(Event event) {
        this.event = event;
    }
}
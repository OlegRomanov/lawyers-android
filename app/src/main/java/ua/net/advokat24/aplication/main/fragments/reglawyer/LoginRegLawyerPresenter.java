package ua.net.advokat24.aplication.main.fragments.reglawyer;

import android.net.Uri;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import ua.net.advokat24.MyApp;
import ua.net.advokat24.repository.SharedPreferencesManager;
import ua.net.advokat24.repository.firebase.categoryimpl.CategoryImpl;
import ua.net.advokat24.repository.firebase.categoryimpl.CategorysInteractor;
import ua.net.advokat24.repository.firebase.categoryimpl.OnCategorysListener;
import ua.net.advokat24.repository.firebase.userupdate.UserUpdateImpl;
import ua.net.advokat24.repository.firebase.userupdate.UserUpdateInteractor;
import ua.net.advokat24.repository.firebase.userupdate.UserUpdateListener;
import ua.net.advokat24.util.models.CategoryListModel;
import ua.net.advokat24.util.models.usermodels.MUser;

import java.util.List;

import ua.net.advokat24.repository.SharedPreferencesManager;
import ua.net.advokat24.repository.firebase.categoryimpl.CategoryImpl;
import ua.net.advokat24.repository.firebase.categoryimpl.CategorysInteractor;
import ua.net.advokat24.repository.firebase.categoryimpl.OnCategorysListener;
import ua.net.advokat24.repository.firebase.userupdate.UserUpdateImpl;
import ua.net.advokat24.repository.firebase.userupdate.UserUpdateInteractor;
import ua.net.advokat24.repository.firebase.userupdate.UserUpdateListener;

@InjectViewState
public class LoginRegLawyerPresenter extends MvpPresenter<LoginRegLawyerView> implements OnCategorysListener {

    private CategorysInteractor categorysInteractor;
    private List<CategoryListModel> list;
    private UserUpdateInteractor userUpdateInteractor;
    private MUser mUser;
    private Uri photo;
    private Uri document;
    private String skils;

    public MUser getmUser() {
        if(mUser == null){
            mUser = new MUser();
        }
        return mUser;
    }


    public String getSkils() {
        return skils;
    }

    public void setSkils(String skils) {
        this.skils = skils;
    }

    public Uri getPhoto() {
        return photo;
    }

    public void setPhoto(Uri photo) {
        this.photo = photo;
    }

    public Uri getDocument() {
        return document;
    }

    public void setDocument(Uri document) {
        this.document = document;
    }

    public List<CategoryListModel> getList() {
        return list;
    }

    public LoginRegLawyerPresenter() {
        categorysInteractor = new CategoryImpl();
        userUpdateInteractor = new UserUpdateImpl();
        categorysInteractor.getList(this);
    }


    @Override
    public void resultList(List<CategoryListModel> list) {
        this.list = list;
    }

    @Override
    public void error() {
        getViewState().initPageError("Wow");
    }

    public MUser getGeadyUser(){
        getmUser().setLocatoinLat("0.0");
        getmUser().setLocatoinLon("0.0");
        getmUser().setReiting(5);
        getmUser().setReitingCount(1);
        getmUser().setStatus(1);
        getmUser().setClient(SharedPreferencesManager.getInstance(MyApp.getContext()).isClient());
        return getmUser();

    }

    public void updateUser(){
        getmUser().setLocatoinLat("0.0");
        getmUser().setLocatoinLon("0.0");
        getmUser().setReiting(5);
        getmUser().setReitingCount(1);
        getmUser().setStatus(1);

        if((FirebaseAuth.getInstance().getCurrentUser()).getPhotoUrl() != null){
            mUser.setPhoto(String.valueOf(FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl()));
        }
        getmUser().setClient(SharedPreferencesManager.getInstance(MyApp.getContext()).isClient());
        userUpdateInteractor.updateUser(getmUser(), new UserUpdateListener() {
            @Override
            public void error(String error) {
                getViewState().error(error);
            }

            @Override
            public void user(MUser mUser) {
                SharedPreferencesManager.getInstance(MyApp.getContext()).setIsRegister(true);
                getViewState().saccess();
            }
        });
    }

    public void preinitUser(boolean mainThread){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user != null) {
            getmUser().setId(user.getUid());
            getmUser().setEmail(user.getEmail());
            getmUser().setName(user.getDisplayName());
            getmUser().setPhoto(String.valueOf(user.getPhotoUrl()));
        }
        getViewState().userReady();
    }


}

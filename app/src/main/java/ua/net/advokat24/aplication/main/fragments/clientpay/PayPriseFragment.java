package ua.net.advokat24.aplication.main.fragments.clientpay;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import ua.net.advokat24.R;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.repository.firebase.taskget.TaskGetImpl;
import ua.net.advokat24.repository.firebase.taskget.TaskGetInteractor;
import ua.net.advokat24.repository.firebase.taskget.TaskGetListener;
import ua.net.advokat24.repository.firebase.userget.UserGetImpl;
import ua.net.advokat24.repository.firebase.userget.UserGetInteractor;
import ua.net.advokat24.repository.firebase.userget.UserGetListener;
import ua.net.advokat24.util.Fields;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.MTask;
import ua.net.advokat24.util.models.usermodels.MUser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.repository.firebase.taskget.TaskGetImpl;
import ua.net.advokat24.repository.firebase.taskget.TaskGetInteractor;
import ua.net.advokat24.repository.firebase.taskget.TaskGetListener;
import ua.net.advokat24.repository.firebase.userget.UserGetImpl;
import ua.net.advokat24.repository.firebase.userget.UserGetInteractor;
import ua.net.advokat24.repository.firebase.userget.UserGetListener;
import ua.net.advokat24.util.Function;

public class PayPriseFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public PayPriseFragment() {

    }

    @BindView(R.id.ll_app_bar_user_info) LinearLayout ll_app_bar_user_info;
    @BindView(R.id.iv_toolbar_backgraond) ImageView iv_toolbar_backgraond;
    @BindView(R.id.app_bar) AppBarLayout app_bar;
    @BindView(R.id.tv_sum)
    TextView tv_sum;

    @OnClick(R.id.tv_btn_action) void tv_btn_action() {
        if(mListener != null){
//            mListener.payForTask();
        }
    }

    @OnClick(R.id.iv_toolbar_back) void toolbarBack() {
        if (mListener != null) {
            mListener.backToHome();
        }
    }

    public static PayPriseFragment newInstance() {
        return new PayPriseFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.f_pay_prise, container, false);
        ButterKnife.bind(this, v);
        app_bar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                int maxScroll = appBarLayout.getTotalScrollRange() / 2;
                float percentage = ((float) Math.abs(verticalOffset) / ((float) maxScroll / 2)) - 1.5f;
                iv_toolbar_backgraond.setAlpha(percentage);
                ll_app_bar_user_info.setAlpha(percentage * -1);    }
        });

        String idTask = ((MainActivity) Objects.requireNonNull(getActivity())).getActionSubjectId();

        TaskGetInteractor taskGetInteractor = new TaskGetImpl();
        taskGetInteractor.getTask(idTask, new TaskGetListener() {
            @Override
            public void task(final MTask mTask) {
                UserGetInteractor userGetInteractor = new UserGetImpl();
                userGetInteractor.getUser(mTask.getLawyerId(), new UserGetListener() {
                    @Override
                    public void error(String error) { }

                    @Override
                    public void user(MUser mUser) {
                        tv_sum.setText(String.valueOf(getPrise(mUser, mTask)));
                    }
                });
            }
        });
        return v;
    }

    private int getPrise(MUser mUser, MTask mTask){
        if(isTime(mUser.getWorkTime().getTimeStart(), mUser.getWorkTime().getTimeEnd(), mTask.getData().substring(11, 16)))
            return mUser.getWorkTime().getPrise();

//        if(isTime(mUser.getWorkTime().getTimeDoubleStart(), mUser.getWorkTime().getTimeDoubleEnd(), mTask.getData().substring(11, 16)))
//            return mUser.getWorkTime().getPriseDouble();

        return 0;
    }

    private boolean isTime(String start, String end, String curent){
        boolean result = false;
        try {
            Date time1 = new SimpleDateFormat(Fields.HOUR_TIME_FORMAT).parse(start);
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(time1);

            Date time2 = new SimpleDateFormat(Fields.HOUR_TIME_FORMAT).parse(end);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(time2);
            calendar2.add(Calendar.DATE, 1);

            Date d = new SimpleDateFormat(Fields.HOUR_TIME_FORMAT).parse(curent);
            Calendar calendar3 = Calendar.getInstance();
            calendar3.setTime(d);
            calendar3.add(Calendar.DATE, 1);

            Date x = calendar3.getTime();
            if (x.after(calendar1.getTime()) && x.before(calendar2.getTime())) {
                result = true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Function.log(String.valueOf(result), getClass().getName() + ".isTime");
        return result;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void backToHome();
    }
}

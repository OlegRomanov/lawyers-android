package ua.net.advokat24.aplication.dialigs;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatDelegate;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import ua.net.advokat24.R;
import ua.net.advokat24.util.models.CategoryListModel;
import com.shawnlin.numberpicker.NumberPicker;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChooseSkilsDialog extends DialogFragment {

    private static final String KEY = "chooseSkiilKey";

    private List<CategoryListModel>list;
    private String[] listBaseCategoty;
    private CategoryListModel category;

    @BindView(R.id.tv_ok)TextView tv_ok;
    @OnClick(R.id.tv_ok) void confirmClick() {
        category = list.get(picker_category.getValue() - 1);
        if(event != null)
            event.result(category.getId());
        dismiss();
    }

    @BindView(R.id.tv_cancel)TextView tv_cancel;
    @OnClick(R.id.tv_cancel) void cancelClick() {
        dismiss();
    }

    @BindView(R.id.fl_out_click)FrameLayout fl_out_click;
    @OnClick(R.id.fl_out_click) void exit() {
        dismiss();
    }

    @BindView(R.id.picker_category)NumberPicker picker_category;
    @BindView(R.id.picker_sub_category)NumberPicker picker_sub_category;

    public static ChooseSkilsDialog newInstance(String gson) {
       ChooseSkilsDialog fragment = new ChooseSkilsDialog();
       Bundle bundle = new Bundle();
       bundle.putString(KEY, gson);
       fragment.setArguments(bundle);
       return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
           String dlist = getArguments().getString(KEY);
           list = new Gson().fromJson(dlist, new TypeToken<List<CategoryListModel>>() {}.getType());
           if(list != null){
               listBaseCategoty = new String[list.size()];
               for (int i = 0; i < list.size(); i++){
                   listBaseCategoty[i] = list.get(i).getName();
               }
           }
        }
    }

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.backgraund_dialogs);
        View v = inflater.inflate(R.layout.d_choose_skils, container, false);
        ButterKnife.bind(this, v);

        picker_category.setMinValue(1);
        picker_category.setMaxValue(listBaseCategoty.length);
        picker_category.setDisplayedValues(listBaseCategoty);
        picker_category.setValue(2);
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }
    @Override
    public void onDismiss(DialogInterface dialog) {
        event.dismiss();
        super.onDismiss(dialog);
    }
    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimationNew;
    }

    public interface Event {
        void result(String result);
        void dismiss();
    }
    private Event event;
    public void registerEvent(Event event){
        this.event = event;
    }
}
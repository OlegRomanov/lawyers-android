package ua.net.advokat24.aplication.main.fragments.clientinfo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import ua.net.advokat24.R;
import ua.net.advokat24.aplication.dialigs.ChooseCityDialog;
import ua.net.advokat24.aplication.dialigs.ConfirmDialog;
import ua.net.advokat24.aplication.dialigs.EditTextDialog;
import ua.net.advokat24.aplication.helpactivitys.TasksActivity;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.util.Fields;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.usermodels.MUser;
import ua.net.advokat24.util.utils.FirebaseUtils;
import ua.net.advokat24.util.utils.ImageUtils;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.net.advokat24.util.Function;

public class ClientInfoFragment extends Fragment {

    public ClientInfoFragment() {
    }

    private OnFragmentInteractionListener mListener;

    @BindView(R.id.fl_field_name)
    FrameLayout fl_field_name;

    @BindView(R.id.ll_app_bar_user_info)
    LinearLayout ll_app_bar_user_info;
    @BindView(R.id.iv_toolbar_backgraond)
    ImageView iv_toolbar_backgraond;
    @BindView(R.id.app_bar)
    AppBarLayout app_bar;
    @BindView(R.id.tv_app_bar_user_name)
    TextView tv_app_bar_user_name;
    @BindView(R.id.iv_app_bar_user_photo)
    ImageView iv_app_bar_user_photo;
    @BindView(R.id.iv_app_bar_user_photo_back)
    ImageView iv_app_bar_user_photo_back;
    @BindView(R.id.tv_field_name)
    TextView tv_field_name;
    @BindView(R.id.tv_field_city)
    TextView tv_field_city;
    @BindView(R.id.tv_field_sum_tasks)
    TextView tv_field_sum_tasks;
    @BindView(R.id.fl_field_city)
    FrameLayout fl_field_city;

    private MUser mUser;

    @OnClick(R.id.iv_toolbar_action)
    void exit() {
        ConfirmDialog dialog = ConfirmDialog.newInstance(
                getContext().getResources().getString(R.string.go_out), getContext().getResources().getString(R.string.you_exit_dialog),
                getContext().getResources().getString(R.string.go_out), getContext().getResources().getString(R.string.cancel));
        dialog.registerEvent(new ConfirmDialog.Event() {
            @Override
            public void confirm() {
                DatabaseReference mDatabase = FirebaseUtils.getDatabase().getReference(Fields.USERS_TABTE);
                mDatabase.child(mUser.getId()).child("notificationKay").setValue(null);
                if (mListener != null) {
                    mListener.logOut();
                }
            }

            @Override
            public void fail() {}

            @Override
            public void isDismis() {}
        });
        dialog.show(getActivity().getSupportFragmentManager(), dialog.getClass().getSimpleName());
    }

    @OnClick(R.id.fl_field_name)
    void fl_field_name() {
        fl_field_name.setClickable(false);
        EditTextDialog dialogText = EditTextDialog.newInstance(
                getContext().getResources().getString(R.string.enter_you_name),
                getContext().getResources().getString(R.string.enter_you_name_and_subname_norm),
                getContext().getResources().getString(R.string.confirm),
                getContext().getResources().getString(R.string.cancel),
                tv_field_name.getText().toString());

        dialogText.registerEvent(new EditTextDialog.Event() {
            @Override
            public void confirm(String text) {
                tv_app_bar_user_name.setText(text + getContext().getResources().getString(R.string.my_profile_you));
                tv_field_name.setText(text);
                ((MainActivity) getActivity()).getmUser().setName(text);
                ((MainActivity) getActivity()).saveUser();
            }

            @Override
            public void dismiss() {
                fl_field_name.setClickable(true);

            }
        });
        dialogText.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), dialogText.getClass().getSimpleName());
    }

    @OnClick(R.id.fl_field_city)
    void fl_field_city() {
        fl_field_city.setClickable(false);
        ChooseCityDialog cityDialog = ChooseCityDialog.newInstance(tv_field_city.getText().toString());
        cityDialog.registerEvent(new ChooseCityDialog.Event() {
            @Override
            public void isDismis() {
                fl_field_city.setClickable(true);
            }

            @Override
            public void confirm(String city) {
                tv_field_city.setText(city);
                ((MainActivity) getActivity()).getmUser().setCity(city);
                ((MainActivity) getActivity()).saveUser();

            }
        });
        cityDialog.show(getActivity().getSupportFragmentManager(), cityDialog.getClass().getSimpleName());
    }

    @OnClick(R.id.tv_works)
    void tv_works() {
        if (((MainActivity) getActivity()).getListMyTasks() != null || ((MainActivity) getActivity()).getListMyTasks().size() != 0) {
            Intent activity = new Intent(getContext(), TasksActivity.class);
            activity.putExtra(TasksActivity.KEY, ((MainActivity) getActivity()).getmUser().getId());
            activity.putExtra(TasksActivity.KEY_IS_KIENT, true);
            getContext().startActivity(activity);
        } else {
            Function.showToast(getContext().getResources().getString(R.string.empty_list_tasks));
        }
    }

    @OnClick(R.id.iv_app_bar_user_photo)
    void putImage() {
        if (mListener != null) {
            mListener.putPhoto(true, false);
        }
    }

    @OnClick(R.id.iv_toolbar_back)
    void iv_toolbar_back() {
        if (mListener != null) {
            mListener.backToHome();
        }
    }

    public static ClientInfoFragment newInstance() {
        return new ClientInfoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.f_client_info, container, false);
        ButterKnife.bind(this, v);

        mUser = ((MainActivity) getActivity()).getmUser();

        tv_app_bar_user_name.setText(mUser.getName() + getContext().getResources().getString(R.string.my_profile_you));
        tv_field_name.setText(mUser.getName());
        tv_field_city.setText(mUser.getCity());
        tv_field_sum_tasks.setText(String.valueOf(((MainActivity) getActivity()).getListMyTasks().size()));
        loadPhoto(mUser);

        app_bar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                int maxScroll = appBarLayout.getTotalScrollRange() / 2;
                float percentage = ((float) Math.abs(verticalOffset) / ((float) maxScroll / 2)) - 1.5f;
                iv_toolbar_backgraond.setAlpha(percentage);
                ll_app_bar_user_info.setAlpha(percentage * -1);
            }
        });
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void logOut();

        void backToHome();

        void putPhoto(boolean isClient, boolean isDocuments);
    }

    private void loadPhoto(MUser mUser) {

        ImageUtils.loadPhoto(getContext(), mUser, iv_app_bar_user_photo);

//        if (mUser.getPhoto() != null) {
//            GlideApp.with(this)
//                    .load(mUser.getPhoto())
//                    .centerCrop()
//                    .diskCacheStrategy(DiskCacheStrategy.NONE)
//                    .skipMemoryCache(true)
//                    .diskCacheStrategy(DiskCacheStrategy.NONE)
//                    .transform(new MultiTransformation<Bitmap>(new CircleCrop()))
//                    .placeholder(R.drawable.ic_user_check_off)
//                    .into(iv_app_bar_user_photo);
//        } else {
//            GlideApp.with(this)
//                    .load(FirebaseStorage.getInstance().getReference().child("images/userphoto/" + mUser.getId() + ".jpg"))
//                    .centerCrop()
//                    .diskCacheStrategy(DiskCacheStrategy.NONE)
//                    .skipMemoryCache(true)
//                    .transform(new MultiTransformation<Bitmap>(new CircleCrop()))
//                    .placeholder(R.drawable.ic_user_check_off)
//                    .error(R.drawable.ic_user_check_off)
//                    .into(iv_app_bar_user_photo);
//        }
    }

    public void updatePhoto(){
        Glide.get(getContext()).clearMemory();
        iv_app_bar_user_photo.setImageDrawable(null);
        loadPhoto(mUser);
    }
}
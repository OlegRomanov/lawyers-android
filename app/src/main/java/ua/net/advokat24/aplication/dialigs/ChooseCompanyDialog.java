package ua.net.advokat24.aplication.dialigs;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import ua.net.advokat24.R;
import ua.net.advokat24.aplication.adapters.CitysAdapter;
import ua.net.advokat24.aplication.adapters.CompanyAdapter;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.repository.firebase.company_create.CompanyCreateImpl;
import ua.net.advokat24.repository.firebase.company_create.CompanyCreateInteractor;
import ua.net.advokat24.repository.firebase.company_create.CompanyCreateListener;
import ua.net.advokat24.repository.firebase.company_get_list.CompanyGetListImpl;
import ua.net.advokat24.repository.firebase.company_get_list.CompanyGetListInteractor;
import ua.net.advokat24.repository.firebase.company_get_list.CompanyGetListListener;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.BaseModel;
import ua.net.advokat24.util.models.citys.City;
import ua.net.advokat24.util.utils.CitysUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ua.net.advokat24.util.Function;

public class ChooseCompanyDialog extends DialogFragment {

    private String curent;
    private String userId;
    private List<BaseModel> mainList;
    private CompanyAdapter adapter;
    private Unbinder unbinder;
    private ProgressDialog dialogWait;

    private static final String KEY_CURENT = "ChooseCompanyDialogKey";
    private static final String KEY_ID = "ChooseCompanyDialogKeyId";

    public static ChooseCompanyDialog newInstance(String curent, String userId) {
        ChooseCompanyDialog fragment = new ChooseCompanyDialog();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_CURENT, curent);
        bundle.putString(KEY_ID, userId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            curent = getArguments().getString(KEY_CURENT);
            userId = getArguments().getString(KEY_ID);
        }
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogTheme);
    }

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.fab_add) FloatingActionButton fab_add;

    @OnClick(R.id.iv_toolbar_action) void iv_toolbar_action() {
        if(adapter.getCityExist().length() != 0){
            if(event != null){
                event.confirm(adapter.getCityExist());
                dismiss();
            }
        } else {
            Function.showToast("Компания не выбрана");
        }
    }

    @OnClick(R.id.fab_add) void fab_add() {
        fab_add.setClickable(false);
        final EditTextDialog dialogText = EditTextDialog.newInstance(getContext().getResources().getString(R.string.edit_compani_name),
                getContext().getResources().getString(R.string.edit_compani_name_sub),
                getContext().getResources().getString(R.string.confirm),
                getContext().getResources().getString(R.string.cancel),
                "");
        dialogText.registerEvent(new EditTextDialog.Event() {
            @Override
            public void confirm(final String text) {
                showProgressDialog();
                CompanyCreateInteractor interactor = new CompanyCreateImpl();
                interactor.createCompany(new BaseModel(userId, text), new CompanyCreateListener() {
                    @Override
                    public void saccess() {
                        if(dialogWait != null) dialogWait.dismiss();
                        if(event != null){
                            event.confirm(text);
                        }
                    }

                    @Override
                    public void isExist() {
                        if(dialogWait != null) dialogWait.dismiss();
                        Function.showToast("Такая компания уже существует");
                    }

                    @Override
                    public void error() {
                        if(dialogWait != null) dialogWait.dismiss();
                        Function.showToast("Ошибка приложения");
                    }
                });
            }

            @Override
            public void dismiss() {
                fab_add.setClickable(true);
            }
        });
        dialogText.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), dialogText.getClass().getSimpleName());
    }

    private void showProgressDialog(){
        dialogWait = ProgressDialog.show(getContext(),
                getResources().getString(R.string.loading),
                getResources().getString(R.string.loading_disc),
                true, false);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        Objects.requireNonNull(getDialog().getWindow()).setBackgroundDrawableResource(R.color.colorTrans);
        View v = inflater.inflate(R.layout.d_choose_city, container, false);
        unbinder = ButterKnife.bind(this, v);
        fab_add.setVisibility(View.VISIBLE);
        initToolbar();

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new CompanyAdapter(curent);
        recyclerView.setAdapter(adapter);
        setList(curent);
        return v;
    }

    private void setList(final String startName){
        CompanyGetListInteractor interactor = new CompanyGetListImpl();
        interactor.getCompaniList(new CompanyGetListListener() {
            @Override
            public void saccess(List<BaseModel> list) {
                if(list != null && list.size() != 0){
                    mainList = list;
                    search("");
                } else {
                    Function.showToast("Список компаний пуст");
                }
                if(dialogWait != null)
                    dialogWait.dismiss();
            }

            @Override
            public void error() {
                Function.showToast("Ошибка приложения");
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void search(String qwerty){
        qwerty = qwerty.toLowerCase();
        final ArrayList<BaseModel> filteredModelList = new ArrayList<>();
        if(mainList != null){
            for (BaseModel model : mainList) {
                final String text = model.getName().toLowerCase();
                if (text.contains(qwerty)) {
                    filteredModelList.add(model);
                }
            }
        }
        adapter.initList(filteredModelList);

    }

    private void initToolbar() {
        toolbar.inflateMenu(R.menu.m_search);
        toolbar.setTitle(getContext().getResources().getString(R.string.city_company));
        MenuItem searchItem = toolbar.getMenu().findItem(R.id.btn_search);
        searchItem.getIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setQueryHint(getContext().getResources().getString(R.string.search));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                search(newText);
                return true;
            }
        });
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Objects.requireNonNull(getDialog().getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        event.isDismis();
        super.onDismiss(dialog);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        Objects.requireNonNull(getDialog().getWindow()).getAttributes().windowAnimations = R.style.DialogAnimationNew;
    }

    public interface Event {
        void isDismis();
        void confirm(String company);
    }

    private Event event;

    public void registerEvent(Event event) {
        this.event = event;
    }
}
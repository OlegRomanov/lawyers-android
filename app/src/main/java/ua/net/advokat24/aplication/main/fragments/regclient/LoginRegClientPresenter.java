package ua.net.advokat24.aplication.main.fragments.regclient;

import android.net.Uri;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import ua.net.advokat24.MyApp;
import ua.net.advokat24.repository.SharedPreferencesManager;
import ua.net.advokat24.repository.firebase.userupdate.UserUpdateImpl;
import ua.net.advokat24.repository.firebase.userupdate.UserUpdateInteractor;
import ua.net.advokat24.repository.firebase.userupdate.UserUpdateListener;
import ua.net.advokat24.util.models.usermodels.MUser;

import ua.net.advokat24.repository.SharedPreferencesManager;
import ua.net.advokat24.repository.firebase.userupdate.UserUpdateImpl;
import ua.net.advokat24.repository.firebase.userupdate.UserUpdateInteractor;
import ua.net.advokat24.repository.firebase.userupdate.UserUpdateListener;

@InjectViewState
public class LoginRegClientPresenter extends MvpPresenter<LoginRegClientView> {

    private UserUpdateInteractor userUpdateInteractor;
    private MUser mUser;
    private Uri photo;

    public MUser getmUser() {
        if(mUser == null){
            mUser = new MUser();
        }
        return mUser;
    }

    public Uri getPhoto() {
        return photo;
    }

    public void setPhoto(Uri photo) {
        this.photo = photo;
    }

    public LoginRegClientPresenter() {
        userUpdateInteractor = new UserUpdateImpl();
        preinitUser(true);
    }

    public void updateUser(){
        getmUser().setClient(SharedPreferencesManager.getInstance(MyApp.getContext()).isClient());
        userUpdateInteractor.updateUser(getmUser(), new UserUpdateListener() {
            @Override
            public void error(String error) {
                getViewState().error(error);
            }

            @Override
            public void user(MUser mUser) {
                SharedPreferencesManager.getInstance(MyApp.getContext()).setIsRegister(true);
                getViewState().saccess();
            }
        });
    }

    public void preinitUser(boolean mainThread){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user != null) {
            getmUser().setId(user.getUid());
            getmUser().setEmail(user.getEmail());
            getmUser().setName(user.getDisplayName());
            getmUser().setPhoto(String.valueOf(user.getPhotoUrl()));
        }
        if(mainThread){
            getViewState().userReady();
        }
    }
}

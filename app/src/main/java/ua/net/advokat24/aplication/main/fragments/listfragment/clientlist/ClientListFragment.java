package ua.net.advokat24.aplication.main.fragments.listfragment.clientlist;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ua.net.advokat24.R;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.aplication.adapters.UsersAdapter;
import ua.net.advokat24.util.models.usermodels.MUser;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.net.advokat24.aplication.adapters.UsersAdapter;
import ua.net.advokat24.aplication.main.MainActivity;

public class ClientListFragment extends Fragment {

    public ClientListFragment() {
        // Required empty public constructor
    }

    private OnFragmentInteractionListener mListener;

    @BindView(R.id.tv_empty_list)
    TextView tv_empty_list;

    public static ClientListFragment newInstance() {
        return new ClientListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.f_client_list, container, false);
        ButterKnife.bind(this, v);

        RecyclerView recyclerView = v.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        UsersAdapter adapter = new UsersAdapter(getContext());
        recyclerView.setAdapter(adapter);
        List<MUser> list = ((MainActivity)getActivity()).getListUsers();
        if(list.size() == 0){
            tv_empty_list.setVisibility(View.VISIBLE);
        } else {
            adapter.setList(((MainActivity)getActivity()).getListUsers());
        }

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void backToHome();
    }
}

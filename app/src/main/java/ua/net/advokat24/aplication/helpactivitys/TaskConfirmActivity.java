package ua.net.advokat24.aplication.helpactivitys;

import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;

import android.text.TextUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import ua.net.advokat24.R;
import ua.net.advokat24.aplication.dialigs.ConfirmDialog;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.aplication.services.ServiceNewTask;
import ua.net.advokat24.repository.firebase.categoryimpl.CategoryImpl;
import ua.net.advokat24.repository.firebase.categoryimpl.CategorysInteractor;
import ua.net.advokat24.repository.firebase.categoryimpl.OnCategorysListener;
import ua.net.advokat24.repository.firebase.taskget.TaskGetImpl;
import ua.net.advokat24.repository.firebase.taskget.TaskGetInteractor;
import ua.net.advokat24.repository.firebase.taskget.TaskGetListener;
import ua.net.advokat24.repository.firebase.userget.UserGetImpl;
import ua.net.advokat24.repository.firebase.userget.UserGetInteractor;
import ua.net.advokat24.repository.firebase.userget.UserGetListener;
import ua.net.advokat24.util.Fields;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.fragmentAnimator.ReplaceToLeft;
import ua.net.advokat24.util.models.CategoryListModel;
import ua.net.advokat24.util.models.MTask;
import ua.net.advokat24.util.models.usermodels.MUser;
import ua.net.advokat24.util.utils.ImageUtils;
import ua.net.advokat24.util.utils.pay_utils.PayImpl;
import ua.net.advokat24.util.utils.pay_utils.PayInteractor;
import ua.net.advokat24.util.utils.pay_utils.PayListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.net.advokat24.util.Function;


public class TaskConfirmActivity extends AppCompatActivity {

    private MTask mTask;
    private MUser user;
    private String key;

    @BindView(R.id.tv_confirm) TextView tv_confirm;
    @BindView(R.id.tv_name_lawyer) TextView tv_name_lawyer;
    @BindView(R.id.tv_name_client) TextView tv_name_client;
    @BindView(R.id.tv_subname_lawyer) TextView tv_subname_lawyer;
    @BindView(R.id.tv_subname_client) TextView tv_subname_client;
    @BindView(R.id.tv_body) TextView tv_body;
    @BindView(R.id.iv_lawyer_photo) ImageView iv_lawyer_photo;
    @BindView(R.id.iv_client_photo) ImageView iv_client_photo;
    @BindView(R.id.fl_lawyer) FrameLayout fl_lawyer;
    @BindView(R.id.fl_click_client) FrameLayout fl_click_client;

    @OnClick(R.id.fl_click_client) void fl_click_client() {
        Intent activity = new Intent(this, ClientDetailsActivity.class);
        activity.putExtra(ClientDetailsActivity.KEY, mTask.getClientId());
        startActivity(activity);
    }
    @OnClick(R.id.fl_lawyer) void fl_lawyer() {
        Intent activity = new Intent(this, LawyerDetailsActivity.class);
        activity.putExtra(LawyerDetailsActivity.KEY, mTask.getLawyerId());
        startActivity(activity);
    }
    @OnClick(R.id.iv_toolbar_back) void iv_toolbar_back() {
        this.finish();
    }

    @OnClick(R.id.tv_confirm) void tv_confirm() {
        if(mTask.isActive() && !mTask.isPremiumRun()){
            tv_confirm.setClickable(false);
            ConfirmDialog dialog = ConfirmDialog.newInstance(
                    this.getResources().getString(R.string.lawyer_pay), this.getResources().getString(R.string.lawyer_pay_sub),
                    this.getResources().getString(R.string.confirm), this.getResources().getString(R.string.cancel));
            dialog.registerEvent(new ConfirmDialog.Event() {
                @Override
                public void confirm() {
                    if(mTask.isActive() && !mTask.isPremiumRun()){
                        payForTask();
                    } else {
                        Function.showToast("Дело передано другому адвокату.");
                    }
                }

                @Override
                public void fail() {
                    Function.showToast("Дело передано другому адвокату.");
                    TaskConfirmActivity.this.finish();
                }

                @Override
                public void isDismis() {
                    tv_confirm.setClickable(true);

                }
            });
            dialog.show(getSupportFragmentManager(), dialog.getClass().getSimpleName());
        } else {
            Function.showToast(getResources().getString(R.string.miss_confirm_task));
        }
    }

    @OnClick(R.id.tv_censel) void tv_censel() {
        if(mTask.isActive()){
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference(Fields.TASKS_TABLE);
            mTask.setActive(true);
            mTask.setAnswerReqwest(true);
            mDatabase.child(mTask.getId()).setValue(mTask);
        }
        this.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_task_confirm);
        ButterKnife.bind(this);
        Bundle b = getIntent().getExtras();
        key = b.getString("extra", "");

        CategorysInteractor categorys = new CategoryImpl();
        categorys.getList(new OnCategorysListener() {
            @Override
            public void resultList(final List<CategoryListModel> listcategorys) {

                FirebaseUser fuser = FirebaseAuth.getInstance().getCurrentUser();
                if (fuser != null) {
                    UserGetInteractor interactor = new UserGetImpl();
                    interactor.getUser(fuser.getUid(), new UserGetListener() {
                        @Override
                        public void error(String error) {}

                        @Override
                        public void user(MUser mUser) {
                            user = mUser;
                        }
                    });
                }
                TaskGetInteractor taskGetInteractor = new TaskGetImpl();
                taskGetInteractor.getTaskRunTime(key, new TaskGetListener() {
                    @Override
                    public void task(final MTask mTask) {
                        TaskConfirmActivity.this.mTask = mTask;
                        if(!TextUtils.isEmpty(mTask.getData()))tv_subname_client.setText(mTask.getData());
                        if(!TextUtils.isEmpty(mTask.getDescriprion()))tv_body.setText(mTask.getDescriprion());
                        if(mTask.getLawyerId() != null){
                            tv_name_lawyer.setText(mTask.getLawyerName());
                            loadPhoto(mTask.getLawyerPhoto(), mTask.getLawyerId(), iv_lawyer_photo);
                        } else {
                            fl_lawyer.setClickable(false);
                            tv_name_lawyer.setText(getResources().getString(R.string.lawyer_not_found));
                            iv_lawyer_photo.setImageResource(R.drawable.ic_uncnow_user);
                        }

                        if(mTask.getClientId() != null){
                            tv_name_client.setText(mTask.getClientName());
                            loadPhoto(mTask.getClientPhoto(), mTask.getClientId(), iv_client_photo);
                        } else {
                            fl_click_client.setClickable(false);
                            tv_name_client.setText(getResources().getString(R.string.no_register));
                            iv_client_photo.setImageResource(R.drawable.ic_uncnow_user);
                        }

                        if (!TextUtils.isEmpty(mTask.getCategory())) {
                            tv_subname_lawyer.setText(Function.getStringSkils(mTask.getCategory(), listcategorys));
                        } else {
                            tv_subname_lawyer.setText(getResources().getString(R.string.anithery));
                        }
                    }
                });
            }

            @Override
            public void error() {}
        });
    }

    private void loadPhoto(String photo, String id, ImageView imageView) {
        ImageUtils.loadPhoto(this, photo, id, imageView);
    }

    public void payForTask() {

//        TODO блокировуа возможности оплатить другому адвокату
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference(Fields.TASKS_TABLE);
        mDatabase.child(key).child("premiumRun").setValue(true);

        PayInteractor interactor = new PayImpl();
        interactor.payForTask("50", new PayListener() {
            @Override
            public void saccess() {
                mShowDialog = true;
            }

            @Override
            public void error() {
                Function.showToast("Ошибка оплаты");
                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference(Fields.TASKS_TABLE);
                mDatabase.child(key).child("premiumRun").setValue(false);
            }
        });
//        saccessPay();
    }



    private boolean mShowDialog = false;

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (mShowDialog) {
            mShowDialog = false;
            saccessPay();
        }
    }


    private void saccessPay(){
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference(Fields.TASKS_TABLE);
        mTask.setActive(false);
        mTask.setAnswerReqwest(true);
        mTask.setPremiumRun(false);
        mTask.setPremium(true);
        if(user != null){
            mTask.setLawyerId(user.getId());
            if(user.getPhoto() != null){
                mTask.setLawyerPhoto(user.getPhoto());
            }
            mTask.setLawyerName(user.getName());
        }
        mDatabase.child(mTask.getId()).setValue(mTask);
        Intent activity = new Intent(TaskConfirmActivity.this, TaskActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("extra", mTask.getId());
        activity.putExtras(bundle);
        TaskConfirmActivity.this.startActivity(activity);
        TaskConfirmActivity.this.finish();
    }
}

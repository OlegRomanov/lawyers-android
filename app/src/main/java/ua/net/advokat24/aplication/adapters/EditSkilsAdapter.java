package ua.net.advokat24.aplication.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ua.net.advokat24.R;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.BaseModel;
import ua.net.advokat24.util.models.CategoryListModel;

import java.util.List;

import ua.net.advokat24.util.Function;

public class EditSkilsAdapter extends RecyclerView.Adapter<EditSkilsAdapter.ViewHolder> {

    private List<String> list;
    private List<CategoryListModel>category;

    public EditSkilsAdapter(List<String> list, List<CategoryListModel>category) {
        this.list = list;
        this.category = category;
    }

    @NonNull
    @Override
    public EditSkilsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.i_skils, parent, false);
        return new EditSkilsAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final EditSkilsAdapter.ViewHolder holder , int position) {

        String model = list.get(holder.getAdapterPosition());
        holder.tv_name.setText(Function.getStringSkils(model, category));
        holder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.remove(holder.getAdapterPosition());
                notifyDataSetChanged();
                if(list.size() == 0){
                    if (event != null){
                        event.refresh();
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_name;
        private ImageView iv_delete;

        ViewHolder(View itemView) {
            super(itemView);
            iv_delete = itemView.findViewById(R.id.iv_delete);
            tv_name = itemView.findViewById(R.id.tv_name);
        }
    }

    public interface Event {
        void refresh();
    }

    private Event event;

    public void registerEvent(Event event) {
        this.event = event;
    }
}

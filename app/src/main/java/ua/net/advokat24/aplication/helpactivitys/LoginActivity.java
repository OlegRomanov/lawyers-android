package ua.net.advokat24.aplication.helpactivitys;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import ua.net.advokat24.R;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.repository.SharedPreferencesManager;
import ua.net.advokat24.util.Function;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_login);
        Function.printHashKey();
        checkInternetConection();
    }

    private void checkInternetConection(){
        if(Function.isOnline(this)){
            start();
        } else {
            showDialog();
        }
    }

    private void start(){
        if (SharedPreferencesManager.getInstance(LoginActivity.this).isRegister()) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            LoginActivity.this.finish();
        } else {
            FirebaseAuth.getInstance().signOut();
            LoginManager.getInstance().logOut();
            SharedPreferencesManager.getInstance(this).setExistIn(false);
            SharedPreferencesManager.getInstance(this).setIsRegister(false);
        }
    }

    private void showDialog() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(LoginActivity.this);
        dialog
                .setTitle(getResources().getString(R.string.error_conection))
                .setMessage(getResources().getString(R.string.error_conection))
                .setNegativeButton(getResources().getString(R.string.go_out), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.cancel();
                        LoginActivity.this.finish();
                    }
                })
                .setPositiveButton(getResources().getString(R.string.repeate), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        checkInternetConection();
                    }
                })
                .show();
    }

    public void isClient(View view) {
        view.setClickable(false);
        SharedPreferencesManager.getInstance(LoginActivity.this).setClient(true);
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        LoginActivity.this.finish();
    }

    public void islawyer(View view) {
        view.setClickable(false);
        SharedPreferencesManager.getInstance(LoginActivity.this).setClient(false);
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        LoginActivity.this.finish();
    }
}

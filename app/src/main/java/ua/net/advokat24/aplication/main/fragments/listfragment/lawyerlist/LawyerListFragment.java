package ua.net.advokat24.aplication.main.fragments.listfragment.lawyerlist;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ua.net.advokat24.R;
import ua.net.advokat24.aplication.adapters.TasksAdapter;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.aplication.main.MainActivityPresenter;
import ua.net.advokat24.repository.firebase.categoryimpl.CategoryImpl;
import ua.net.advokat24.repository.firebase.categoryimpl.CategorysInteractor;
import ua.net.advokat24.repository.firebase.categoryimpl.OnCategorysListener;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksImpl;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksInteractor;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksListener;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.CategoryListModel;
import ua.net.advokat24.util.models.MTask;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.net.advokat24.aplication.adapters.TasksAdapter;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.repository.firebase.categoryimpl.CategoryImpl;
import ua.net.advokat24.repository.firebase.categoryimpl.CategorysInteractor;
import ua.net.advokat24.repository.firebase.categoryimpl.OnCategorysListener;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksImpl;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksInteractor;
import ua.net.advokat24.repository.firebase.tasksimpl.TasksListener;

public class LawyerListFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private TasksAdapter adapter;
    private List<MTask> mTaskList;

    @BindView(R.id.tv_empty_list) TextView tv_empty_list;

    public LawyerListFragment() {}

    @OnClick(R.id.iv_toolbar_back) void iv_toolbar_back() {
        if (mListener != null) {
            mListener.backToHome();
        }
    }

    public static LawyerListFragment newInstance() {
        return new LawyerListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.f_lawyer_list, container, false);
        ButterKnife.bind(this, v);
        RecyclerView recyclerView = v.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new TasksAdapter(getContext(), false);
        recyclerView.setAdapter(adapter);
        final TextView tv_empty_list = v.findViewById(R.id.tv_empty_list);

        TasksInteractor init = new TasksImpl();
        init.getTasksRunTime(((MainActivity) getActivity()).getmUser().getId(), false, new TasksListener() {
            @Override
            public void resultList(final List<MTask> listtasks) {
                if(listtasks == null || listtasks.size() == 0){
                    tv_empty_list.setVisibility(View.VISIBLE);
                } else {
                    mTaskList = listtasks;
                    if (((MainActivity) getActivity()).getListCategory() != null){
                        adapter.initList(mTaskList, ((MainActivity) getActivity()).getListCategory());
                    } else {
                        CategorysInteractor iterator = new CategoryImpl();
                        iterator.getList(new OnCategorysListener() {
                            @Override
                            public void resultList(List<CategoryListModel> list) {
                                adapter.initList(mTaskList, list);
                            }

                            @Override
                            public void error() {

                            }
                        });
                    }
                }
            }

            @Override
            public void error() {

            }
        });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void backToHome();
    }
}
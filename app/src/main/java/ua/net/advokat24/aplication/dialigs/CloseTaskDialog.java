package ua.net.advokat24.aplication.dialigs;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.interfaces.OnSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar;
import ua.net.advokat24.MyApp;
import ua.net.advokat24.R;
import ua.net.advokat24.util.Function;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CloseTaskDialog extends DialogFragment {

    public static CloseTaskDialog newInstance() {
        return new CloseTaskDialog();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private String speed_reaction, quality_level, saccess_level;

    @BindView(R.id.rs_speed_reaction) CrystalSeekbar rs_speed_reaction;
    @BindView(R.id.rs_quality_level) CrystalSeekbar rs_quality_level;
    @BindView(R.id.rs_saccess_level) CrystalSeekbar rs_saccess_level;

    @BindView(R.id.tv_speed_reaction) TextView tv_speed_reaction;
    @BindView(R.id.tv_quality_level) TextView tv_quality_level;
    @BindView(R.id.tv_saccess_level) TextView tv_saccess_level;

    @OnClick(R.id.fl_ok) void fl_ok() {
        if(event != null){
            event.confirm(((Integer.parseInt(speed_reaction) + Integer.parseInt(quality_level) + Integer.parseInt(saccess_level)) / 3));
        }
        dismiss();
    }

    @OnClick(R.id.fl_cancel) void fl_cancel() {
        dismiss();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        Objects.requireNonNull(getDialog().getWindow()).setBackgroundDrawableResource(R.color.colorTrans);


        View v = inflater.inflate(R.layout.d_close_task, container, false);

        ButterKnife.bind(this, v);

        rs_speed_reaction.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
            @Override
            public void valueChanged(Number value) {
                speed_reaction = String.valueOf(value);
                tv_speed_reaction.setText(speed_reaction);

            }
        });

        rs_quality_level.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
            @Override
            public void valueChanged(Number value) {
                quality_level = String.valueOf(value);
                tv_quality_level.setText(quality_level);
            }
        });

        rs_saccess_level.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
            @Override
            public void valueChanged(Number value) {
                saccess_level = String.valueOf(value);
                tv_saccess_level.setText(saccess_level);
            }
        });


        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        Objects.requireNonNull(getDialog().getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        event.dismiss();
        super.onDismiss(dialog);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        Objects.requireNonNull(getDialog().getWindow()).getAttributes().windowAnimations = R.style.DialogAnimationNew;
    }

    public interface Event {
        void confirm(int reiting);
        void dismiss();
    }

    private Event event;

    public void registerEvent(Event event) {
        this.event = event;
    }
}
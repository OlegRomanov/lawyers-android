package ua.net.advokat24.aplication.main.fragments.listfragment.clientlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import ua.net.advokat24.R;
import ua.net.advokat24.aplication.helpactivitys.LawyerDetailsActivity;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.util.models.usermodels.MUser;
import ua.net.advokat24.util.utils.CitysUtils;
import ua.net.advokat24.util.utils.MapUtils;

import java.util.List;

import butterknife.ButterKnife;
import ua.net.advokat24.aplication.helpactivitys.LawyerDetailsActivity;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.util.models.usermodels.MUser;

public class ClientMapFragment extends Fragment implements
        OnMapReadyCallback{

    public ClientMapFragment() {}

    public static ClientMapFragment newInstance() {
        return new ClientMapFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    MapView mMapView;
    private GoogleMap googleMap;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.f_map, container, false);
        ButterKnife.bind(this, v);
        mMapView = v.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(this);
        return v;
    }

    public void onMapReady(GoogleMap map) {
        CameraUpdate current = CameraUpdateFactory.newLatLngZoom(CitysUtils.getCityCordinate(((MainActivity)getActivity()).getSearchCity()),8);
        map.moveCamera(current);
        setMarkers(((MainActivity)getActivity()).getListUsers(), map);
    }

    private void setMarkers(List<MUser>list, GoogleMap map){
        if (list != null){
            map.clear();
            map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    String tag = String.valueOf(marker.getTag());
                    Intent activity = new Intent(getContext(), LawyerDetailsActivity.class);
                    activity.putExtra(LawyerDetailsActivity.KEY, tag);
                    startActivity(activity);
                    return false;
                }
            });
            for (MUser user : list) {
                if(user.getLocatoinLat() != null && user.getLocatoinLon() != null){

                    LatLng latlon = new LatLng(Double.parseDouble(user.getLocatoinLat()), Double.parseDouble(user.getLocatoinLon()));
                    Marker marker = map.addMarker(new MarkerOptions()
                                    .position(latlon)
                                    .icon(MapUtils.bitmapDescriptorFromVector(getContext(), R.drawable.ic_marker_blue))
//                        .title(MapUtils.getAdressFromLocation(latlon, getContext()))
                    );
                    marker.setTag(user.getId());
                }
            }

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

}
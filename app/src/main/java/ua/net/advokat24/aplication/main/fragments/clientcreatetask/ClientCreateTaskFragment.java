package ua.net.advokat24.aplication.main.fragments.clientcreatetask;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import ua.net.advokat24.R;
import ua.net.advokat24.aplication.dialigs.ChooseTaskSkilsDialog;
import ua.net.advokat24.aplication.dialigs.EditTextDialog;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.util.models.MTask;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ClientCreateTaskFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private Location location;

    @BindView(R.id.ll_consalting)
    LinearLayout ll_consalting;

    @BindView(R.id.ll_extra)
    LinearLayout ll_extra;

    @OnClick(R.id.ll_consalting) void ll_consalting() {
        openDialog();
    }

    @OnClick(R.id.ll_extra) void ll_extra() {
        openDialog();
    }

    void openDialog() {
        ll_consalting.setClickable(false);
        ll_extra.setClickable(false);
        ChooseTaskSkilsDialog dialog = ChooseTaskSkilsDialog.newInstance(new Gson().toJson(((MainActivity) getActivity()).getListCategory()));
        dialog.registerEvent(new ChooseTaskSkilsDialog.Event() {
            @Override
            public void result(final String result) {
                EditTextDialog dialogText = EditTextDialog.newInstance("Описание",
                        "Опишите свою проблему и мы\n" +
                                "подберем адвоката",
                        "Отправить",
                        "Пропустить",
                        "");
                dialogText.registerEvent(new EditTextDialog.Event() {
                    @Override
                    public void confirm(String text) {
                        if (mListener != null) {
                            MTask mTask = new MTask();
                            mTask.setDescriprion(text);
                            mTask.setCategory(result);
                            if(location != null){
                                mTask.setLocatoinLat(String.valueOf(location.getLatitude()));
                                mTask.setLocatoinLon(String.valueOf(location.getLongitude()));
                            }
                            mListener.payGoToPhoneNamber(mTask);
                        }
                    }

                    @Override
                    public void dismiss() {
                        ll_consalting.setClickable(true);
                        ll_extra.setClickable(true);
                    }
                });
                dialogText.registerEventCancelable(new EditTextDialog.EventCancelable() {
                    @Override
                    public void cancel() {
                        if (mListener != null) {
                            MTask mTask = new MTask();
                            mTask.setDescriprion("");
                            mTask.setCategory(result);
                            if(location != null){
                                mTask.setLocatoinLat(String.valueOf(location.getLatitude()));
                                mTask.setLocatoinLon(String.valueOf(location.getLongitude()));
                            }
                            mListener.payGoToPhoneNamber(mTask);
                        }
                    }
                });
                dialogText.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), dialogText.getClass().getSimpleName());
            }

            @Override
            public void dismiss() {
                ll_consalting.setClickable(true);
                ll_extra.setClickable(true);
            }
        });
        dialog.show(getActivity().getSupportFragmentManager(), dialog.getClass().getSimpleName());
    }

    public ClientCreateTaskFragment() { }

    public void setTaskLocation(Location location){
        this.location = location;

    }

    public static ClientCreateTaskFragment newInstance() {
        return new ClientCreateTaskFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.f_task_config, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        void payGoToPhoneNamber(MTask mTask);
    }
}

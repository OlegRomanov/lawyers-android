package ua.net.advokat24.aplication.dialigs;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import ua.net.advokat24.R;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.util.models.usermodels.WorkTime;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.net.advokat24.util.Function;

public class WorkTimeDialog extends DialogFragment {

    private WorkTime workTime;

    private static final String KEY = "workTimeDialogKey";

    public static WorkTimeDialog newInstance(String data) {
        WorkTimeDialog fragment = new WorkTimeDialog();
        Bundle bundle = new Bundle();
        bundle.putString(KEY, data);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            workTime = new Gson().fromJson(getArguments().getString(KEY), WorkTime.class);
        }
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogTheme);
    }

    @BindView(R.id.tv_time_start) TextView tv_time_start;
//    @BindView(R.id.tv_time_double_start) TextView tv_time_double_start;
    @BindView(R.id.tv_time_end) TextView tv_time_end;
//    @BindView(R.id.tv_double_time_end) TextView tv_double_time_end;

    @BindView(R.id.tv_prise) TextView tv_prise;
//    @BindView(R.id.tv_prise_double) TextView tv_prise_double;

    @BindView(R.id.swch_monday) SwitchCompat swch_monday;
    @BindView(R.id.swch_tuesday) SwitchCompat swch_tuesday;
    @BindView(R.id.swch_wednesday) SwitchCompat swch_wednesday;
    @BindView(R.id.swch_thursday) SwitchCompat swch_thursday;
    @BindView(R.id.swch_friday) SwitchCompat swch_friday;
    @BindView(R.id.swch_saturday) SwitchCompat swch_saturday;
    @BindView(R.id.swch_sunday) SwitchCompat swch_sunday;

    @BindView(R.id.ll_prise) LinearLayout ll_prise;
//    @BindView(R.id.ll_prise_double) LinearLayout ll_prise_double;

    @BindView(R.id.ll_time) LinearLayout ll_time;
//    @BindView(R.id.ll_time_double) LinearLayout ll_time_double;

//    private boolean isTimeValidate(){
//        int a = Integer.parseInt(workTime.getTimeStart().replaceAll(":", ""));
//        int b = Integer.parseInt(workTime.getTimeEnd().replaceAll(":", ""));
////        int c = Integer.parseInt(workTime.getTimeDoubleStart().replaceAll(":", ""));
////        int d = Integer.parseInt(workTime.getTimeDoubleEnd().replaceAll(":", ""));
//        return (c < a && d < a) || (c > b && d > b);
//    }

    @OnClick(R.id.iv_toolbar_action) void iv_toolbar_action() {
        workTime.setMonday(swch_monday.isChecked());
        workTime.setTuesday(swch_tuesday.isChecked());
        workTime.setWednesday(swch_wednesday.isChecked());
        workTime.setThursday(swch_thursday.isChecked());
        workTime.setFriday(swch_friday.isChecked());
        workTime.setSaturday(swch_saturday.isChecked());
        workTime.setSunday(swch_sunday.isChecked());

        event.confirm(workTime);
        dismiss();

//        if(isTimeValidate()){
//            event.confirm(workTime);
//            dismiss();
//        } else {
//            Function.showToast("Время обычной работы и работы по двойному тарифу не должно пересекаться");
//        }
    }

    @OnClick(R.id.ll_prise) void ll_prise() {
        ll_prise.setClickable(false);
        EditPriseDialog dialog = EditPriseDialog.newInstance();
        dialog.registerEvent(new EditPriseDialog.Event() {

            @Override
            public void confirm(String text) {
                workTime.setPrise(Integer.parseInt(text));
                tv_prise.setText(text);
            }

            @Override
            public void dismiss() {
                ll_prise.setClickable(true);
            }
        });

        if (!dialog.isVisible()){
            dialog.show(getActivity().getSupportFragmentManager(), dialog.getClass().getSimpleName());
        }
    }

//    @OnClick(R.id.ll_prise_double) void ll_prise_double() {
//        ll_prise_double.setClickable(false);
//
//        EditPriseDialog dialog = EditPriseDialog.newInstance();
//        dialog.registerEvent(new EditPriseDialog.Event() {
//
//            @Override
//            public void confirm(String text) {
//                workTime.setPriseDouble(Integer.parseInt(text));
//                tv_prise_double.setText(text);
//            }
//
//            @Override
//            public void dismiss() {
//                ll_prise_double.setClickable(true);
//            }
//        });
//
//        if (!dialog.isVisible()){
//            dialog.show(getActivity().getSupportFragmentManager(), dialog.getClass().getSimpleName());
//        }
//    }

    @OnClick(R.id.iv_toolbar_back) void iv_toolbar_back() {
        dismiss();
    }

    @OnClick(R.id.ll_time) void ll_time() {

        ll_time.setClickable(false);
        WorkTimeHourDialog dialog = WorkTimeHourDialog.newInstance(workTime.getTimeStart(), workTime.getTimeEnd());
        dialog.registerEvent(new WorkTimeHourDialog.Event() {
            @Override
            public void result(String from, String to) {
                workTime.setTimeStart(from);
                workTime.setTimeEnd(to);
                tv_time_start.setText(from);
                tv_time_end.setText(to);
            }

            @Override
            public void dismiss() {
                ll_time.setClickable(true);
            }
        });

        if (!dialog.isVisible()){
            dialog.show(getActivity().getSupportFragmentManager(), dialog.getClass().getSimpleName());
        }
    }

//    @OnClick(R.id.ll_time_double) void ll_time_double() {
//
//        ll_time_double.setClickable(false);
//        WorkTimeHourDialog dialog = WorkTimeHourDialog.newInstance(workTime.getTimeDoubleStart(), workTime.getTimeDoubleEnd());
//        dialog.registerEvent(new WorkTimeHourDialog.Event() {
//            @Override
//            public void result(String from, String to) {
////                workTime.setTimeDoubleStart(from);
////                workTime.setTimeDoubleEnd(to);
//                tv_time_double_start.setText(from);
//                tv_double_time_end.setText(to);
//            }
//
//            @Override
//            public void dismiss() {
//                ll_time_double.setClickable(true);
//            }
//        });
//
//        if (!dialog.isVisible()){
//            dialog.show(getActivity().getSupportFragmentManager(), dialog.getClass().getSimpleName());
//        }
//    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        Objects.requireNonNull(getDialog().getWindow()).setBackgroundDrawableResource(R.color.colorTrans);
        View v = inflater.inflate(R.layout.d_edit_work_time, container, false);
        ButterKnife.bind(this, v);

        swch_monday.setChecked(workTime.isMonday());
        swch_tuesday.setChecked(workTime.isTuesday());
        swch_wednesday.setChecked(workTime.isWednesday());
        swch_thursday.setChecked(workTime.isThursday());
        swch_friday.setChecked(workTime.isFriday());
        swch_saturday.setChecked(workTime.isSaturday());
        swch_sunday.setChecked(workTime.isSunday());

        tv_time_start.setText(workTime.getTimeStart());
        tv_time_end.setText(workTime.getTimeEnd());

//        tv_time_double_start.setText(workTime.getTimeDoubleStart());
//        tv_double_time_end.setText(workTime.getTimeDoubleEnd());
//
//        tv_prise.setText(String.valueOf(workTime.getPrise()));
//        tv_prise_double.setText(String.valueOf(workTime.getPriseDouble()));

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        Objects.requireNonNull(getDialog().getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        event.dismiss();
        super.onDismiss(dialog);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        Objects.requireNonNull(getDialog().getWindow()).getAttributes().windowAnimations = R.style.DialogAnimationNew;
    }

    public interface Event {
        void confirm(WorkTime workTime);
        void dismiss();
    }

    private Event event;

    public void registerEvent(Event event) {
        this.event = event;
    }
}
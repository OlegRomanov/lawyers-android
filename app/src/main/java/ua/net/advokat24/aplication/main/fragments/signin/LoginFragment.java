package ua.net.advokat24.aplication.main.fragments.signin;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import ua.net.advokat24.util.Function;
import ua.net.advokat24.R;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.repository.SharedPreferencesManager;
import ua.net.advokat24.util.MyTextInputLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.net.advokat24.aplication.main.MainActivity;
import ua.net.advokat24.repository.SharedPreferencesManager;
import ua.net.advokat24.util.Function;

public class LoginFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public LoginFragment() {}

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @BindView(R.id.til_email) MyTextInputLayout til_email;
    @BindView(R.id.til_passwd) MyTextInputLayout til_passwd;
    @BindView(R.id.et_email) TextInputEditText et_email;
    @BindView(R.id.et_passwd) TextInputEditText et_passwd;
    @BindView(R.id.fi_registrationFB) FrameLayout fi_registrationFB;
//    @BindView(R.id.chb_remember_me) CheckBox chb_remember_me;

    @OnClick(R.id.tv_login) void login() {
        if(et_email.getText().toString().length() > 0 && et_passwd.getText().toString().length() > 0){
            if(Function.checkEmail(et_email.getText().toString())){
                if(et_passwd.getText().toString().length() > 5){
                    if (mListener != null) {
//                        if(chb_remember_me.isChecked()){
//                            SharedPreferencesManager.getInstance(getContext()).setUserEmail(et_email.getText().toString());
//                            SharedPreferencesManager.getInstance(getContext()).setPasswd(et_passwd.getText().toString());
//                        }
                        mListener.signin(et_email.getText().toString(), et_passwd.getText().toString());
                    }
                } else {
                    til_passwd.setError(" ");
                    Function.showToast(getResources().getString(R.string.pasword_litle));
                }
            } else {
                til_email.setError(" ");
                Function.showToast(getResources().getString(R.string.invalid_eail));
            }
        } else {
            if (TextUtils.isEmpty(et_email.getText().toString())) {
                til_email.setError(" ");
            }
            if (TextUtils.isEmpty(et_passwd.getText().toString())) {
                til_passwd.setError(" ");
            }
            Function.showToast(getResources().getString(R.string.error_enter_oll_fields));
        }
    }

    @OnClick(R.id.tv_registration) void registration() {
        if (mListener != null) {
            mListener.goToRegistration();
        }
    }

    @OnClick(R.id.iv_toolbar_back) void toolbarBack() {
        if (mListener != null) {
            ((MainActivity)getActivity()).logOut();
        }
    }

    @OnClick(R.id.forgotPasswd) void forgotPasswd() {
        if(Function.checkEmail(et_email.getText().toString())){
            final ProgressDialog dialogWait = Function.startProgress(getContext());
            FirebaseAuth.getInstance().sendPasswordResetEmail(et_email.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            dialogWait.dismiss();
                            if (task.isSuccessful()) {
                                Function.showToast("Сообщение отправлено на почту");
                            } else {
                                Function.showToast(getResources().getString(R.string.invalid_eail));
                            }
                        }
                    });
        } else {
            til_email.setError(" ");
            Function.showToast(getResources().getString(R.string.invalid_eail));
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.f_login, container, false);
        ButterKnife.bind(this, v);

        LoginButton facebook_login = v.findViewById(R.id.facebook_login);
        mListener.facebookButton(facebook_login);;
        et_email.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "sf_ui_display_dold.ttf"));
        til_email.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "sf_uI_display_regular.ttf"));

        et_email.setText(SharedPreferencesManager.getInstance(getContext()).getUserEmail());
        et_passwd.setText(SharedPreferencesManager.getInstance(getContext()).getPasswd());

        et_email.addTextChangedListener(new TextWatcher() {public void afterTextChanged(Editable s) {}public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(til_email.isErrorEnabled()){
                    til_email.setErrorEnabled(false);
                }
            }
        });

        et_passwd.addTextChangedListener(new TextWatcher() {public void afterTextChanged(Editable s) {}public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(til_passwd.isErrorEnabled()){
                    til_passwd.setErrorEnabled(false);
                }
            }
        });

        if(FirebaseAuth.getInstance().getCurrentUser() != null){
            fi_registrationFB.setVisibility(View.GONE);
        }

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void signin(String email, String passwd);
        void facebookButton(LoginButton facebook_login);
        void goToRegistration();
    }
}
